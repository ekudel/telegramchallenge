package com.kubro.telegramchallenge;

import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kubro.telegramchallenge.util.Consumer;
import com.kubro.telegramchallenge.util.Size;
import com.kubro.telegramchallenge.view.EditPictureGlassView;
import com.kubro.telegramchallenge.view.ImageBrowserView;
import com.kubro.telegramchallenge.view.PhotoRotationStripesView;

import java.util.Locale;

/**
 * @author Eugene.Kudelevsky
 */
public class EditPhotoFragment extends FragmentBase {

  private View myRotatePictureButton;
  private View myEditPictureCancelButton;
  private View myEditPictureResetButton;
  private View myEditPictureDoneButton;
  private ImageBrowserView myEditPhotoView;
  private EditPictureGlassView myEditPictureGlassView;
  private View myEditPicturePanel;
  private PhotoRotationStripesView myPhotoRotationStripesView;
  private int myBaseRotationAngle = 0;
  private TextView myPhotoRotationTextView;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_edit_photo, container, false);
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    View view = getView();
    assert view != null;
    myRotatePictureButton = view.findViewById(R.id.rotatePictureButton);
    myEditPictureCancelButton = view.findViewById(R.id.editPictureCancelButton);
    myEditPictureResetButton = view.findViewById(R.id.editPictureResetButton);
    myEditPictureDoneButton = view.findViewById(R.id.editPictureDoneButton);
    myEditPhotoView = (ImageBrowserView) view.findViewById(R.id.editPhotoView);
    myEditPictureGlassView = (EditPictureGlassView) view.findViewById(R.id.editPictureGlassView);
    myEditPicturePanel = view.findViewById(R.id.editPicturePanel);
    myPhotoRotationStripesView = (PhotoRotationStripesView) view.findViewById(R.id.photoRotationStripesView);
    myPhotoRotationTextView = (TextView) view.findViewById(R.id.photoRotationTextView);

    myRotatePictureButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        StateAwareActivity activity = getActivityInterface();

        if (activity == null || activity.getState() != ActivityState.EDITING_PHOTO) {
          return;
        }
        myBaseRotationAngle -= 90;
        updateRotationAndAngleText();
        handleOnCroppingGlassChanged();
      }
    });
    myEditPictureCancelButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        StateAwareActivity activity = getActivityInterface();

        if (activity != null && activity.getState() == ActivityState.EDITING_PHOTO) {
          cancelEditingPhoto();
        }
      }
    });
    myEditPictureResetButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        StateAwareActivity activity = getActivityInterface();

        if (activity != null && activity.getState() == ActivityState.EDITING_PHOTO) {
          reset();
        }
      }
    });
    myEditPictureDoneButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        ActivityInterface activity = getActivityInterface();

        if (activity == null || activity.getState() != ActivityState.EDITING_PHOTO) {
          return;
        }
        activity.onEditingFinished(true);
      }
    });
    myEditPhotoView.setChangeListener(new Runnable() {
      @Override
      public void run() {
        myEditPictureGlassView.updateMargins();
        updateEditResetButtonVisibility();
      }
    });
    updateEditResetButtonVisibility();

    myEditPictureGlassView.setOnGlassMarginsChangedListener(new Runnable() {
      @Override
      public void run() {
        handleOnCroppingGlassChanged();
      }
    });
    myEditPictureGlassView.setGlassRectAdjuster(new Consumer<RectF>() {
      @Override
      public void consume(RectF rect) {
        PointF[] imageVertices = myEditPhotoView.getImageVerticesOnView();

        if (imageVertices != null) {
          rect.intersect(Util.computeBoundRect(imageVertices));
        }
      }
    });
    myEditPictureGlassView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
      @Override
      public void onLayoutChange(View v, int left, int top, int right, int bottom,
                                 int oldLeft, int oldTop, int oldRight, int oldBottom) {
        if (left == 0 && top == 0 && right == 0 && bottom == 0) {
          return;
        }
        myEditPhotoView.setContentInsets(myEditPictureGlassView.getMinGlassHMargin(),
            myEditPictureGlassView.getMinGlassVMargin());
        dropGlassMargins();
      }
    });
    myPhotoRotationStripesView.setAngleChangedListener(new PhotoRotationStripesView.AngleChangedListener() {
      @Override
      public void angleChanged(float angle) {
        StateAwareActivity activity = getActivityInterface();

        if (activity == null || activity.getState() != ActivityState.EDITING_PHOTO) {
          return;
        }
        updateRotationAndAngleText();
        handleOnCroppingGlassChanged();
      }
    });
  }

  public void reset() {
    myBaseRotationAngle = 0;
    myPhotoRotationStripesView.setCurrentAngle(0);
    myEditPhotoView.resetZoom();
    updateRotationAndAngleText();
    dropGlassMargins();
  }

  private void updateRotationAndAngleText() {
    float angle = Math.round(myPhotoRotationStripesView.getCurrentAngle() * 10) / 10f;
    myEditPhotoView.setImageRotation(myBaseRotationAngle + angle);

    if (Math.abs(angle) < Util.EPS) {
      myPhotoRotationTextView.setText("0.0\u00B0");
    } else {
      myPhotoRotationTextView.setText(String.format(Locale.US, "%.1f\u00B0", angle));
    }
  }

  public void dropGlassMargins() {
    myEditPictureGlassView.setGlassMargins(0, 0, 0, 0);
    myEditPictureGlassView.setShowGrid(false);
    handleOnCroppingGlassChanged();
  }

  public boolean onBackPressed() {
    StateAwareActivity activity = getActivityInterface();

    if (activity != null && activity.getState() == ActivityState.EDITING_PHOTO) {
      cancelEditingPhoto();
      return true;
    }
    return false;
  }

  private void cancelEditingPhoto() {
    ActivityInterface activity = getActivityInterface();

    if (activity != null) {
      activity.onEditingFinished(false);
    }
  }

  private void handleOnCroppingGlassChanged() {
    ActivityInterface activity = getActivityInterface();

    if (activity == null) {
      return;
    }
    float viewWidth = (float) myEditPhotoView.getWidth();
    float viewHeight = (float) myEditPhotoView.getHeight();

    if (viewWidth == 0 || viewHeight == 0) {
      return;
    }
    float l = myEditPictureGlassView.getGlassLeftMargin();
    float r = myEditPictureGlassView.getWidth() - myEditPictureGlassView.getGlassRightMargin();
    float t = myEditPictureGlassView.getGlassTopMargin();
    float b = myEditPictureGlassView.getHeight() - myEditPictureGlassView.getGlassBottomMargin();
    RectF viewRect = new RectF(l, t, r, b);
    RectF contentRect = myEditPhotoView.toContentRect(viewRect);
    float ratio = viewWidth / viewHeight;
    float l2 = contentRect.centerX() - ratio * contentRect.height() / 2;
    float t2 = contentRect.top;
    float scale = (viewHeight - myEditPictureGlassView.getMinGlassVMargin() * 2) / contentRect.height();

    if (l2 > contentRect.left) {
      l2 = contentRect.left;
      t2 = contentRect.centerY() - contentRect.width() / ratio / 2;
      scale = (viewWidth - myEditPictureGlassView.getMinGlassHMargin() * 2) / contentRect.width();
    }
    myEditPhotoView.setZoom(scale, 0, 0);
    myEditPhotoView.setViewportPosition(l2 * scale - myEditPictureGlassView.getMinGlassHMargin(),
        t2 * scale - myEditPictureGlassView.getMinGlassVMargin());
    viewRect = myEditPhotoView.toViewRect(contentRect);

    myEditPictureGlassView.setGlassMargins(viewRect.left, viewRect.top,
        viewWidth - viewRect.right,
        viewHeight - viewRect.bottom
    );
  }

  private void updateEditResetButtonVisibility() {
    boolean modified = myEditPhotoView.isZoomChanged();
    myEditPictureResetButton.setVisibility(modified ? View.VISIBLE : View.GONE);
  }

  @Nullable
  private ActivityInterface getActivityInterface() {
    return (ActivityInterface) getActivity();
  }

  public ImageBrowserView getEditPhotoView() {
    return myEditPhotoView;
  }

  public View getEditPicturePanel() {
    return myEditPicturePanel;
  }

  public EditPictureGlassView getEditPictureGlassView() {
    return myEditPictureGlassView;
  }

  public float getImageRotation() {
    return myEditPhotoView.getImageRotation();
  }

  public interface ActivityInterface extends StateAwareActivity {
    void onEditingFinished(boolean ok);

    Size getBitmapSizeForEditing();
  }
}
