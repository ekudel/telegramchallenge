package com.kubro.telegramchallenge;

import android.animation.Animator;
import android.graphics.PointF;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Eugene.Kudelevsky
 */
public class Util {
  private static final String LOG_TAG = Util.class.getSimpleName();
  public static final float EPS = 1e-4f;
  public static final float VIDEO_BOTTOM_PANEL_ALPHA = 0.4f;

  private Util() {
  }

  public static final double PHOTO_RATIO = 4.0 / 3;
  public static final double VIDEO_RATIO = 16.0 / 9;

  // for debugging animations
  public static void setAnimatorDuration(@NonNull Animator animator, long duration) {
    animator.setDuration(duration);
  }

  public static boolean copyFile(@NonNull File from, @NonNull File to) {
    try {
      InputStream input = new FileInputStream(from);
      OutputStream output = new FileOutputStream(to);

      try {
        byte[] buffer = new byte[1024];
        int length;
        while ((length = input.read(buffer)) > 0) {
          output.write(buffer, 0, length);
        }
      } finally {
        input.close();
        output.close();
      }
      return true;
    } catch (IOException e) {
      Log.e(LOG_TAG, "cannot copy file from \"" + from.getAbsolutePath() +
          "\" to \"" + to.getAbsolutePath() + "\"");
      return false;
    }
  }

  @NonNull
  public static RectF scaleRect(@NonNull RectF rect, float factor) {
    return new RectF(rect.left * factor, rect.top * factor,
        rect.right * factor, rect.bottom * factor);
  }

  @Nullable
  public static PointF intersectLines(@NonNull PointF line1Point1, @NonNull PointF line1Point2,
                                      @NonNull PointF line2Point1, @NonNull PointF line2Point2) {
    double a1 = (double) line1Point1.y - line1Point2.y;
    double b1 = (double) line1Point2.x - line1Point1.x;
    double c1 = (double) line1Point1.x * line1Point2.y - (double) line1Point2.x * line1Point1.y;
    double a2 = (double) line2Point1.y - line2Point2.y;
    double b2 = (double) line2Point2.x - line2Point1.x;
    double c2 = (double) line2Point1.x * line2Point2.y - (double) line2Point2.x * line2Point1.y;

    if (a1 * b2 - a2 * b1 == 0 || a1 * b2 - a2 * b1 == 0) {
      return null;
    }
    float x = (float) ((c2 * b1 - c1 * b2) / (a1 * b2 - a2 * b1));
    float y = (float) ((a2 * c1 - a1 * c2) / (a1 * b2 - a2 * b1));
    return new PointF(x, y);
  }

  @NonNull
  public static PointF[] intersectLineSegmentAndPolygon(
      @NonNull PointF[] polygon, @NonNull PointF linePoint1, @NonNull PointF linePoint2) {
    PointF[] points = intersectLineAndPolygon(polygon, linePoint1, linePoint2);
    List<PointF> result = new ArrayList<>();
    float minX = Math.min(linePoint1.x, linePoint2.x);
    float maxX = Math.max(linePoint1.x, linePoint2.x);
    float minY = Math.min(linePoint1.y, linePoint2.y);
    float maxY = Math.max(linePoint1.y, linePoint2.y);

    for (PointF p : points) {
      if (p.x >= minX - Util.EPS && p.x <= maxX + Util.EPS &&
          p.y >= minY - Util.EPS && p.y <= maxY + Util.EPS) {
        result.add(p);
      }
    }
    return result.toArray(new PointF[result.size()]);
  }

  @NonNull
  public static PointF[] intersectLineAndPolygon(
      @NonNull PointF[] polygon, @NonNull PointF linePoint1, @NonNull PointF linePoint2) {
    Set<PointF> result = new HashSet<>();

    for (int i = 0; i < polygon.length; i++) {
      PointF polygonPoint1 = polygon[i];
      PointF polygonPoint2 = polygon[i < polygon.length - 1 ? i + 1 : 0];
      PointF intersection = intersectLines(polygonPoint1, polygonPoint2, linePoint1, linePoint2);

      if (intersection != null &&
          intersection.x >= Math.min(polygonPoint1.x, polygonPoint2.x) - Util.EPS &&
          intersection.x <= Math.max(polygonPoint1.x, polygonPoint2.x) + Util.EPS) {
        result.add(intersection);
      }
    }
    return result.toArray(new PointF[result.size()]);
  }

  @NonNull
  public static <T> T[] concatArrays(@NonNull T[] arr1, @NonNull T[] arr2) {
    @SuppressWarnings("unchecked")
    T[] result = (T[]) java.lang.reflect.Array
        .newInstance(arr1.getClass().getComponentType(), arr1.length + arr2.length);
    System.arraycopy(arr1, 0, result, 0, arr1.length);
    System.arraycopy(arr2, 0, result, arr1.length, arr2.length);
    return result;
  }

  @NonNull
  public static RectF computeBoundRect(@NonNull PointF[] points) {
    if (points.length == 0) {
      throw new IllegalArgumentException();
    }
    float leftBound = Float.MAX_VALUE;
    float rightBound = -Float.MAX_VALUE;
    float topBound = Float.MAX_VALUE;
    float bottomBound = -Float.MAX_VALUE;

    for (PointF p : points) {
      leftBound = Math.min(leftBound, p.x);
      rightBound = Math.max(rightBound, p.x);
      topBound = Math.min(topBound, p.y);
      bottomBound = Math.max(bottomBound, p.y);
    }
    return new RectF(leftBound, topBound, rightBound, bottomBound);
  }

  public static float fitIntoBounds(float value, float min, float max) {
    return Math.min(max, Math.max(min, value));
  }

  public static boolean contains(@NonNull int[] arr, int value) {
    for (int element : arr) {
      if (element == value) {
        return true;
      }
    }
    return false;
  }
}
