package com.kubro.telegramchallenge;

import android.support.annotation.NonNull;

/**
 * @author Eugene.Kudelevsky
 */
public interface StateAwareActivity {
  void updateState(@NonNull ActivityState state);

  void updateStateBeforeTransition(@NonNull ActivityState toState);

  void updateStateBeforePlayingAnimation();

  void updateStateAfterPlayingAnimation();

  ActivityState getState();
}
