package com.kubro.telegramchallenge;

/**
 * @author Eugene.Kudelevsky
 */
public interface OrientationAwareActivity {
  int getUiOrientationInDegrees();

  int getSensorOrientationInDegrees();
}
