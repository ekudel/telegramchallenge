package com.kubro.telegramchallenge;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;

import com.kubro.telegramchallenge.util.Consumer;
import com.kubro.telegramchallenge.util.Size;
import com.kubro.telegramchallenge.view.ImageBrowserView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends Activity implements
    BrowseFragment.ActivityInterface,
    EditPhotoFragment.ActivityInterface,
    MainFragment.ActivityInterface,
    OrientationAwareActivity {
  private static final String LOG_TAG = MainActivity.class.getSimpleName();
  private static final float MAX_BITMAP_SIZE_FOR_IMAGE_VIEW = 2048;

  private ActivityState myState;
  private boolean myDestroyed;

  private MainFragment myMainFragment;
  private BrowseFragment myBrowseFragment;
  private EditPhotoFragment myEditPhotoFragment;
  @NonNull private List<FragmentBase> myAllFragments = Collections.emptyList();

  // todo: optimize memory
  private volatile Bitmap myTempBitmap;
  private volatile Bitmap myScaledTempBitmap;

  private volatile File myTempVideoFile;
  private HandlerThread myMediaFilesThread;
  private Handler myMediaFilesHandler;
  private OrientationEventListener myOrientationEventListener;
  private int myCurrentOrientationInDegrees;
  private int myAppliedOrientationInDegrees;
  private Handler myHandler;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.activity_main);

    myHandler = new Handler();
    myDestroyed = false;
    myTempBitmap = null;
    myScaledTempBitmap = null;
    myTempVideoFile = null;
    myMediaFilesThread = new HandlerThread("MediaFiles");
    myMediaFilesThread.start();
    myMediaFilesHandler = new Handler(myMediaFilesThread.getLooper());
    myCurrentOrientationInDegrees = 0;
    myAppliedOrientationInDegrees = 0;

    FragmentManager fragmentManager = getFragmentManager();
    myMainFragment = (MainFragment) fragmentManager.findFragmentById(R.id.mainFragment);
    myBrowseFragment = (BrowseFragment) fragmentManager.findFragmentById(R.id.browseFragment);
    myEditPhotoFragment = (EditPhotoFragment) fragmentManager.findFragmentById(R.id.editPhotoFragment);
    myAllFragments = Arrays.asList(myMainFragment, myBrowseFragment, myEditPhotoFragment);

    myOrientationEventListener = new OrientationEventListener(this) {
      @Override
      public void onOrientationChanged(int orientation) {
        int newOrientation = Math.round((float) orientation / 90) * 90 % 360;

        if (myCurrentOrientationInDegrees != newOrientation) {
          myCurrentOrientationInDegrees = newOrientation;
          handleOrientationChanged();
        }
      }
    };
    showFragment(R.id.mainFragment, true);
    updateState(ActivityState.CAPTURING_PHOTO);
  }

  private void handleOrientationChanged() {
    for (FragmentBase fragment : myAllFragments) {
      fragment.onSensorOrientationChanged(myCurrentOrientationInDegrees);
    }
    if (myCurrentOrientationInDegrees != myAppliedOrientationInDegrees) {
      for (final FragmentBase fragment : myAllFragments) {
        if (fragment.canHandleOrientationChange()) {
          final int oldDegrees = myAppliedOrientationInDegrees;
          myAppliedOrientationInDegrees = myCurrentOrientationInDegrees;
          int delta1 = myAppliedOrientationInDegrees - oldDegrees;
          int delta2 = delta1 <= 0 ? delta1 + 360 : delta1 - 360;
          final int delta = Math.abs(delta1) < Math.abs(delta2) ? delta1 : delta2;
          updateState(new ActivityState.ChangingOrientation(myState));
          Animator animator = fragment.buildOrientationChangeAnimator(oldDegrees, delta);

          if (animator == null) {
            throw new IllegalStateException("Return value shouldn't be null");
          }
          animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
              if (myDestroyed) {
                return;
              }
              fragment.setUiOrientation(myAppliedOrientationInDegrees);

              if (!(myState instanceof ActivityState.ChangingOrientation)) {
                throw new IllegalStateException("Incorrect state: " + myState);
              }
              updateState(((ActivityState.ChangingOrientation) myState).getNextState());
            }
          });
          animator.start();
          break;
        }
      }
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    myOrientationEventListener.enable();
  }

  @Override
  protected void onPause() {
    super.onPause();
    myOrientationEventListener.disable();
  }

  private void showFragment(int fragmentId, boolean hideOtherFragments) {
    if (myDestroyed) {
      return;
    }
    FragmentTransaction transaction = getFragmentManager().beginTransaction();

    for (Fragment fragment : myAllFragments) {
      if (fragmentId == fragment.getId()) {
        transaction.show(fragment);
      } else if (hideOtherFragments) {
        transaction.hide(fragment);
      }
    }
    transaction.commitAllowingStateLoss();
  }

  @Override
  public void updateState(@NonNull ActivityState state) {
    myState = state;

    if (state == ActivityState.CAPTURING_PHOTO || state == ActivityState.CAPTURING_VIDEO ||
        state instanceof ActivityState.RecordingVideo || state == ActivityState.BROWSING_VIDEO) {
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    } else {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
    if (myCurrentOrientationInDegrees != myAppliedOrientationInDegrees) {
      myHandler.post(new Runnable() {
        @Override
        public void run() {
          handleOrientationChanged();
        }
      });
    }
  }

  @Override
  public void updateStateBeforeTransition(@NonNull ActivityState toState) {
    updateState(new ActivityState.TransitionalState(myState, toState));
  }

  @Override
  public void updateStateBeforePlayingAnimation() {
    updateState(new ActivityState.PlayingAnimationState(myState));
  }

  @Override
  public void updateStateAfterPlayingAnimation() {
    if (myState instanceof ActivityState.PlayingAnimationState) {
      updateState(((ActivityState.PlayingAnimationState) myState).getNextState());
    } else if (myState instanceof ActivityState.TransitionalState) {
      updateState(((ActivityState.TransitionalState) myState).getToState());
    } else {
      Log.e(LOG_TAG, "IllegalState: " + myState, new IllegalStateException());
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    myDestroyed = true;
    myMediaFilesHandler.removeCallbacks(null);

    myMediaFilesHandler.post(new Runnable() {
      @Override
      public void run() {
        clearTempData();
        myMediaFilesThread.quit();
      }
    });
  }

  @Override
  public void onBackPressed() {
    for (FragmentBase fragment : myAllFragments) {
      if (fragment.onBackPressed()) {
        return;
      }
    }
    super.onBackPressed();
  }

  @Override
  public ActivityState getState() {
    return myState;
  }

  @Override
  public Size getBitmapSizeForEditing() {
    return new Size(myScaledTempBitmap.getWidth(), myScaledTempBitmap.getHeight());
  }

  @Override
  public boolean isDestroyed() {
    return myDestroyed;
  }

  public void onCropPictureButtonClicked() {
    updateStateBeforeTransition(ActivityState.EDITING_PHOTO);
    buildStartOrStopEditingPhotoAnimator(true).start();
  }

  @NonNull
  public Animator buildStartOrStopEditingPhotoAnimator(final boolean forStart) {
    ValueAnimator animator = forStart ? ValueAnimator.ofFloat(0f, 1f) :
        ValueAnimator.ofFloat(1f, 0f);
    animator.setInterpolator(new LinearInterpolator());
    Util.setAnimatorDuration(animator, 300);
    final int fragmentId = forStart ? R.id.editPhotoFragment : R.id.browseFragment;

    animator.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationStart(Animator animation) {
        showFragment(fragmentId, false);

        if (forStart) {
          myEditPhotoFragment.getEditPicturePanel().setVisibility(View.VISIBLE);
          myEditPhotoFragment.reset();
          myEditPhotoFragment.getEditPictureGlassView().setVisibility(View.VISIBLE);
          myEditPhotoFragment.getEditPhotoView().setImageBitmap(myScaledTempBitmap);
        } else {
          myBrowseFragment.getBrowsePhotoViewWrapper().setVisibility(View.VISIBLE);
          FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)
              myBrowseFragment.getBrowsePhotoView().getLayoutParams();
          layoutParams.setMargins(0, 0, 0, 0);
          myBrowseFragment.getBrowsePhotoView().setLayoutParams(layoutParams);
          myBrowseFragment.getSeekBar().setVisibility(View.GONE);
          myBrowseFragment.getBrowsePicturePanel().setVisibility(View.VISIBLE);
        }
      }

      @Override
      public void onAnimationEnd(Animator animation) {
        showFragment(fragmentId, true);

        if (forStart) {
          myBrowseFragment.getBrowsePicturePanel().setVisibility(View.GONE);
          myBrowseFragment.getBrowsePhotoViewWrapper().setVisibility(View.GONE);
          myEditPhotoFragment.getEditPhotoView().setVisibility(View.VISIBLE);
          myEditPhotoFragment.getEditPhotoView().setContentInsets(
              myEditPhotoFragment.getEditPictureGlassView().getMinGlassHMargin(),
              myEditPhotoFragment.getEditPictureGlassView().getMinGlassVMargin());
        } else {
          myEditPhotoFragment.getEditPicturePanel().setVisibility(View.GONE);
          myEditPhotoFragment.getEditPictureGlassView().setVisibility(View.GONE);
          myEditPhotoFragment.getEditPhotoView().setVisibility(View.GONE);
          myEditPhotoFragment.getEditPhotoView().setAlpha(1f);
          myEditPhotoFragment.getEditPhotoView().setImageDrawable(null);
        }
        updateStateAfterPlayingAnimation();
      }
    });
    animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      @Override
      public void onAnimationUpdate(ValueAnimator animation) {
        float value = (float) animation.getAnimatedValue();
        myEditPhotoFragment.getEditPicturePanel().setAlpha(value);
        myEditPhotoFragment.getEditPictureGlassView().setAlpha(value);
        myBrowseFragment.getBrowsePicturePanel().setAlpha(1f - value);

        if (forStart) {
          FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)
              myBrowseFragment.getBrowsePhotoView().getLayoutParams();
          layoutParams.topMargin = (int) (myEditPhotoFragment.
              getEditPictureGlassView().getMinGlassVMargin() * value);
          layoutParams.bottomMargin = (int) (myEditPhotoFragment.
              getEditPictureGlassView().getMinGlassVMargin() * value);
          layoutParams.leftMargin = (int) (myEditPhotoFragment.
              getEditPictureGlassView().getMinGlassHMargin() * value);
          layoutParams.rightMargin = (int) (myEditPhotoFragment.
              getEditPictureGlassView().getMinGlassHMargin() * value);
          myBrowseFragment.getBrowsePhotoView().setLayoutParams(layoutParams);
        } else {
          myEditPhotoFragment.getEditPhotoView().setAlpha(value);
        }
      }
    });
    return animator;
  }

  @Override
  public void onEditingFinished(boolean ok) {
    Runnable runnable = new Runnable() {
      public void run() {
        if (!myDestroyed) {
          updateStateBeforeTransition(ActivityState.BROWSING_PHOTO);
          buildStartOrStopEditingPhotoAnimator(false).start();
        }
      }
    };
    if (ok) {
      cropAndRotatePicture(runnable);
    } else {
      runnable.run();
    }
  }

  private void cropAndRotatePicture(@NonNull final Runnable runAfter) {
    RectF glassRect = myEditPhotoFragment.getEditPictureGlassView().getGlassRect();
    ImageBrowserView editPhotoView = myEditPhotoFragment.getEditPhotoView();
    RectF rectOnContent = editPhotoView.toContentRect(glassRect);
    RectF contentBoundsRect = editPhotoView.getContentBoundsRect();
    rectOnContent.set(
        rectOnContent.left - contentBoundsRect.left,
        rectOnContent.top - contentBoundsRect.top,
        rectOnContent.right - contentBoundsRect.left,
        rectOnContent.bottom - contentBoundsRect.top);
    final RectF rect = Util.scaleRect(rectOnContent,
        (float) myTempBitmap.getWidth() / myScaledTempBitmap.getWidth());
    final float rotation = myEditPhotoFragment.getImageRotation();

    myMediaFilesHandler.post(new Runnable() {
      @Override
      public void run() {
        if (myTempBitmap == null) {
          runOnUiThread(runAfter);
          return;
        }
        if (rect.left == 0 && rect.top == 0 && rect.bottom == myTempBitmap.getHeight() &&
            rect.right == myTempBitmap.getWidth() && Math.abs(rotation) < Util.EPS) {
          runOnUiThread(runAfter);
          return;
        }
        Matrix matrix = new Matrix();
        matrix.postRotate(rotation);
        myTempBitmap = Bitmap.createBitmap(myTempBitmap, 0, 0, myTempBitmap.getWidth(),
            myTempBitmap.getHeight(), matrix, true);
        myTempBitmap = Bitmap.createBitmap(myTempBitmap, (int) rect.left, (int) rect.top,
            (int) rect.width(), (int) rect.height());
        myScaledTempBitmap = reduceBitmap(myTempBitmap);
        applyBitmapHackForSurfaceView(myScaledTempBitmap);

        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            if (!myDestroyed) {
              myBrowseFragment.getBrowsePhotoView().setImageBitmap(myScaledTempBitmap);
            }
            runAfter.run();
          }
        });
      }
    });
  }

  private static void applyBitmapHackForSurfaceView(@NonNull Bitmap bitmap) {
    final int step = 100;
    int[] pixels = new int[bitmap.getWidth() * step];

    for (int i = 0; i < bitmap.getHeight(); i += step) {
      int lineCount = Math.min(step, bitmap.getHeight() - i);
      bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, i, bitmap.getWidth(), lineCount);
      bitmap.setPixels(pixels, 0, bitmap.getWidth(), 0, i, bitmap.getWidth(), lineCount);
    }
  }

  @Override
  public void onPictureTaken(@NonNull final byte[] data) {
    if (myState != ActivityState.TAKING_PICTURE) {
      Log.e(LOG_TAG, "Illegal state: " + myState, new IllegalStateException());
      return;
    }
    myMediaFilesHandler.post(new Runnable() {
      @Override
      public void run() {
        clearTempData();
        myScaledTempBitmap = null;
        myTempBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

        if (myTempBitmap == null) {
          Log.e(LOG_TAG, "Cannot create decode photo bitmap");
        } else {
          myScaledTempBitmap = reduceBitmap(myTempBitmap);

          runOnUiThread(new Runnable() {
            @Override
            public void run() {
              if (!myDestroyed) {
                myBrowseFragment.getBrowsePhotoView().setImageBitmap(myScaledTempBitmap);
                updateStateBeforeTransition(ActivityState.BROWSING_PHOTO);
                buildStartBrowsingAnimator(false).start();
              }
            }
          });
        }
      }
    });
  }

  @Override
  public void initOutputVideoFile(@NonNull final Consumer<String> callback) {
    myMediaFilesHandler.post(new Runnable() {
      @Override
      public void run() {
        clearTempData();
        myTempVideoFile = getOutputMediaFile(true);
        final String filePath;

        if (myTempVideoFile == null) {
          Log.e(LOG_TAG, "Cannot create temp video file");
          filePath = null;
        } else {
          filePath = myTempVideoFile.getAbsolutePath();
        }
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            callback.consume(filePath);
          }
        });
      }
    });
  }

  private boolean writeBitmapToFile(@NonNull File file, @NonNull Bitmap bitmap) {
    try {
      FileOutputStream fos = new FileOutputStream(file);

      try {
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
      } finally {
        fos.close();
      }
      return true;
    } catch (FileNotFoundException e) {
      Log.e(LOG_TAG, "File not found: " + e.getMessage());
      return false;
    } catch (IOException e) {
      Log.e(LOG_TAG, "Error accessing file: " + e.getMessage());
      return false;
    }
  }

  @NonNull
  private static Bitmap reduceBitmap(@NonNull Bitmap bitmap) {
    float ratio = Math.min(MAX_BITMAP_SIZE_FOR_IMAGE_VIEW / bitmap.getWidth(),
        MAX_BITMAP_SIZE_FOR_IMAGE_VIEW / bitmap.getHeight());
    int width = Math.round(ratio * bitmap.getWidth());
    int height = Math.round(ratio * bitmap.getHeight());
    return Bitmap.createScaledBitmap(bitmap, width, height, true);
  }

  public void onBrowsingFinished(boolean ok) {
    final boolean video = myState == ActivityState.BROWSING_VIDEO;
    updateStateBeforeTransition(video ? ActivityState.CAPTURING_VIDEO : ActivityState.CAPTURING_PHOTO);

    if (ok) {
      myMediaFilesHandler.post(new Runnable() {
        @Override
        public void run() {
          File outputFile = getOutputMediaFile(video);

          if (outputFile != null && saveMediaFile(video)) {
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                Uri.fromFile(outputFile)));
          }
          clearTempData();
        }
      });
    }
    buildFinishBrowsingAnimator(video).start();
  }

  @NonNull
  private Animator buildFinishBrowsingAnimator(final boolean video) {
    ValueAnimator animator = ValueAnimator.ofFloat(1f, 0f);

    animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      @Override
      public void onAnimationUpdate(ValueAnimator animation) {
        float value = (float) animation.getAnimatedValue();
        myBrowseFragment.getBrowseOkButton().setScaleX(value);
        myBrowseFragment.getBrowseOkButton().setScaleY(value);
        myBrowseFragment.getBrowseCancelButton().setScaleX(value);
        myBrowseFragment.getBrowseCancelButton().setScaleY(value);

        if (video) {
          myBrowseFragment.getSeekBar().setAlpha(value);
          myBrowseFragment.getPlayPauseVideoButton().setScaleX(value);
          myBrowseFragment.getPlayPauseVideoButton().setScaleY(value);
        } else {
          myBrowseFragment.getCropButton().setScaleX(value);
          myBrowseFragment.getCropButton().setScaleY(value);
        }
        myMainFragment.getCaptureControlPanel().setAlpha(1f - value);
        myMainFragment.getCaptureModeIndicator().setAlpha(1f - value);

        if (!video) {
          myBrowseFragment.getBrowsePhotoViewWrapper().setAlpha(value);
        }
      }
    });
    Util.setAnimatorDuration(animator, 200);
    animator.setInterpolator(new AccelerateInterpolator());

    animator.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationStart(Animator animation) {
        showFragment(R.id.mainFragment, false);
        myBrowseFragment.getBottomPanelBlackout().setVisibility(View.GONE);
        myMainFragment.getCaptureControlPanel().setVisibility(View.VISIBLE);
        myMainFragment.getCaptureModeIndicator().setVisibility(View.VISIBLE);
        myBrowseFragment.getSurfaceView().setVisibility(View.GONE);
      }

      @Override
      public void onAnimationEnd(Animator animation) {
        showFragment(R.id.mainFragment, true);

        if (!video) {
          myBrowseFragment.getBottomPanelBlackout().setVisibility(View.VISIBLE);
        }
        myBrowseFragment.getBrowsePicturePanel().setVisibility(View.GONE);

        if (!video) {
          myBrowseFragment.getBrowsePhotoViewWrapper().setVisibility(View.GONE);
          myBrowseFragment.getBrowsePhotoView().setImageDrawable(null);
        } else {
          myBrowseFragment.getSeekBar().setAlpha(1f);
        }
        updateStateAfterPlayingAnimation();
      }
    });
    return animator;
  }

  @NonNull
  public Animator buildStartBrowsingAnimator(final boolean video) {
    ValueAnimator animator = ValueAnimator.ofFloat(0f, 1.2f, 1f);

    animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      @Override
      public void onAnimationUpdate(ValueAnimator animation) {
        float value = (float) animation.getAnimatedValue();
        myBrowseFragment.getBrowseOkButton().setScaleX(value);
        myBrowseFragment.getBrowseOkButton().setScaleY(value);
        myBrowseFragment.getBrowseCancelButton().setScaleX(value);
        myBrowseFragment.getBrowseCancelButton().setScaleY(value);

        if (video) {
          myBrowseFragment.getPlayPauseVideoButton().setScaleX(value);
          myBrowseFragment.getPlayPauseVideoButton().setScaleY(value);

          if (value >= 1f && myBrowseFragment.getSurfaceView().getVisibility() != View.VISIBLE) {
            myBrowseFragment.getSurfaceView().setVisibility(View.VISIBLE);
            myBrowseFragment.getBottomPanelBlackout().setAlpha(Util.VIDEO_BOTTOM_PANEL_ALPHA);
            myBrowseFragment.getBottomPanelBlackout().setVisibility(View.VISIBLE);
          }
        } else {
          myBrowseFragment.getCropButton().setScaleX(value);
          myBrowseFragment.getCropButton().setScaleY(value);
        }
        float alpha = Math.max(0f, 1f - value);
        myMainFragment.getCaptureControlPanel().setAlpha(alpha);
        myMainFragment.getCaptureModeIndicator().setAlpha(alpha);

        if (!video) {
          myBrowseFragment.getBrowsePhotoViewWrapper().setAlpha(Math.min(1f, value));
        }
      }
    });
    Util.setAnimatorDuration(animator, 400);
    animator.setInterpolator(new AccelerateDecelerateInterpolator());

    animator.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationStart(Animator animation) {
        showFragment(R.id.browseFragment, false);
        myBrowseFragment.getMainPanelLayout().setMatchParentHeight(video);
        myBrowseFragment.getPlayPauseVideoButton().setVisibility(video ? View.VISIBLE : View.GONE);
        myBrowseFragment.getCropButton().setVisibility(video ? View.GONE : View.VISIBLE);
        myBrowseFragment.getBottomPanelBlackout().setVisibility(View.GONE);

        if (video) {
          myBrowseFragment.getSeekBar().setVisibility(View.VISIBLE);
          myBrowseFragment.getSeekBar().setProgress(0f);
        } else {
          myBrowseFragment.getBrowsePhotoViewWrapper().setVisibility(View.VISIBLE);
          myBrowseFragment.getSeekBar().setVisibility(View.GONE);
        }
        myBrowseFragment.getBrowsePicturePanel().setVisibility(View.VISIBLE);
      }

      @Override
      public void onAnimationEnd(Animator animation) {
        showFragment(R.id.browseFragment, true);

        if (!video) {
          myBrowseFragment.getSurfaceView().setVisibility(View.GONE);
          myBrowseFragment.getBottomPanelBlackout().setAlpha(1f);
          myBrowseFragment.getBottomPanelBlackout().setVisibility(View.VISIBLE);
        }
        myMainFragment.getCaptureControlPanel().setVisibility(View.GONE);
        myMainFragment.getCaptureControlPanel().setAlpha(1f);
        myMainFragment.getCaptureModeIndicator().setVisibility(View.GONE);
        myMainFragment.getCaptureModeIndicator().setAlpha(1f);
        updateStateAfterPlayingAnimation();
      }
    });
    return animator;
  }

  public void onVideoRecordingFinished(boolean success) {
    if (!success) {
      updateState(ActivityState.CAPTURING_VIDEO);
    } else {
      myBrowseFragment.showVideoPlayer(myTempVideoFile.getAbsolutePath());
      buildStartBrowsingAnimator(true).start();
    }
  }

  private boolean saveMediaFile(boolean video) {
    File outputFile = getOutputMediaFile(video);

    if (outputFile == null) {
      return false;
    }
    if (video) {
      return myTempVideoFile != null && Util.copyFile(myTempVideoFile, outputFile);
    } else {
      return writeBitmapToFile(outputFile, myTempBitmap);
    }
  }

  @Nullable
  private static File getOutputMediaFile(boolean video) {
    String type = video ? Environment.DIRECTORY_MOVIES : Environment.DIRECTORY_PICTURES;
    File dir = new File(Environment.getExternalStoragePublicDirectory(type), "TelegramChallenge");

    if (!dir.exists() && !dir.mkdirs()) {
      Log.e(LOG_TAG, "failed to create directory");
      return null;
    }
    String suffix = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

    return video
        ? new File(dir.getPath() + File.separator + "VID_" + suffix + ".mp4")
        : new File(dir.getPath() + File.separator + "IMG_" + suffix + ".jpg");
  }

  @SuppressWarnings("ResultOfMethodCallIgnored")
  private void clearTempData() {
    if (myTempBitmap != null) {
      myTempBitmap = null;
    }
    if (myScaledTempBitmap != null) {
      myScaledTempBitmap = null;
    }
    if (myTempVideoFile != null) {
      myTempVideoFile.delete();
      myTempVideoFile = null;
    }
  }

  @Override
  public int getUiOrientationInDegrees() {
    return myAppliedOrientationInDegrees;
  }

  @Override
  public int getSensorOrientationInDegrees() {
    return myCurrentOrientationInDegrees;
  }
}
