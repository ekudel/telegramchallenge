package com.kubro.telegramchallenge;

import android.support.annotation.NonNull;

/**
 * @author Eugene.Kudelevsky
 */
public interface CameraHelperCallback {
  boolean requestCameraPermissionIfNeeded();

  void onPictureTaken(@NonNull byte[] data);
}
