package com.kubro.telegramchallenge;

import android.graphics.SurfaceTexture;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Surface;

import com.kubro.telegramchallenge.util.Consumer;
import com.kubro.telegramchallenge.util.Disposable;
import com.kubro.telegramchallenge.util.Size;

import java.io.IOException;
import java.util.List;

/**
 * @author Eugene.Kudelevsky
 */
public abstract class CameraHelperBase {
  private static final String LOG_TAG = CameraHelperBase.class.getSimpleName();

  @NonNull protected final HandlerThread myBackgroundThread;
  @NonNull protected final Handler myBackgroundHandler;
  @NonNull protected final CameraHelperCallback myCallback;
  protected MediaRecorder myMediaRecorder;
  private volatile long myRecordingStartTime;

  public CameraHelperBase(@NonNull CameraHelperCallback callback) {
    myCallback = callback;
    myBackgroundThread = new HandlerThread("Camera");
    myBackgroundThread.start();
    myBackgroundHandler = new Handler(myBackgroundThread.getLooper());
  }

  public void release() {
    myBackgroundHandler.removeCallbacks(null);
    myBackgroundThread.quit();
  }

  @SuppressWarnings("SuspiciousNameCombination")
  @Nullable
  protected static Size computeBestPreviewSize(@NonNull List<Size> sizes, int width, int height) {
    if (sizes.isEmpty()) {
      return null;
    }
    if (width > height) {
      int t = width;
      width = height;
      height = t;
    }
    final double aspectTolerance = 0.1;
    double targetRatio = (double) width / height;
    Size result = null;
    int delta = Integer.MAX_VALUE;

    for (Size size : sizes) {
      int sWidth = Math.min(size.getWidth(), size.getHeight());
      int sHeight = Math.max(size.getWidth(), size.getHeight());
      double ratio = (double) sWidth / sHeight;

      if (Math.abs(ratio - targetRatio) <= aspectTolerance && Math.abs(sHeight - height) < delta) {
        result = size;
        delta = Math.abs(sHeight - height);
      }
    }
    if (result == null) {
      delta = Integer.MAX_VALUE;

      for (Size size : sizes) {
        int sHeight = Math.max(size.getWidth(), size.getHeight());

        if (Math.abs(sHeight - height) < delta) {
          result = size;
          delta = Math.abs(sHeight - height);
        }
      }
    }
    return result;
  }

  @Nullable
  protected static Size computeBestPictureSize(@NonNull List<Size> sizes) {
    if (sizes.isEmpty()) {
      return null;
    }
    final double targetRatio = Util.PHOTO_RATIO;
    Size bestSize = null;
    Size maxSize = null;

    for (Size size : sizes) {
      double ratio1 = (double) size.getHeight() / size.getWidth();
      double ratio2 = (double) size.getWidth() / size.getHeight();

      if ((Math.abs(ratio1 - targetRatio) < Util.EPS || Math.abs(ratio2 - targetRatio) < Util.EPS) &&
          (bestSize == null || size.getHeight() > bestSize.getHeight())) {
        bestSize = size;
      }
      if (maxSize == null || size.getHeight() > maxSize.getHeight()) {
        maxSize = size;
      }
    }
    return bestSize != null ? bestSize : maxSize;
  }

  protected void releaseMediaRecorder() {
    if (myMediaRecorder != null) {
      myRecordingStartTime = 0;
      myMediaRecorder.stop();
      myMediaRecorder.release();
      myMediaRecorder = null;
    }
  }

  protected boolean doStopVideoRecording() {
    if (myMediaRecorder != null) {
      myRecordingStartTime = 0;
      try {
        myMediaRecorder.stop();
      } catch (Exception e) {
        Log.d(LOG_TAG, "can't stop", e);
        return false;
      }
      myMediaRecorder.release();
      myMediaRecorder = null;
    }
    return true;
  }

  protected boolean prepareMediaRecorder(@NonNull MediaRecorder mediaRecorder) {
    try {
      mediaRecorder.prepare();
    } catch (IOException e) {
      Log.e(LOG_TAG, "cannot prepare media recorder", e);
      myMediaRecorder = null;
      return false;
    }
    myMediaRecorder = mediaRecorder;
    return true;
  }

  protected void doStartVideoRecording() {
    myMediaRecorder.start();
    myRecordingStartTime = System.currentTimeMillis();
  }

  @Nullable
  public abstract Disposable resetCamera(int previewSurfaceWidth, int previewSurfaceHeight, @NonNull CameraType cameraType,
                                         int displayRotation, @Nullable FlashMode flashMode, boolean forVideo, boolean force,
                                         @NonNull Runnable runAfter);

  public abstract void setupFocusMode(boolean video);

  public abstract void setFlashMode(@NonNull FlashMode flashMode);

  public abstract void setDisplayRotation(int displayRotation);

  public abstract void setSurfaceTexture(@Nullable SurfaceTexture texture);

  public abstract void closeCamera();

  public abstract void changePreviewSize(final int width, final int height, @NonNull Runnable runAfter);

  public abstract void getSupportedFlashModes(@NonNull Consumer<List<FlashMode>> consumer);

  public long getRecordingStartTime() {
    return myRecordingStartTime;
  }

  public abstract void startVideoRecording(@NonNull Surface surface, int displayRotation, @NonNull String outputVideoFilePath,
                                           @NonNull Consumer<Boolean> consumer);

  public abstract void stopVideoRecording(Consumer<Boolean> consumer);

  public abstract void takePictureWhenFocused();
}
