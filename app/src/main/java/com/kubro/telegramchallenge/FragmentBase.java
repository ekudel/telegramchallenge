package com.kubro.telegramchallenge;

import android.animation.Animator;
import android.app.Activity;
import android.app.Fragment;
import android.support.annotation.Nullable;

/**
 * @author Eugene.Kudelevsky
 */
public class FragmentBase extends Fragment {
  public boolean onBackPressed() {
    return false;
  }

  public boolean canHandleOrientationChange() {
    return false;
  }

  @Nullable
  public Animator buildOrientationChangeAnimator(int startDegrees, int deltaDegrees) {
    return null;
  }

  public void setUiOrientation(int degrees) {
  }

  public void onSensorOrientationChanged(int degrees) {
  }

  @Override
  public void onHiddenChanged(boolean hidden) {
    super.onHiddenChanged(hidden);

    if (!hidden) {
      Activity activity = getActivity();

      if (activity instanceof OrientationAwareActivity) {
        setUiOrientation(((OrientationAwareActivity) activity).getUiOrientationInDegrees());
      }
    }
  }
}
