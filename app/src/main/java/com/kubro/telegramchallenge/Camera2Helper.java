package com.kubro.telegramchallenge;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaRecorder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Size;
import android.view.Surface;

import com.kubro.telegramchallenge.util.Consumer;
import com.kubro.telegramchallenge.util.Disposable;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * @author Eugene.Kudelevsky
 */
@TargetApi(21)
public class Camera2Helper extends CameraHelperBase {
  private static final String LOG_TAG = Camera2Helper.class.getSimpleName();

  private final CameraManager myCameraManager;
  private final Semaphore myCameraOpenCloseLock = new Semaphore(1);
  private SurfaceTexture mySurfaceTexture;

  private CameraDevice myCameraDevice;
  private CameraCaptureSession myPreviewSession;
  private ImageReader myImageReader;
  private CaptureRequest.Builder myPreviewRequestBuilder;

  private Surface myPreviewSurface;
  private Surface myRecordingSurface;
  private CameraCharacteristics myCameraCharacteristics;
  private int myDisplayRotation = 0;
  private FlashMode myFlashMode = FlashMode.OFF;

  public Camera2Helper(@NonNull Context context, @NonNull CameraHelperCallback callback) {
    super(callback);
    myCameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
  }

  @Nullable
  public Disposable resetCamera(final int previewSurfaceWidth, final int previewSurfaceHeight,
                                @NonNull final CameraType cameraType, final int displayRotation, @Nullable final FlashMode flashMode,
                                final boolean forVideo, final boolean force, @NonNull final Runnable runAfter) {
    String cameraApiId = getCameraApiId(cameraType);

    if (cameraApiId == null) {
      runAfter.run();
      return null;
    }
    myDisplayRotation = displayRotation;
    myFlashMode = flashMode;
    closeCamera();
    setupAndOpenCamera(cameraApiId, previewSurfaceWidth, previewSurfaceHeight, forVideo, runAfter);

    return new Disposable() {
      @Override
      public void dispose() {
      }
    };
  }

  @Override
  public void setupFocusMode(boolean video) {
    int focusMode = chooseFocusMode(video);

    if (focusMode >= 0) {
      myPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, focusMode);
      passPreviewCaptureRequest(null);
    } else {
      Log.e(LOG_TAG, "Cannot choose focus mode");
    }
  }

  @Override
  public void setFlashMode(@NonNull FlashMode flashMode) {
    myFlashMode = flashMode;
  }

  @Override
  public void setDisplayRotation(int displayRotation) {
    myDisplayRotation = displayRotation;
  }

  private void setupAndOpenCamera(@NonNull final String cameraId, final int previewSurfaceWidth, final int previewSurfaceHeight,
                                  final boolean forVideo, @NonNull final Runnable runAfter) {
    if (!myCallback.requestCameraPermissionIfNeeded()) {
      return;
    }
    try {
      if (!myCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
        throw new RuntimeException("Time out waiting to lock camera opening");
      }
      final Runnable runAfterWrapper = new Runnable() {
        private boolean myFlag = false;

        @Override
        public void run() {
          if (!myFlag) {
            myFlag = true;
            runAfter.run();
          }
        }
      };
      myCameraCharacteristics = myCameraManager.getCameraCharacteristics(cameraId);
      //noinspection MissingPermission
      myCameraManager.openCamera(cameraId, new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
          myCameraOpenCloseLock.release();
          myCameraDevice = camera;
          configureCameraAndStartCaptureSession(previewSurfaceWidth, previewSurfaceHeight, forVideo, runAfterWrapper);
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
          myCameraOpenCloseLock.release();
          camera.close();
          myCameraDevice = null;
          runAfterWrapper.run();
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
          myCameraOpenCloseLock.release();
          camera.close();
          myCameraDevice = null;
          Log.e(LOG_TAG, "Serious error when opening camera: " + error);
          runAfterWrapper.run();
        }
      }, null);
    } catch (CameraAccessException e) {
      Log.e(LOG_TAG, "Cannot open camera", e);
    } catch (InterruptedException e) {
      throw new RuntimeException("Interrupted while trying to lock camera opening", e);
    }
  }

  @Override
  public void changePreviewSize(int width, int height, @NonNull Runnable runAfter) {
    doSetupPreviewSize(width, height);
    restartPreviewSession(runAfter);
  }

  private void doSetupPictureSize() {
    StreamConfigurationMap map = myCameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

    if (map == null) {
      Log.e(LOG_TAG, "Cannot get SCALER_STREAM_CONFIGURATION_MAP for picture for camera " + myCameraDevice.getId());
      return;
    }
    Size[] supportedPreviewSizes = map.getOutputSizes(ImageFormat.JPEG);
    com.kubro.telegramchallenge.util.Size bestPictureSize = computeBestPictureSize(toLegacySizes(Arrays.asList(supportedPreviewSizes)));

    if (bestPictureSize != null) {
      myImageReader = ImageReader.newInstance(bestPictureSize.getWidth(), bestPictureSize.getHeight(), ImageFormat.JPEG, 1);
      myImageReader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
          Image image = reader.acquireNextImage();
          byte[] bytes;
          try {
            ByteBuffer buffer = image.getPlanes()[0].getBuffer();
            bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
          } finally {
            image.close();
          }
          myCallback.onPictureTaken(bytes);
        }
      }, null);
    } else {
      myImageReader = null;
      Log.e(LOG_TAG, "Cannot compute best picture size");
    }
  }

  private void doSetupPreviewSize(int surfaceWidth, int surfaceHeight) {
    StreamConfigurationMap map = myCameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

    if (map == null) {
      Log.e(LOG_TAG, "Cannot get SCALER_STREAM_CONFIGURATION_MAP for preview for camera " + myCameraDevice.getId());
      return;
    }
    Size[] supportedPreviewSizes = map.getOutputSizes(SurfaceTexture.class);
    com.kubro.telegramchallenge.util.Size bestPreviewSize = computeBestPreviewSize(
        toLegacySizes(Arrays.asList(supportedPreviewSizes)), surfaceWidth, surfaceHeight);

    if (bestPreviewSize != null) {
      mySurfaceTexture.setDefaultBufferSize(bestPreviewSize.getWidth(), bestPreviewSize.getHeight());
    } else {
      Log.e(LOG_TAG, "Cannot compute best preview size");
    }
  }

  @NonNull
  private List<com.kubro.telegramchallenge.util.Size> toLegacySizes(@NonNull List<Size> sizes) {
    List<com.kubro.telegramchallenge.util.Size> result = new ArrayList<>();

    for (Size size : sizes) {
      result.add(new com.kubro.telegramchallenge.util.Size(size.getWidth(), size.getHeight()));
    }
    return result;
  }

  @Override
  public void getSupportedFlashModes(@NonNull Consumer<List<FlashMode>> consumer) {
    consumer.consume(getSupportedFlashModes());
  }

  @Override
  public void startVideoRecording(@NonNull Surface surface, int displayRotation, @NonNull String outputFilePath,
                                  @NonNull final Consumer<Boolean> consumer) {
    if (myPreviewSession != null) {
      myPreviewSession.close();
      myPreviewSession = null;
    }
    MediaRecorder mediaRecorder = new MediaRecorder();
    mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
    mediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
    mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
    mediaRecorder.setVideoEncodingBitRate(10000000);
    mediaRecorder.setVideoFrameRate(30);
    mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
    mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
    Size videoSize = chooseVideoSize();

    if (videoSize != null) {
      mediaRecorder.setVideoSize(videoSize.getWidth(), videoSize.getHeight());
    }
    mediaRecorder.setOrientationHint(computeCameraRotation());
    mediaRecorder.setOutputFile(outputFilePath);
    final boolean success = prepareMediaRecorder(mediaRecorder);

    if (!success) {
      consumer.consume(false);
    } else {
      restartPreviewSession(new Runnable() {
        @Override
        public void run() {
          doStartVideoRecording();
          consumer.consume(true);
        }
      });
    }
  }

  @Nullable
  private Size chooseVideoSize() {
    StreamConfigurationMap map = myCameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

    if (map == null) {
      return null;
    }
    Size[] outputSizes = map.getOutputSizes(MediaRecorder.class);

    for (Size size : outputSizes) {
      if (size.getWidth() == size.getHeight() * Util.VIDEO_RATIO && size.getWidth() <= 1080) {
        return size;
      }
    }
    return outputSizes[outputSizes.length - 1];
  }

  @Override
  public void stopVideoRecording(final Consumer<Boolean> consumer) {
    if (myPreviewSession != null) {
      myPreviewSession.close();
      myPreviewSession = null;
    }
    consumer.consume(doStopVideoRecording());
  }

  @Override
  public void takePictureWhenFocused() {
    try {
      myPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START);

      myPreviewSession.capture(myPreviewRequestBuilder.build(), new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
          // todo: pre-capture sequence
          captureStillPicture();
        }
      }, null);
    } catch (CameraAccessException e) {
      Log.e(LOG_TAG, "Cannot autofocus", e);
    }
  }

  private void captureStillPicture() {
    try {
      final CaptureRequest.Builder requestBuilder = myCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
      requestBuilder.addTarget(myImageReader.getSurface());
      requestBuilder.set(CaptureRequest.CONTROL_AF_MODE, myPreviewRequestBuilder.get(CaptureRequest.CONTROL_AF_MODE));

      if (myFlashMode != null) {
        requestBuilder.set(CaptureRequest.FLASH_MODE, getApiFlashMode(myFlashMode));
      }
      requestBuilder.set(CaptureRequest.JPEG_ORIENTATION, computeCameraRotation());
      myPreviewSession.stopRepeating();

      myPreviewSession.capture(requestBuilder.build(), new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
          myPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
          try {
            myPreviewSession.capture(myPreviewRequestBuilder.build(), null, null);
            restartPreviewSession(null);
          } catch (CameraAccessException e) {
            Log.e(LOG_TAG, "Cannot unlock focus", e);
          }
        }
      }, null);
    } catch (CameraAccessException e) {
      Log.e(LOG_TAG, "Cannot capture still image", e);
    }
  }

  private int computeCameraRotation() {
    if (myDisplayRotation < 0) {
      return 0;
    }
    int rotation = (myDisplayRotation + 45) / 90 * 90;
    Integer facing = myCameraCharacteristics.get(CameraCharacteristics.LENS_FACING);
    Integer orientation = myCameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);

    if (orientation == null || facing == null) {
      return 0;
    }
    return facing == CameraCharacteristics.LENS_FACING_FRONT
        ? (orientation - rotation + 360) % 360
        : (orientation + rotation) % 360;
  }

  @NonNull
  private List<FlashMode> getSupportedFlashModes() {
    if (myCameraDevice == null) {
      return Collections.emptyList();
    }
    int[] modes = myCameraCharacteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_MODES);

    if (modes == null) {
      return Collections.emptyList();
    }
    List<FlashMode> result = new ArrayList<>();

    for (FlashMode mode : FlashMode.values()) {
      if (Util.contains(modes, getApiFlashMode(mode))) {
        result.add(mode);
      }
    }
    return result;
  }

  @Override
  public void setSurfaceTexture(@Nullable SurfaceTexture texture) {
    mySurfaceTexture = texture;
  }

  @Override
  public void closeCamera() {
    try {
      myCameraOpenCloseLock.acquire();

      if (myPreviewSession != null) {
        myPreviewSession.close();
        myPreviewSession = null;
      }
      if (myCameraDevice != null) {
        myCameraDevice.close();
        myCameraDevice = null;
      }
      if (myImageReader != null) {
        myImageReader.close();
        myImageReader = null;
      }
      myCameraCharacteristics = null;
    } catch (InterruptedException e) {
      throw new RuntimeException("Interrupted while trying to lock camera closing", e);
    } finally {
      myCameraOpenCloseLock.release();
    }
  }


  private void configureCameraAndStartCaptureSession(int previewSurfaceWidth, int previewSurfaceHeight, final boolean forVideo,
                                                     @NonNull final Runnable runAfter) {
    try {
      myPreviewRequestBuilder = myCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
    } catch (CameraAccessException e) {
      Log.e(LOG_TAG, "Cannot create preview request", e);
      runAfter.run();
      return;
    }
    int focusMode = chooseFocusMode(forVideo);

    if (focusMode >= 0) {
      myPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, focusMode);
    }
    doSetupPictureSize();
    doSetupPreviewSize(previewSurfaceWidth, previewSurfaceHeight);
    restartPreviewSession(runAfter);
  }

  private void restartPreviewSession(@Nullable final Runnable runAfter) {
    final Runnable runAfterWrapper = new Runnable() {
      private boolean myFlag = false;

      @Override
      public void run() {
        if (!myFlag) {
          myFlag = true;

          if (runAfter != null) {
            runAfter.run();
          }
        }
      }
    };
    if (myPreviewSession != null) {
      myPreviewSession.close();
      myPreviewSession = null;
    }
    List<Surface> allSurfaces = new ArrayList<>();

    if (myPreviewSurface != null) {
      myPreviewRequestBuilder.removeTarget(myPreviewSurface);
    }
    myPreviewSurface = new Surface(mySurfaceTexture);
    myPreviewRequestBuilder.addTarget(myPreviewSurface);
    allSurfaces.add(myPreviewSurface);

    if (myRecordingSurface != null) {
      myPreviewRequestBuilder.removeTarget(myRecordingSurface);
      myRecordingSurface = null;
    }
    if (myMediaRecorder != null) {
      myRecordingSurface = myMediaRecorder.getSurface();
      myPreviewRequestBuilder.addTarget(myRecordingSurface);
      allSurfaces.add(myRecordingSurface);
    }
    allSurfaces.add(myImageReader.getSurface());

    try {
      myCameraDevice.createCaptureSession(allSurfaces, new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(@NonNull CameraCaptureSession session) {
          myPreviewSession = session;
          passPreviewCaptureRequest(runAfterWrapper);
        }

        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
          runAfterWrapper.run();
          Log.e(LOG_TAG, "Configuring capture session failed");
        }
      }, null);
    } catch (CameraAccessException e) {
      Log.e(LOG_TAG, "Cannot create capture session", e);
    }
  }

  private void passPreviewCaptureRequest(@Nullable final Runnable runAfter) {
    if (myPreviewSession == null || myPreviewRequestBuilder == null) {
      if (runAfter != null) {
        runAfter.run();
      }
      return;
    }
    try {
      myPreviewSession.setRepeatingRequest(myPreviewRequestBuilder.build(), new CameraCaptureSession.CaptureCallback() {
        private boolean myFlag = false;

        @Override
        public void onCaptureStarted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, long timestamp,
                                     long frameNumber) {
          if (!myFlag && runAfter != null) {
            myFlag = true;
            runAfter.run();
          }
        }
      }, null);
    } catch (CameraAccessException e) {
      Log.e(LOG_TAG, "Cannot pass preview capture request", e);
    }
  }

  @Nullable
  private String getCameraApiId(@NonNull CameraType cameraType) {
    int cameraFacing = getCameraFacing(cameraType);

    try {
      for (String cameraId : myCameraManager.getCameraIdList()) {
        CameraCharacteristics characteristics = myCameraManager.getCameraCharacteristics(cameraId);
        Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);

        if (facing != null && facing == cameraFacing) {
          return cameraId;
        }
      }
    } catch (CameraAccessException e) {
      Log.e(LOG_TAG, "Cannot access camera", e);
    }
    return null;
  }

  private static int getApiFlashMode(@NonNull FlashMode flashMode) {
    switch (flashMode) {
      case AUTO:
        return CameraMetadata.CONTROL_AE_MODE_ON_AUTO_FLASH;
      case ON:
        return CameraMetadata.CONTROL_AE_MODE_ON;
      case OFF:
        return CameraMetadata.CONTROL_AE_MODE_OFF;
      default:
        Log.e(LOG_TAG, "unknown flash mode " + flashMode);
        return CameraMetadata.CONTROL_AE_MODE_OFF;
    }
  }

  private static int getCameraFacing(@NonNull CameraType type) {
    switch (type) {
      case BACK:
        return CameraCharacteristics.LENS_FACING_BACK;
      case FRONT:
        return CameraCharacteristics.LENS_FACING_FRONT;
      default:
        Log.e(LOG_TAG, "unknown camera type " + type);
        return Camera.CameraInfo.CAMERA_FACING_FRONT;
    }
  }

  private int chooseFocusMode(boolean forVideo) {
    if (myCameraDevice == null) {
      return -1;
    }
    int[] supportedFocusModes = myCameraCharacteristics.get(CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES);

    if (supportedFocusModes != null && supportedFocusModes.length > 0) {
      if (forVideo) {
        return doChooseFocusMode(supportedFocusModes, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_VIDEO, CaptureRequest.CONTROL_AF_MODE_OFF);
      } else {
        return doChooseFocusMode(supportedFocusModes, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE, CaptureRequest.CONTROL_AF_MODE_AUTO,
            CaptureRequest.CONTROL_AF_MODE_OFF);
      }
    }
    return -1;
  }

  private static int doChooseFocusMode(@NonNull int[] supportedFocusModes, @NonNull int... focusModes) {
    for (int focusMode : focusModes) {
      for (int focusMode1 : supportedFocusModes) {
        if (focusMode == focusMode1) {
          return focusMode;
        }
      }
    }
    return -1;
  }
}
