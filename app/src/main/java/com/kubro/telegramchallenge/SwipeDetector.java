package com.kubro.telegramchallenge;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * @author Eugene.Kudelevsky
 */
public abstract class SwipeDetector implements View.OnTouchListener {
  private static final int SWIPE_THRESHOLD = 100;
  private static final int SWIPE_VELOCITY_THRESHOLD = 100;

  private final GestureDetector myGestureDetector;

  public SwipeDetector(@NonNull Context context) {
    myGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
      @Override
      public boolean onDown(MotionEvent e) {
        return true;
      }

      @Override
      public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        float diffY = e2.getY() - e1.getY();
        float diffX = e2.getX() - e1.getX();

        if (Math.abs(diffX) > Math.abs(diffY) && Math.abs(diffX) > SWIPE_THRESHOLD &&
            Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
          if (diffX > 0) {
            onSwipeRight();
          } else {
            onSwipeLeft();
          }
          return true;
        }
        return false;
      }
    });
  }

  protected abstract void onSwipeRight();

  protected abstract void onSwipeLeft();

  @Override
  public boolean onTouch(View v, MotionEvent event) {
    return myGestureDetector.onTouchEvent(event);
  }

  public void install(@NonNull View view) {
    view.setOnTouchListener(this);
  }
}
