package com.kubro.telegramchallenge.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.kubro.telegramchallenge.R;
import com.kubro.telegramchallenge.util.Size;
import com.kubro.telegramchallenge.Util;

/**
 * @author Eugene.Kudelevsky
 */
public class MainPanelLayout extends RelativeLayout {
  private int myMinButtonPanelHeight;
  private boolean myMatchParentHeight;

  public MainPanelLayout(Context context) {
    super(context);
  }

  public void setMatchParentHeight(boolean matchParentHeight) {
    if (myMatchParentHeight != matchParentHeight) {
      myMatchParentHeight = matchParentHeight;
      requestLayout();
    }
  }

  public MainPanelLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
    myMinButtonPanelHeight = context.getResources().
        getDimensionPixelSize(R.dimen.min_photo_bottom_panel_height);
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    if (myMatchParentHeight) {
      super.onMeasure(widthMeasureSpec, heightMeasureSpec);
      return;
    }
    Size size = computeSize(widthMeasureSpec, heightMeasureSpec, myMinButtonPanelHeight);
    super.onMeasure(MeasureSpec.makeMeasureSpec(size.getWidth(), MeasureSpec.EXACTLY),
        MeasureSpec.makeMeasureSpec(size.getHeight(), MeasureSpec.EXACTLY));
  }

  @NonNull
  public static Size computeSize(int widthMeasureSpec, int heightMeasureSpec,
                                 int minButtonPanelHeight) {
    int width = MeasureSpec.getSize(widthMeasureSpec);
    int maxHeight = Math.max(0, MeasureSpec.getSize(heightMeasureSpec) - minButtonPanelHeight);
    int height = Math.min(maxHeight, (int) (width * Util.PHOTO_RATIO));
    return new Size(width, height);
  }
}