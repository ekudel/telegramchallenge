package com.kubro.telegramchallenge.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author Eugene.Kudelevsky
 */
public class SimpleSeekBar extends View {
  private final Paint myPaint = new Paint();
  private float myProgress = 0f;

  public SimpleSeekBar(Context context, AttributeSet attrs) {
    super(context, attrs);
    myPaint.setStyle(Paint.Style.FILL);
    myPaint.setColor(Color.WHITE);
  }

  public void setProgress(float progress) {
    myProgress = progress;
    invalidate();
  }

  @Override
  protected void onDraw(Canvas canvas) {
    canvas.drawRect(0, 0, canvas.getWidth() * myProgress, canvas.getHeight(), myPaint);
  }
}
