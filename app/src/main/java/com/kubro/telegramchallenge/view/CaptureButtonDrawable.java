package com.kubro.telegramchallenge.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;

import com.kubro.telegramchallenge.R;
import com.kubro.telegramchallenge.Util;

/**
 * @author Eugene.Kudelevsky
 */
public class CaptureButtonDrawable extends Drawable {
  private final Context myContext;

  private final Paint myShape1Paint = new Paint();
  private float myShape1Width;
  private float myShape1Radius;

  private final Paint myShape2Paint = new Paint();
  private int myShape2Alpha;
  private float myExtRingWith;

  private final Paint myShape3Paint = new Paint();
  private float myShape3Radius;

  private final Paint myShape4Paint = new Paint();
  private float myShape4Radius;

  private final Paint myShape5Paint = new Paint();
  private final float myShape5Radius;
  private float myShape5Size;

  private final Paint myShape6Paint = new Paint();
  private float myShape6Radius;

  private final int myInsets;
  private final int myWidth;
  private final int myHeight;
  private final RectF myTempRect = new RectF();
  private final Path myTempPath = new Path();
  private boolean myDrawShape6BehindShape5 = false;

  @NonNull private final CapturingPhotoState myCapturingPhotoState;
  @NonNull private final CapturingVideoState myCapturingVideoState;
  @NonNull private final RecordingVideoState myRecordingVideoState;

  public CaptureButtonDrawable(@NonNull Context context) {
    myContext = context;
    Resources resources = context.getResources();
    myWidth = resources.getDimensionPixelSize(R.dimen.capture_button_width);
    myHeight = resources.getDimensionPixelSize(R.dimen.capture_button_height);
    myInsets = resources.getDimensionPixelSize(R.dimen.capture_button_insets);

    myShape1Paint.setColor(resources.getColor(R.color.capture_button1));
    myShape1Paint.setStyle(Paint.Style.FILL);
    myShape1Paint.setAntiAlias(true);

    myShape2Paint.setColor(resources.getColor(R.color.capture_button2));
    myShape2Paint.setStyle(Paint.Style.FILL);
    myShape2Paint.setAntiAlias(true);

    myShape3Paint.setColor(resources.getColor(R.color.capture_button1));
    myShape3Paint.setStyle(Paint.Style.FILL);
    myShape3Paint.setAntiAlias(true);

    myShape4Paint.setColor(resources.getColor(R.color.capture_button4));
    myShape4Paint.setStyle(Paint.Style.FILL);
    myShape4Paint.setAntiAlias(true);

    myShape5Paint.setColor(resources.getColor(R.color.capture_button1));
    myShape5Paint.setStyle(Paint.Style.FILL);
    myShape5Paint.setAntiAlias(true);
    myShape5Radius = resources.getDimension(R.dimen.capture_button_shape5_radius);

    myShape6Paint.setColor(resources.getColor(R.color.capture_button3));
    myShape6Paint.setStyle(Paint.Style.FILL);
    myShape6Paint.setAntiAlias(true);

    myCapturingPhotoState = new CapturingPhotoState();
    myCapturingVideoState = new CapturingVideoState();
    myRecordingVideoState = new RecordingVideoState();
    myCapturingPhotoState.init();
  }

  public int getShape2Alpha() {
    return myShape2Alpha;
  }

  public float getMaxExtRingWidth() {
    return (float) myHeight / 2 - myShape6Radius;
  }

  @Override
  public int getIntrinsicWidth() {
    return myWidth + myInsets * 2;
  }

  @Override
  public int getIntrinsicHeight() {
    return myHeight + myInsets * 2;
  }

  @Override
  public void setAlpha(int alpha) {
  }

  @Override
  public void setColorFilter(ColorFilter colorFilter) {
  }

  @Override
  public int getOpacity() {
    return PixelFormat.TRANSLUCENT;
  }

  @Override
  public void draw(Canvas canvas) {
    Rect bounds = getBounds();
    float cx = (bounds.left + bounds.right) / 2;
    float cy = (bounds.top + bounds.bottom) / 2;

    if (myShape1Width > Util.EPS && myShape1Radius > Util.EPS) {
      float shape1Left = cx - myShape1Width / 2;
      float shape1Right = cx + myShape1Width / 2;
      float shape1Top = cy - myShape1Radius;
      float shape1Bottom = cy + myShape1Radius;
      float angle;

      if (myShape1Width > 2 * myShape1Radius - Util.EPS) {
        angle = 90f;
      } else {
        angle = (float) Math.toDegrees(Math.acos(1 - (double) myShape1Width / 2 / myShape1Radius));
      }
      myTempPath.reset();
      myTempRect.set(shape1Left, shape1Top, shape1Left + myShape1Radius * 2, shape1Bottom);
      myTempPath.arcTo(myTempRect, 180f - angle, 2 * angle, true);
      myTempRect.set(shape1Right - myShape1Radius * 2, shape1Top, shape1Right, shape1Bottom);
      myTempPath.arcTo(myTempRect, -angle, 2 * angle);
      canvas.drawPath(myTempPath, myShape1Paint);

      myShape2Paint.setAlpha(myShape2Alpha);
      float r1 = myShape1Radius - myExtRingWith;

      if (myShape1Width > myExtRingWith * 2 && r1 > Util.EPS) {
        myTempPath.reset();
        myTempRect.set(shape1Left + myExtRingWith, shape1Top + myExtRingWith,
            shape1Left + myExtRingWith + 2 * r1, shape1Bottom - myExtRingWith);
        myTempPath.arcTo(myTempRect, 180f - angle, 2 * angle, true);
        myTempRect.set(shape1Right - myExtRingWith - r1 * 2, shape1Top + myExtRingWith,
            shape1Right - myExtRingWith, shape1Bottom - myExtRingWith);
        myTempPath.arcTo(myTempRect, -angle, 2 * angle);
        canvas.drawPath(myTempPath, myShape2Paint);
      }
    }
    if (myShape3Radius > Util.EPS) {
      canvas.drawCircle(cx, cy, myShape3Radius, myShape3Paint);
    }
    if (myShape4Radius > Util.EPS) {
      canvas.drawCircle(cx, cy, myShape4Radius, myShape4Paint);
    }
    if (myDrawShape6BehindShape5 && myShape6Radius > Util.EPS) {
      canvas.drawCircle(cx, cy, myShape6Radius, myShape6Paint);
    }
    if (myShape5Size > Util.EPS) {
      float shape5HalfSize = myShape5Size / 2;
      myTempRect.set(cx - shape5HalfSize, cy - shape5HalfSize, cx + shape5HalfSize, cy + shape5HalfSize);
      canvas.drawRoundRect(myTempRect, myShape5Radius, myShape5Radius, myShape5Paint);
    }
    if (!myDrawShape6BehindShape5 && myShape6Radius > Util.EPS) {
      canvas.drawCircle(cx, cy, myShape6Radius, myShape6Paint);
    }
  }

  @NonNull
  public CapturingPhotoState getCapturingPhotoState() {
    return myCapturingPhotoState;
  }

  @NonNull
  public CapturingVideoState getCapturingVideoState() {
    return myCapturingVideoState;
  }

  @NonNull
  public RecordingVideoState getRecordingVideoState() {
    return myRecordingVideoState;
  }

  class State {
    protected void init() {
      myDrawShape6BehindShape5 = false;
    }
  }

  public class CapturingPhotoState extends State {
    private final float myDefaultShape1Width;
    private final float myDefaultShape1Radius;
    private final float myDefaultExtRingWith;
    private final float myDefaultShape3Radius;
    private final float myDefaultShape6Radius;
    private final float myMiddleShape4Radius;

    public CapturingPhotoState() {
      //noinspection SuspiciousNameCombination
      myDefaultShape1Width = myHeight;
      Resources resources = myContext.getResources();
      myDefaultShape3Radius = resources.getDimension(R.dimen.capture_button_capture_photo_shape3_radius);
      myDefaultShape1Radius = myHeight / 2;
      myDefaultExtRingWith = resources.getDimension(R.dimen.capture_button_capture_photo_ext_ring_width);
      myDefaultShape6Radius = resources.getDimension(R.dimen.capture_button_capture_photo_shape6_radius);
      myMiddleShape4Radius = resources.getDimension(R.dimen.capture_button_capture_photo_shape4_middle_radius);
    }

    @Override
    protected void init() {
      super.init();
      //noinspection SuspiciousNameCombination
      myShape1Width = myDefaultShape1Width;
      myShape1Radius = myDefaultShape1Radius;
      myShape3Radius = myDefaultShape3Radius;
      myShape4Radius = 0f;
      myShape5Size = 0f;
      myShape6Radius = myDefaultShape6Radius;
      myExtRingWith = myDefaultExtRingWith;
      myShape2Alpha = 255;
      invalidateSelf();
    }

    @NonNull
    public ValueAnimator createTakePictureAnimator() {
      ValueAnimator animator = ValueAnimator.ofFloat(getMaxExtRingWidth(), myDefaultExtRingWith);
      animator.setInterpolator(new DecelerateInterpolator());
      Util.setAnimatorDuration(animator, 150L);

      animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
          init();
          myExtRingWith = (float) animation.getAnimatedValue();
          invalidateSelf();
        }
      });
      return animator;
    }

    @NonNull
    public ValueAnimator createToRecordingVideoAnimator() {
      ValueAnimator animator = ValueAnimator.ofFloat(0f, 17f);
      animator.setInterpolator(new DecelerateInterpolator());
      Util.setAnimatorDuration(animator, 170);
      final float shape5MinSize = myShape5Radius * 2;

      animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
          init();
          float value = (float) animation.getAnimatedValue();
          float val1 = 1f - (Math.max(1f, Math.min(7f, value)) - 1f) / 6f;
          myShape6Radius = myDefaultShape6Radius * val1;
          myShape1Radius = myDefaultShape1Radius * val1;
          myShape1Width = myShape1Radius * 2;
          myExtRingWith = myDefaultExtRingWith * val1;
          myShape5Size = shape5MinSize + (Math.max(2f, Math.min(10f, value)) - 2f) / 8f *
              (myRecordingVideoState.myDefaultShape5Size - shape5MinSize);
          myShape4Radius = myMiddleShape4Radius + (Math.max(2f, value) - 2f) / 15f *
              (myRecordingVideoState.myDefaultShape4Radius - myMiddleShape4Radius);
          invalidateSelf();
        }
      });
      animator.addListener(new AnimatorListenerAdapter() {
        @Override
        public void onAnimationStart(Animator animation) {
          myDrawShape6BehindShape5 = true;
        }

        @Override
        public void onAnimationEnd(Animator animation) {
          myDrawShape6BehindShape5 = false;
          invalidateSelf();
        }
      });
      return animator;
    }

    @NonNull
    public ValueAnimator createToCapturingVideoAnimator() {
      ValueAnimator animator = ValueAnimator.ofFloat(0f, 21f);
      animator.setInterpolator(new LinearInterpolator());
      Util.setAnimatorDuration(animator, 250);

      animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
          init();
          float value = (float) animation.getAnimatedValue();
          // Scenario:
          // 0-10: myShape1Width: myDefaultShape1Width-myCapturingVideoState.myDefaultShape1Width
          // 0-7: myShape6Radius: myDefaultShape6Radius-0
          // 0-13: myShape2Alpha: 255-0
          // 8-21: myShape4Radius: 0-myCapturingVideoState.myDefaultShape4Radius
          myShape1Width = myDefaultShape1Width + (myCapturingVideoState.myDefaultShape1Width - myDefaultShape1Width) * Math.min(10f, value) / 10f;
          myShape6Radius = myDefaultShape6Radius * (1f - Math.min(7f, value) / 7f);
          myShape2Alpha = (int) (255f * (1f - Math.min(13f, value) / 13f));
          myShape4Radius = myCapturingVideoState.myDefaultShape4Radius * (Math.max(8f, Math.min(21f, value)) - 8f) / 13f;
          invalidateSelf();
        }
      });
      return animator;
    }
  }

  public class CapturingVideoState extends State {
    private final float myDefaultShape1Width;
    private final float myDefaultShape1Radius;
    private final float myDefaultShape4Radius;
    private final float myMiddleShape4Radius1;
    private final float myMiddleShape4Radius2;

    public CapturingVideoState() {
      Resources resources = myContext.getResources();
      myDefaultShape1Width = myWidth;
      myDefaultShape1Radius = myHeight / 2;
      myDefaultShape4Radius = resources.getDimension(R.dimen.capture_button_capture_video_shape4_radius);
      myMiddleShape4Radius1 = resources.getDimension(R.dimen.capture_button_capture_video_shape4_middle1_radius);
      myMiddleShape4Radius2 = resources.getDimension(R.dimen.capture_button_capture_video_shape4_middle2_radius);
    }

    @Override
    protected void init() {
      super.init();
      //noinspection SuspiciousNameCombination
      myShape1Width = myDefaultShape1Width;
      myShape1Radius = myDefaultShape1Radius;
      myShape3Radius = 0f;
      myShape4Radius = myDefaultShape4Radius;
      myShape5Size = 0f;
      myShape6Radius = 0f;
      myExtRingWith = 0f;
      myShape2Alpha = 0;
      invalidateSelf();
    }

    @NonNull
    public ValueAnimator createToCapturingPhotoAnimator() {
      ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f);
      animator.setInterpolator(new DecelerateInterpolator());
      Util.setAnimatorDuration(animator, 250);

      animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
          init();
          float value = (float) animation.getAnimatedValue();
          myExtRingWith = myCapturingPhotoState.myDefaultExtRingWith;
          myShape1Width = myCapturingPhotoState.myDefaultShape1Width + (
              myDefaultShape1Width - myCapturingPhotoState.myDefaultShape1Width) * (1f - value);
          myShape6Radius = myCapturingPhotoState.myDefaultShape6Radius * value;
          myShape2Alpha = (int) (255f * value);
          invalidateSelf();
        }
      });
      return animator;
    }

    @NonNull
    public Animator createToRecordingVideoAnimator() {
      final float shape5MinSize = myShape5Radius * 2;

      ValueAnimator animator = ValueAnimator.ofFloat(0f, 34f);
      animator.setInterpolator(new LinearInterpolator());
      Util.setAnimatorDuration(animator, 340L);

      animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
          init();
          // Scenario
          // 0-6: myShape1Width: myDefaultShape1Width-myRecordingVideoState.myDefaultShape1Width
          // 0: myShape5Size = myShape5Radius * 2
          // 2-12: myShape4Radius: myDefaultShape4Radius-myMiddleShape4Radius1
          // 12-20: myShape4Radius: myMiddleShape4Radius1-myMiddleShape4Radius2
          // 25-34: myShape4Radius: myMiddleShape4Radius2-myRecordingVideoState.myDefaultShape4Radius
          // 7-17: myShape5Size: myShape5Radius * 2-myRecordingVideoState.myDefaultShape5Size
          //
          float value = (float) animation.getAnimatedValue();
          myShape1Width = myDefaultShape1Width - Math.min(6f, value) / 6f * (myDefaultShape1Width - myRecordingVideoState.myDefaultShape1Width);
          myShape5Size = shape5MinSize + (Math.max(7f, Math.min(17f, value)) - 7f) / 10f *
              (myRecordingVideoState.myDefaultShape5Size - shape5MinSize);
          if (value < 12f)
            myShape4Radius = myDefaultShape4Radius + (Math.max(2f, value) - 2f) / 10f *
                (myMiddleShape4Radius1 - myDefaultShape4Radius);
          else if (value < 20f)
            myShape4Radius = myMiddleShape4Radius1 - (Math.min(20f, value) - 12f) / 8f *
                (myMiddleShape4Radius1 - myMiddleShape4Radius2);
          else
            myShape4Radius = myMiddleShape4Radius2 + (Math.max(25f, value) - 25f) / 9f *
                (myRecordingVideoState.myDefaultShape4Radius - myMiddleShape4Radius2);
          invalidateSelf();
        }
      });
      return animator;
    }
  }

  public class RecordingVideoState extends State {
    private final float myDefaultShape1Width;
    private final float myDefaultShape1Radius;
    private final float myDefaultShape4Radius;
    private final float myDefaultShape5Size;

    public RecordingVideoState() {
      Resources resources = myContext.getResources();
      myDefaultShape1Width = resources.getDimension(R.dimen.capture_button_video_recording_shape1_width);
      myDefaultShape1Radius = resources.getDimension(R.dimen.capture_button_video_recording_shape1_radius);
      myDefaultShape4Radius = myHeight / 2;
      myDefaultShape5Size = resources.getDimension(R.dimen.capture_button_video_recording_shape5_size);
    }

    @Override
    protected void init() {
      super.init();
      //noinspection SuspiciousNameCombination
      myShape1Width = myDefaultShape1Width;
      myShape1Radius = myDefaultShape1Radius;
      myShape3Radius = 0f;
      myShape4Radius = myDefaultShape4Radius;
      myShape5Size = myDefaultShape5Size;
      myShape6Radius = 0f;
      myExtRingWith = 0f;
      myShape2Alpha = 0;
      invalidateSelf();
    }

    @NonNull
    public Animator createToCapturingVideoAnimator() {
      ValueAnimator animator = ValueAnimator.ofFloat(0f, 22f);
      animator.setInterpolator(new LinearInterpolator());
      Util.setAnimatorDuration(animator, 220L);

      animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
          init();
          float value = (float) animation.getAnimatedValue();
          myShape5Size = 0;
          myShape1Width = Math.min(10f, value) / 10f * myCapturingVideoState.myDefaultShape1Width;
          myShape1Radius = myDefaultShape1Radius + Math.min(17f, value) / 17f *
              (myCapturingVideoState.myDefaultShape1Radius - myDefaultShape1Radius);
          myShape4Radius = myDefaultShape4Radius - (Math.max(9f, value) - 9f) / 13f *
              (myDefaultShape4Radius - myCapturingVideoState.myDefaultShape4Radius);
          invalidateSelf();
        }
      });
      return animator;
    }
  }
}
