package com.kubro.telegramchallenge.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;

import com.kubro.telegramchallenge.Util;
import com.kubro.telegramchallenge.util.Size;
import com.kubro.telegramchallenge.util.SizeF;

/**
 * @author Eugene.Kudelevsky
 */
public class ImageBrowserView extends ImageView {
  private static final float MAX_ZOOM = 3;

  private final ScaleGestureDetector myScaleGestureDetector;
  private final GestureDetector myGestureDetector;

  private float myRotation = 0;
  private float myZoom = 1;
  @NonNull private PointF myViewportPosition = new PointF(0, 0);
  @NonNull private MyState myState = MyState.DEFAULT;

  @Nullable private SizeF myScaledImageSize = null;
  private Float myMinZoom = null;
  private float myContentHInset = 0;
  private float myContentVInset = 0;
  private Runnable myChangeListener;

  public ImageBrowserView(Context context, AttributeSet attrs) {
    super(context, attrs);
    myScaleGestureDetector = new ScaleGestureDetector(context, new MyScaleGestureListener());
    myGestureDetector = new GestureDetector(context, new MyGestureListener());
    setScaleType(ScaleType.MATRIX);
    setOnTouchListener(new MyOnTouchListener());
  }

  @Override
  public void setImageResource(int resId) {
    super.setImageResource(resId);
    imageChanged();
  }

  @Override
  public void setImageURI(Uri uri) {
    super.setImageURI(uri);
    imageChanged();
  }

  @Override
  public void setImageDrawable(Drawable drawable) {
    super.setImageDrawable(drawable);
    imageChanged();
  }

  @Override
  public void setImageBitmap(Bitmap bm) {
    super.setImageBitmap(bm);
    imageChanged();
  }

  private void imageChanged() {
    myMinZoom = null;
    resetZoom();
  }

  @NonNull
  public Size getImageSize() {
    Drawable drawable = getDrawable();
    return drawable == null ? Size.EMPTY : new Size(
        drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
  }

  public void setZoom(float zoom, float focusX, float focusY) {
    float newZoom = Math.max(getMinZoom(), Math.min(MAX_ZOOM, zoom));
    float focusOnContentX = myViewportPosition.x + focusX;
    float focusOnContentY = myViewportPosition.y + focusY;
    focusOnContentX *= newZoom / myZoom;
    focusOnContentY *= newZoom / myZoom;
    myZoom = newZoom;
    myScaledImageSize = null;
    setViewportPosition(focusOnContentX - focusX, focusOnContentY - focusY);
  }

  private void fireChanged() {
    if (myChangeListener != null) {
      myChangeListener.run();
    }
  }

  public void setImageRotation(float degrees) {
    degrees = (360 + degrees) % 360;
    float focusX = (float) getWidth() / 2;
    float focusY = (float) getHeight() / 2;
    float focusOnContentX = myViewportPosition.x + focusX;
    float focusOnContentY = myViewportPosition.y + focusY;
    float deltaDegrees = degrees - myRotation;
    myRotation = degrees;
    Matrix matrix = new Matrix();
    matrix.postRotate(deltaDegrees, 0, 0);
    float[] newViewportXY = {focusOnContentX, focusOnContentY};
    matrix.mapPoints(newViewportXY);
    myMinZoom = null;
    setViewportPosition(newViewportXY[0] - focusX, newViewportXY[1] - focusY);

    if (myZoom < getMinZoom()) {
      setZoom(myZoom, focusX, focusY);
    }
  }

  public void setViewportPosition(float x, float y) {
    myViewportPosition = adjustPointOnContent(x, y);
    updateMatrix();
    fireChanged();
  }

  @NonNull
  private PointF adjustPointOnContent(float x, float y) {
    float viewWidth = (float) getWidth();
    float viewHeight = (float) getHeight();
    float leftBound = 0;
    float topBound = 0;
    SizeF scaledImageSize = getScaledImageSize();
    float rightBound = leftBound + scaledImageSize.getWidth();
    float bottomBound = topBound + scaledImageSize.getHeight();

    if (rightBound - leftBound < myContentHInset || bottomBound - topBound < myContentVInset) {
      return new PointF(0, 0);
    }
    Matrix matrix = new Matrix();
    matrix.postRotate(-myRotation);
    float leftInset, topInset, rightInset, bottomInset;
    float sin = (float) Math.sin(Math.toRadians(myRotation));
    float cos = (float) Math.cos(Math.toRadians(myRotation));
    float extraHInset;
    float extraVInset;

    if (myRotation < 90) {
      extraHInset = myContentHInset * cos;
      extraVInset = myContentVInset * cos;
      topInset = 0;
      leftInset = -viewHeight * sin;
      rightInset = viewWidth * cos;
      bottomInset = viewHeight * cos - viewWidth * sin;
    } else if (myRotation < 180) {
      extraHInset = myContentHInset * sin;
      extraVInset = myContentVInset * sin;
      leftInset = 0;
      topInset = viewWidth * sin;
      rightInset = viewHeight * sin + viewWidth * cos;
      bottomInset = viewHeight * cos;
    } else if (myRotation < 270) {
      extraHInset = -myContentHInset * cos;
      extraVInset = -myContentVInset * cos;
      topInset = viewWidth * sin - viewHeight * cos;
      leftInset = -viewWidth * cos;
      rightInset = viewHeight * sin;
      bottomInset = 0;
    } else {
      extraHInset = -myContentHInset * sin;
      extraVInset = -myContentVInset * sin;
      leftInset = -viewHeight * sin - viewWidth * cos;
      topInset = -viewHeight * cos;
      rightInset = 0;
      bottomInset = -viewWidth * sin;
    }
    float[] xy = {x, y};
    matrix.mapPoints(xy);
    float newX = Math.max(leftBound + leftInset - extraHInset,
        Math.min(rightBound - rightInset + extraHInset, xy[0]));
    float newY = Math.max(topBound + topInset - extraVInset,
        Math.min(bottomBound - bottomInset + extraVInset, xy[1]));
    matrix.reset();
    matrix.postRotate(myRotation);
    float[] points = {newX, newY, leftBound, topBound, rightBound, topBound,
        leftBound, bottomBound, rightBound, bottomBound};
    matrix.mapPoints(points);
    newX = points[0];
    newY = points[1];
    PointF p1 = new PointF(points[2], points[3]);
    PointF p2 = new PointF(points[4], points[5]);
    PointF p3 = new PointF(points[6], points[7]);
    PointF p4 = new PointF(points[8], points[9]);
    float outLeft = Math.min(Math.min(p1.x, p2.x), Math.min(p3.x, p4.x)) - myContentHInset;
    float outRight = Math.max(Math.max(p1.x, p2.x), Math.max(p3.x, p4.x)) + myContentHInset;
    float outTop = Math.min(Math.min(p1.y, p2.y), Math.min(p3.y, p4.y)) - myContentVInset;
    float outBottom = Math.max(Math.max(p1.y, p2.y), Math.max(p3.y, p4.y)) + myContentVInset;
    float hExtraSpace = Math.max(0, viewWidth / 2 - (outRight - outLeft) / 2);
    float vExtraSpace = Math.max(0, viewHeight / 2 - (outBottom - outTop) / 2);
    newX = -hExtraSpace + Math.max(outLeft, Math.min(outRight - viewWidth, newX));
    newY = -vExtraSpace + Math.max(outTop, Math.min(outBottom - viewHeight, newY));
    return new PointF(newX, newY);
  }

  private float getMinZoom() {
    if (myMinZoom == null) {
      Size imageSize = getImageSize();
      float imageWidth = imageSize.getWidth();
      float imageHeight = imageSize.getHeight();
      float sin = (float) Math.sin(Math.toRadians(myRotation));
      float cos = (float) Math.cos(Math.toRadians(myRotation));
      float viewWidth = getWidth() - myContentHInset * 2;
      float viewHeight = getHeight() - myContentVInset * 2;

      if (viewWidth > 0 && viewHeight > 0 && imageWidth > 0 && imageHeight > 0) {
        if (myRotation < 90) {
          myMinZoom = Math.min(viewWidth / (imageHeight * sin + imageWidth * cos),
              viewHeight / (sin * imageWidth + cos * imageHeight));
        } else if (myRotation < 180) {
          myMinZoom = Math.min(viewWidth / (imageHeight * sin - imageWidth * cos),
              viewHeight / (sin * imageWidth - cos * imageHeight));
        } else if (myRotation < 270) {
          myMinZoom = Math.min(viewWidth / (-imageHeight * sin - cos * imageWidth),
              viewHeight / (-sin * imageWidth - cos * imageHeight));
        } else {
          myMinZoom = Math.min(viewWidth / (-imageHeight * sin + cos * imageWidth),
              viewHeight / (-sin * imageWidth + cos * imageHeight));
        }
      } else {
        myMinZoom = 1f;
      }
    }
    return myMinZoom;
  }

  @NonNull
  public SizeF getScaledImageSize() {
    if (myScaledImageSize == null) {
      Size imageSize = getImageSize();

      if (getWidth() == 0 || getHeight() == 0 || imageSize.getWidth() == 0 ||
          imageSize.getHeight() == 0) {
        myScaledImageSize = SizeF.EMPTY;
      } else {
        myScaledImageSize = new SizeF(imageSize.getWidth() * myZoom, imageSize.getHeight() * myZoom);
      }
    }
    return myScaledImageSize;
  }

  private void updateMatrix() {
    setImageMatrix(computeImageMatrix());
  }

  @Nullable
  private Matrix computeImageMatrix() {
    Size imageSize = getImageSize();
    float viewWidth = getWidth();
    float viewHeight = getHeight();

    if (imageSize.getWidth() == 0 || imageSize.getHeight() == 0 ||
        viewWidth == 0 || viewHeight == 0) {
      return null;
    }
    Matrix matrix = new Matrix();
    matrix.postScale(myZoom, myZoom);
    matrix.postRotate(myRotation);
    matrix.postTranslate(-myViewportPosition.x, -myViewportPosition.y);
    return matrix;
  }

  @Override
  protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    super.onSizeChanged(w, h, oldw, oldh);
    myScaledImageSize = null;
    myMinZoom = null;
    resetZoom();
  }

  public void resetZoom() {
    setZoom(0f, (float) getWidth() / 2, (float) getHeight() / 2);
  }

  public void setChangeListener(@Nullable Runnable runnable) {
    myChangeListener = runnable;
  }

  public boolean isZoomChanged() {
    return myZoom > getMinZoom() + Util.EPS;
  }

  @NonNull
  public RectF toContentRect(@NonNull RectF viewRect) {
    RectF boundsRect = getViewportBoundsRect();

    RectF result = new RectF(myViewportPosition.x + viewRect.left,
        myViewportPosition.y + viewRect.top,
        myViewportPosition.x + viewRect.right,
        myViewportPosition.y + viewRect.bottom);
    result.intersect(boundsRect);
    float scale = 1 / myZoom;
    result.set(result.left * scale, result.top * scale,
        result.right * scale, result.bottom * scale);
    return result;
  }

  @NonNull
  public RectF getContentBoundsRect() {
    RectF rect = getViewportBoundsRect();
    float scale = 1 / myZoom;
    rect.set(rect.left * scale, rect.top * scale, rect.right * scale, rect.bottom * scale);
    return rect;
  }

  @NonNull
  public RectF getViewportBoundsRect() {
    Size size = getImageSize();
    RectF boundsRect = new RectF(0, 0, size.getWidth(), size.getHeight());
    Matrix matrix = new Matrix();
    matrix.postScale(myZoom, myZoom);
    matrix.postRotate(myRotation);
    matrix.mapRect(boundsRect);
    return boundsRect;
  }

  @Nullable
  public PointF[] getImageVerticesOnView() {
    Matrix matrix = computeImageMatrix();

    if (matrix == null) {
      return null;
    }
    Size imageSize = getImageSize();
    float[] pts = {0, 0, imageSize.getWidth(), 0, imageSize.getWidth(),
        imageSize.getHeight(), 0, imageSize.getHeight()};
    matrix.mapPoints(pts);
    PointF[] result = new PointF[4];

    for (int i = 0; i < 4; i++) {
      result[i] = new PointF(pts[i * 2], pts[i * 2 + 1]);
    }
    return result;
  }

  public float getZoom() {
    return myZoom;
  }

  @NonNull
  public RectF toViewRect(@NonNull RectF contentRect) {
    Matrix matrix = new Matrix();
    matrix.postScale(myZoom, myZoom);
    matrix.postTranslate(-myViewportPosition.x, -myViewportPosition.y);
    RectF rect1 = new RectF(contentRect);
    matrix.mapRect(rect1);
    return rect1;
  }

  public void setContentInsets(float hInset, float vInset) {
    myContentHInset = hInset;
    myContentVInset = vInset;
    myScaledImageSize = null;
    myMinZoom = null;
    resetZoom();
    setViewportPosition(myViewportPosition.x, myViewportPosition.y);
  }

  public float getImageRotation() {
    return myRotation;
  }

  private class MyScaleGestureListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
    @Override
    public boolean onScaleBegin(ScaleGestureDetector detector) {
      myState = MyState.ZOOMING;
      return true;
    }

    @Override
    public void onScaleEnd(ScaleGestureDetector detector) {
      myState = MyState.DEFAULT;
    }

    @Override
    public boolean onScale(ScaleGestureDetector detector) {
      if (myState == MyState.ZOOMING) {
        setZoom(myZoom * detector.getScaleFactor(), detector.getFocusX(), detector.getFocusY());
        return true;
      }
      return false;
    }
  }

  private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
  }

  private class MyOnTouchListener implements OnTouchListener {
    private float myLastTouchX = 0;
    private float myLastTouchY = 0;
    private int myActivePointerId = MotionEvent.INVALID_POINTER_ID;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
      myScaleGestureDetector.onTouchEvent(event);
      myGestureDetector.onTouchEvent(event);
      int action = event.getActionMasked();
      int pointerIndex;

      switch (action) {
        case MotionEvent.ACTION_DOWN:
          if (myState == MyState.DEFAULT) {
            pointerIndex = event.getActionIndex();
            myLastTouchX = event.getX(pointerIndex);
            myLastTouchY = event.getY(pointerIndex);
            myActivePointerId = event.getPointerId(pointerIndex);
            myState = MyState.MOVING;
          }
          break;
        case MotionEvent.ACTION_MOVE:
          if (myState == MyState.MOVING) {
            pointerIndex = event.findPointerIndex(myActivePointerId);
            float x = event.getX(pointerIndex);
            float y = event.getY(pointerIndex);
            float dx = myLastTouchX - x;
            float dy = myLastTouchY - y;
            setViewportPosition(myViewportPosition.x + dx, myViewportPosition.y + dy);
            myLastTouchX = x;
            myLastTouchY = y;
          }
          break;
        case MotionEvent.ACTION_UP:
          if (myState == MyState.MOVING) {
            myActivePointerId = MotionEvent.INVALID_POINTER_ID;
            myState = MyState.DEFAULT;
          }
          break;
        case MotionEvent.ACTION_CANCEL:
          if (myState == MyState.MOVING) {
            myActivePointerId = MotionEvent.INVALID_POINTER_ID;
            myState = MyState.DEFAULT;
          }
          break;
        case MotionEvent.ACTION_POINTER_UP:
          if (myState == MyState.MOVING) {
            pointerIndex = event.getActionIndex();
            int pointerId = event.getPointerId(pointerIndex);

            if (pointerId == myActivePointerId) {
              int newPointerIndex = pointerIndex == 0 ? 1 : 0;
              myLastTouchX = event.getX(newPointerIndex);
              myLastTouchY = event.getY(newPointerIndex);
              myActivePointerId = event.getPointerId(newPointerIndex);
            }
          }
          break;
      }
      return true;
    }
  }

  private enum MyState {
    DEFAULT, ZOOMING, MOVING
  }
}