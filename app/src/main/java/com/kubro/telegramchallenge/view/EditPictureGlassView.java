package com.kubro.telegramchallenge.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.kubro.telegramchallenge.R;
import com.kubro.telegramchallenge.util.Consumer;

/**
 * @author Eugene.Kudelevsky
 */
public class EditPictureGlassView extends View {
  private static final int OUTSIDE_ALPHA = 150;
  private static final int LINE1_ALPHA = 180;
  private static final int LINE2_ALPHA = 150;
  private static final int ROW_COUNT = 3;
  private static final int COLUMN_COUNT = 3;

  private final Paint myPaint = new Paint();
  private final float myCornerLineWidth;
  private final float myLine1Thickness;
  private final float myLine2Thickness;
  private final float myCornerLineThickness;
  private final float myMinGlassHMargin;
  private final float myMoveEdgeTolerance;
  private final float myMinGlassSize;

  private float myMinGlassVMargin = 0;
  private float myGlassLeftMargin = 0;
  private float myGlassRightMargin = 0;
  private float myGlassTopMargin = 0;
  private float myGlassBottomMargin = 0;
  private boolean myShowGrid = false;

  private Runnable myOnGlassMarginsChangedListener;
  @Nullable private Consumer<RectF> myGlassRectAdjuster;

  public EditPictureGlassView(Context context, AttributeSet attrs) {
    super(context, attrs);
    Resources resources = context.getResources();
    myLine1Thickness = resources.getDimension(R.dimen.edit_picture_glass_line1_thickness);
    myLine2Thickness = resources.getDimension(R.dimen.edit_picture_glass_line2_thickness);
    myCornerLineThickness = resources.getDimension(R.dimen.edit_picture_glass_corner_line_thickness);
    myCornerLineWidth = resources.getDimension(R.dimen.edit_picture_glass_corner_line_width);
    myMinGlassHMargin = resources.getDimension(R.dimen.edit_picture_glass_min_h_margin);
    myGlassLeftMargin = myMinGlassHMargin;
    myGlassRightMargin = myMinGlassHMargin;
    myMoveEdgeTolerance = resources.getDimension(R.dimen.edit_picture_glass_move_edge_tolerance);
    myMinGlassSize = resources.getDimension(R.dimen.edit_picture_glass_min_size);
    setOnTouchListener(new MyOnTouchListener());
  }

  public void setOnGlassMarginsChangedListener(Runnable onGlassMarginsChangedListener) {
    myOnGlassMarginsChangedListener = onGlassMarginsChangedListener;
  }

  public float getMinGlassHMargin() {
    return myMinGlassHMargin;
  }

  public float getMinGlassVMargin() {
    return myMinGlassVMargin;
  }

  public float getGlassLeftMargin() {
    return myGlassLeftMargin;
  }

  public float getGlassRightMargin() {
    return myGlassRightMargin;
  }

  public float getGlassTopMargin() {
    return myGlassTopMargin;
  }

  public float getGlassBottomMargin() {
    return myGlassBottomMargin;
  }

  @NonNull
  public RectF getGlassRect() {
    return new RectF(myGlassLeftMargin, myGlassTopMargin, getWidth() - myGlassRightMargin,
        getHeight() - myGlassBottomMargin);
  }

  public void setShowGrid(boolean showGrid) {
    myShowGrid = showGrid;
  }

  public void setGlassMargins(float left, float top, float right, float bottom) {
    if (myGlassRectAdjuster != null && getWidth() != 0 && getHeight() != 0) {
      RectF rect = new RectF(left, top, Math.max(left, getWidth() - right),
          Math.max(top, getHeight() - bottom));
      myGlassRectAdjuster.consume(rect);
      left = rect.left;
      top = rect.top;
      right = getWidth() - rect.right;
      bottom = getHeight() - rect.bottom;
    }
    myGlassLeftMargin = Math.max(myMinGlassHMargin, left);
    myGlassTopMargin = Math.max(myMinGlassVMargin, top);
    myGlassRightMargin = Math.max(myMinGlassHMargin, right);
    myGlassBottomMargin = Math.max(myMinGlassVMargin, bottom);

    if (!myShowGrid) {
      myShowGrid = true;
    }
    invalidate();
  }

  public void setGlassRectAdjuster(@Nullable Consumer<RectF> adjuster) {
    myGlassRectAdjuster = adjuster;
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    int width = MeasureSpec.getSize(widthMeasureSpec);
    int height = MeasureSpec.getSize(heightMeasureSpec);
    myMinGlassVMargin = myMinGlassHMargin / width * height;
    float newGlassTopMargin = Math.max(myGlassTopMargin, myMinGlassVMargin);
    float newGlassBottomMargin = Math.max(myGlassBottomMargin, myMinGlassVMargin);
    setGlassMargins(myGlassLeftMargin, newGlassTopMargin, myGlassRightMargin, newGlassBottomMargin);
    super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
        MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
  }

  public void updateMargins() {
    setGlassMargins(myGlassLeftMargin, myGlassTopMargin, myGlassRightMargin, myGlassBottomMargin);
  }

  @Override
  protected void onDraw(Canvas canvas) {
    float width = canvas.getWidth();
    float height = canvas.getHeight();
    myPaint.setStyle(Paint.Style.FILL);
    myPaint.setColor(Color.BLACK);
    myPaint.setAlpha(OUTSIDE_ALPHA);

    canvas.drawRect(0, 0, width, myGlassTopMargin, myPaint);
    canvas.drawRect(0, height - myGlassBottomMargin, width, height, myPaint);
    canvas.drawRect(0, myGlassTopMargin, myGlassLeftMargin, height - myGlassBottomMargin, myPaint);
    canvas.drawRect(width - myGlassRightMargin, myGlassTopMargin, width, height - myGlassBottomMargin, myPaint);

    myPaint.reset();
    myPaint.setStyle(Paint.Style.STROKE);
    myPaint.setColor(Color.WHITE);
    myPaint.setAlpha(LINE1_ALPHA);
    myPaint.setStrokeWidth(myLine1Thickness);
    canvas.drawRect(myGlassLeftMargin, myGlassTopMargin, width - myGlassRightMargin,
        height - myGlassBottomMargin, myPaint);

    myPaint.reset();
    myPaint.setColor(Color.WHITE);
    myPaint.setStyle(Paint.Style.FILL);
    float cornersLeft = myGlassLeftMargin - myCornerLineThickness + myLine1Thickness / 2;
    float cornersRight = width - myGlassRightMargin + myCornerLineThickness - myLine1Thickness / 2;
    float cornersTop = myGlassTopMargin - myCornerLineThickness + myLine1Thickness / 2;
    float cornersBottom = height - myGlassBottomMargin + myCornerLineThickness - myLine1Thickness / 2;

    canvas.drawRect(cornersLeft, cornersTop, cornersLeft + myCornerLineWidth, cornersTop + myCornerLineThickness, myPaint);
    canvas.drawRect(cornersLeft, cornersTop, cornersLeft + myCornerLineThickness, cornersTop + myCornerLineWidth, myPaint);
    canvas.drawRect(cornersRight - myCornerLineWidth, cornersTop, cornersRight, cornersTop + myCornerLineThickness, myPaint);
    canvas.drawRect(cornersRight - myCornerLineThickness, cornersTop, cornersRight, cornersTop + myCornerLineWidth, myPaint);
    canvas.drawRect(cornersLeft, cornersBottom - myCornerLineThickness, cornersLeft + myCornerLineWidth, cornersBottom, myPaint);
    canvas.drawRect(cornersLeft, cornersBottom - myCornerLineWidth, cornersLeft + myCornerLineThickness, cornersBottom, myPaint);
    canvas.drawRect(cornersRight - myCornerLineWidth, cornersBottom - myCornerLineThickness, cornersRight, cornersBottom, myPaint);
    canvas.drawRect(cornersRight - myCornerLineThickness, cornersBottom - myCornerLineWidth, cornersRight, cornersBottom, myPaint);

    if (myShowGrid) {
      myPaint.reset();
      myPaint.setStyle(Paint.Style.STROKE);
      myPaint.setColor(Color.WHITE);
      myPaint.setAlpha(LINE2_ALPHA);
      myPaint.setStrokeWidth(Math.round(myLine2Thickness));
      float columnWidth = (width - myGlassLeftMargin - myGlassRightMargin) / COLUMN_COUNT;
      float rowHeight = (height - myGlassTopMargin - myGlassBottomMargin) / ROW_COUNT;
      float y = myGlassTopMargin;

      for (int row = 1; row < ROW_COUNT; row++) {
        y += rowHeight;
        canvas.drawLine(myGlassLeftMargin, y, width - myGlassRightMargin, y, myPaint);
      }
      float x = myGlassLeftMargin;

      for (int column = 1; column < COLUMN_COUNT; column++) {
        x += columnWidth;
        canvas.drawLine(x, myGlassTopMargin, x, height - myGlassBottomMargin, myPaint);
      }
    }
  }

  private class MyOnTouchListener implements OnTouchListener {
    private boolean myMovingLeftTopCorner;
    private boolean myMovingLeftBottomCorner;
    private boolean myMovingRightTopCorner;
    private boolean myMovingRightBottomCorner;
    private float mySavedGlassLeftMargin;
    private float mySavedGlassRightMargin;
    private float mySavedGlassTopMargin;
    private float mySavedGlassBottomMargin;
    private float myStartX;
    private float myStartY;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
      int action = event.getAction();
      float x = event.getX();
      float y = event.getY();

      if (action == MotionEvent.ACTION_DOWN) {
        boolean moveLeftEdge = myGlassLeftMargin - myMoveEdgeTolerance < x && x < myGlassLeftMargin + myMoveEdgeTolerance;
        boolean moveRightEdge = getWidth() - myGlassRightMargin - myMoveEdgeTolerance < x &&
            x < getWidth() - myGlassRightMargin + myMoveEdgeTolerance;
        boolean moveTopEdge = myGlassTopMargin - myMoveEdgeTolerance < y && y < myGlassTopMargin + myMoveEdgeTolerance;
        boolean moveBottomEdge = getHeight() - myGlassBottomMargin - myMoveEdgeTolerance < y &&
            y < getHeight() - myGlassBottomMargin + myMoveEdgeTolerance;

        if (moveLeftEdge && moveTopEdge) {
          myStartX = x;
          myStartY = y;
          mySavedGlassLeftMargin = myGlassLeftMargin;
          mySavedGlassTopMargin = myGlassTopMargin;
          myMovingLeftTopCorner = true;
          return true;
        } else if (moveLeftEdge && moveBottomEdge) {
          myStartX = x;
          myStartY = y;
          mySavedGlassLeftMargin = myGlassLeftMargin;
          mySavedGlassBottomMargin = myGlassBottomMargin;
          myMovingLeftBottomCorner = true;
          return true;
        } else if (moveRightEdge && moveTopEdge) {
          myStartX = x;
          myStartY = y;
          mySavedGlassRightMargin = myGlassRightMargin;
          mySavedGlassTopMargin = myGlassTopMargin;
          myMovingRightTopCorner = true;
          return true;
        } else if (moveRightEdge && moveBottomEdge) {
          myStartX = x;
          myStartY = y;
          mySavedGlassRightMargin = myGlassRightMargin;
          mySavedGlassBottomMargin = myGlassBottomMargin;
          myMovingRightBottomCorner = true;
          return true;
        }
      } else if (action == MotionEvent.ACTION_MOVE) {
        float maxLeftMargin = getWidth() - myGlassRightMargin - myMinGlassSize;
        float maxRightMargin = getWidth() - myGlassLeftMargin - myMinGlassSize;
        float maxTopMargin = getHeight() - myGlassBottomMargin - myMinGlassSize;
        float maxBottomMargin = getHeight() - myGlassTopMargin - myMinGlassSize;

        if (myMovingLeftTopCorner) {
          float newGlassLeftMargin = Math.min(maxLeftMargin, Math.max(myMinGlassHMargin, mySavedGlassLeftMargin + x - myStartX));
          float newGlassTopMargin = Math.min(maxTopMargin, Math.max(myMinGlassVMargin, mySavedGlassTopMargin + y - myStartY));
          setGlassMargins(newGlassLeftMargin, newGlassTopMargin, myGlassRightMargin, myGlassBottomMargin);
          myShowGrid = true;
          invalidate();
        } else if (myMovingLeftBottomCorner) {
          float newGlassLeftMargin = Math.min(maxLeftMargin, Math.max(myMinGlassHMargin, mySavedGlassLeftMargin + x - myStartX));
          float newGlassBottomMargin = Math.min(maxBottomMargin, Math.max(myMinGlassVMargin, mySavedGlassBottomMargin - y + myStartY));
          setGlassMargins(newGlassLeftMargin, myGlassTopMargin, myGlassRightMargin, newGlassBottomMargin);
          myShowGrid = true;
          invalidate();
        } else if (myMovingRightTopCorner) {
          float newGlassRightMargin = Math.min(maxRightMargin, Math.max(myMinGlassHMargin, mySavedGlassRightMargin - x + myStartX));
          float newGlassTopMargin = Math.min(maxTopMargin, Math.max(myMinGlassVMargin, mySavedGlassTopMargin + y - myStartY));
          setGlassMargins(myGlassLeftMargin, newGlassTopMargin, newGlassRightMargin, myGlassBottomMargin);
          myShowGrid = true;
          invalidate();
        } else if (myMovingRightBottomCorner) {
          float newGlassRightMargin = Math.min(maxRightMargin, Math.max(myMinGlassHMargin, mySavedGlassRightMargin - x + myStartX));
          float newGlassBottomMargin = Math.min(maxBottomMargin, Math.max(myMinGlassVMargin, mySavedGlassBottomMargin - y + myStartY));
          setGlassMargins(myGlassLeftMargin, myGlassTopMargin, newGlassRightMargin, newGlassBottomMargin);
          myShowGrid = true;
          invalidate();
        }
      } else if (action == MotionEvent.ACTION_UP) {
        if (myMovingLeftTopCorner || myMovingLeftBottomCorner ||
            myMovingRightTopCorner || myMovingRightBottomCorner) {
          if (myOnGlassMarginsChangedListener != null) {
            myOnGlassMarginsChangedListener.run();
          }
        }
        myStartX = 0;
        myStartY = 0;
        mySavedGlassLeftMargin = 0;
        mySavedGlassRightMargin = 0;
        mySavedGlassTopMargin = 0;
        mySavedGlassBottomMargin = 0;
        myMovingLeftTopCorner = false;
        myMovingLeftBottomCorner = false;
        myMovingRightTopCorner = false;
        myMovingRightBottomCorner = false;
      }
      return false;
    }

  }
}
