package com.kubro.telegramchallenge.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

/**
 * @author Eugene.Kudelevsky
 */
public class ToggleImageButton extends ImageButton {
  private static final int[] STATE_CHECKED = {android.R.attr.state_checked};
  private boolean myChecked;

  public ToggleImageButton(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public void toggle() {
    setChecked(!myChecked);
  }

  public boolean isChecked() {
    return myChecked;
  }

  public void setChecked(boolean checked) {
    if (myChecked != checked) {
      myChecked = checked;
      refreshDrawableState();
    }
  }

  @Override
  public int[] onCreateDrawableState(int extraSpace) {
    final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);

    if (myChecked) {
      mergeDrawableStates(drawableState, STATE_CHECKED);
    }
    return drawableState;
  }
}
