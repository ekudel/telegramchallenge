package com.kubro.telegramchallenge.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.kubro.telegramchallenge.R;
import com.kubro.telegramchallenge.Util;

/**
 * @author Eugene.Kudelevsky
 */
public class PhotoRotationStripesView extends View {
  private static final float MIN_ALPHA = 0.2f;
  private static final float ANGLE_STEP = 2;
  private static final float BOUND_ANGLE = 30;

  private final Paint myPaint = new Paint();
  private final float myStripePadding1;
  private final float myStripePadding2;
  private final int myMainStripeColor;
  private final float myStripeWidth;
  private final float myStripeWidth1;
  private final float myStripeWidth2;
  private final float myVPadding;
  private float myLastTouchX = 0;
  private boolean myRolling = false;
  private float myK;
  private float myRadius = 0;
  private float myCurrentAngle = 0;
  private AngleChangedListener myAngleChangedListener;

  public PhotoRotationStripesView(Context context, AttributeSet attrs) {
    super(context, attrs);
    Resources resources = context.getResources();
    myStripeWidth = resources.getDimension(R.dimen.photo_rotation_stripe_width);
    myStripeWidth1 = resources.getDimension(R.dimen.photo_rotation_stripe_width1);
    myStripeWidth2 = resources.getDimension(R.dimen.photo_rotation_stripe_width2);
    myVPadding = resources.getDimension(R.dimen.photo_rotation_stripes_base_v_padding);
    myStripePadding1 = resources.getDimension(R.dimen.photo_rotation_stripe_padding1);
    myStripePadding2 = resources.getDimension(R.dimen.photo_rotation_stripe_padding2);
    myPaint.setStyle(Paint.Style.STROKE);
    myMainStripeColor = resources.getColor(R.color.photo_rotation_stripe_color1);

    setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        int action = event.getActionMasked();

        switch (action) {
          case MotionEvent.ACTION_DOWN:
            if (!myRolling) {
              myRolling = true;
              myLastTouchX = event.getX();
            }
            break;
          case MotionEvent.ACTION_MOVE:
            if (myRolling && myRadius > Util.EPS) {
              float dx = myLastTouchX - event.getX();
              myLastTouchX = event.getX();
              float angle = myCurrentAngle + (float) Math.toDegrees(Math.asin(dx / 7 / myRadius));
              setCurrentAngle(Math.max(-BOUND_ANGLE, Math.min(BOUND_ANGLE, angle)));
              invalidate();
            }
            break;
          case MotionEvent.ACTION_UP:
          case MotionEvent.ACTION_CANCEL:
            myRolling = false;
            break;
        }
        return true;
      }
    });
  }

  public void setCurrentAngle(float value) {
    if (Math.abs(value - myCurrentAngle) >= Util.EPS) {
      myCurrentAngle = value;
      invalidate();
      fireAngleChanged();
    }
  }

  @Override
  protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    float width = (float) getWidth() - myStripeWidth * 2;
    myK = width;
    myRadius = (float) (width * myK / (2 * myK * Math.sin(Math.toRadians(BOUND_ANGLE)) +
        width * (Math.cos(Math.toRadians(BOUND_ANGLE)) - 1)));
  }

  @Override
  protected void onDraw(Canvas canvas) {
    float currentAngle = -myCurrentAngle;
    float angle = -BOUND_ANGLE + (currentAngle % ANGLE_STEP);
    float step = ANGLE_STEP;
    float width = (float) canvas.getWidth();
    float height = (float) getHeight();
    float cy = height / 2;
    float cx = width / 2;

    while (angle <= BOUND_ANGLE + Util.EPS) {
      float alpha = MIN_ALPHA + (1f - MIN_ALPHA) * (1f - Math.abs(angle) / BOUND_ANGLE);
      float stripePadding;

      if (angle >= 0 && angle <= currentAngle + Util.EPS ||
          angle < 0 && angle >= currentAngle - Util.EPS) {
        myPaint.setColor(myMainStripeColor);
      } else {
        myPaint.setColor(Color.argb(Math.round(255 * alpha), 255, 255, 255));
      }
      if (Math.abs(currentAngle - angle) < Util.EPS) {
        myPaint.setStrokeWidth(myStripeWidth1);
        stripePadding = myStripePadding2 + myVPadding;
      } else {
        myPaint.setStrokeWidth(myStripeWidth);
        stripePadding = myStripePadding1 + myVPadding;
      }
      float cos = (float) Math.cos(Math.toRadians(angle));
      float sin = (float) Math.sin(Math.toRadians(angle));
      float l = height - stripePadding * 2;
      float h = l * myK / (myK + myRadius * (1 - cos));
      float x = myRadius * myK * sin / (myK + myRadius * (1 - cos));
      canvas.drawLine(cx + x, cy - h / 2, cx + x, cy + h / 2, myPaint);
      angle += step;
    }
    myPaint.setColor(myMainStripeColor);
    myPaint.setStrokeWidth(myStripeWidth2);
    canvas.drawLine(cx, myVPadding, cx, height - myVPadding, myPaint);
  }

  public void setAngleChangedListener(AngleChangedListener angleChangedListener) {
    myAngleChangedListener = angleChangedListener;
  }

  private void fireAngleChanged() {
    if (myAngleChangedListener != null) {
      myAngleChangedListener.angleChanged(myCurrentAngle);
    }
  }

  public float getCurrentAngle() {
    return myCurrentAngle;
  }

  public interface AngleChangedListener {
    void angleChanged(float angle);
  }
}
