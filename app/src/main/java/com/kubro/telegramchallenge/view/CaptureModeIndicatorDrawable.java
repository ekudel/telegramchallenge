package com.kubro.telegramchallenge.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.animation.LinearInterpolator;

import com.kubro.telegramchallenge.R;
import com.kubro.telegramchallenge.Util;

/**
 * @author Eugene.Kudelevsky
 */
public class CaptureModeIndicatorDrawable extends Drawable {
  private final float myRadius;
  private final float myDiameter;
  private final float myDistanceBetweenCircles;
  private final Paint mySelectedCirclePaint;
  private float myLeftOvalWidth;
  private float myRightOvalWidth;
  private float mySelectedCircleCenterX;
  private final Paint myPaint;
  private final RectF myTempRect = new RectF();
  private final ValueAnimator myAnimator2;

  public CaptureModeIndicatorDrawable(@NonNull Context context) {
    myRadius = context.getResources().getDimension(R.dimen.capture_mode_indicator_radius);
    myDistanceBetweenCircles = context.getResources().getDimension(R.dimen.capture_mode_indicator_distance_between_circles);
    myDiameter = myRadius * 2;
    myLeftOvalWidth = myDiameter;
    myRightOvalWidth = myDiameter;
    mySelectedCircleCenterX = myRadius;
    mySelectedCirclePaint = new Paint();
    mySelectedCirclePaint.setColor(Color.WHITE);
    mySelectedCirclePaint.setAlpha((int) (255 * 0.85f));
    mySelectedCirclePaint.setStyle(Paint.Style.FILL);
    mySelectedCirclePaint.setAntiAlias(true);
    myPaint = new Paint();
    myPaint.setColor(Color.WHITE);
    myPaint.setAlpha((int) (255 * 0.3f));
    myPaint.setStyle(Paint.Style.FILL);
    myPaint.setAntiAlias(true);
    myAnimator2 = ValueAnimator.ofFloat(getWidth(), myDiameter);
    myAnimator2.setStartDelay(100);
    Util.setAnimatorDuration(myAnimator2, 150);
    myAnimator2.setInterpolator(new LinearInterpolator());

    myAnimator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      @Override
      public void onAnimationUpdate(ValueAnimator animation) {
        float value = (float) animation.getAnimatedValue();
        myLeftOvalWidth = value;
        myRightOvalWidth = value;
        invalidateSelf();
      }
    });
  }

  @NonNull
  public ValueAnimator getAnimator2() {
    return myAnimator2;
  }

  @NonNull
  public ValueAnimator getAnimator1(final boolean leftToRight) {
    ValueAnimator animator1 = ValueAnimator.ofFloat(myDiameter, getWidth());
    animator1.setInterpolator(new LinearInterpolator());
    Util.setAnimatorDuration(animator1, 150);

    animator1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      @Override
      public void onAnimationUpdate(ValueAnimator animation) {
        float value = (float) animation.getAnimatedValue();

        if (leftToRight) {
          mySelectedCircleCenterX = value - myRadius;
          myLeftOvalWidth = value;
          myRightOvalWidth = myDiameter;
        } else {
          mySelectedCircleCenterX = getWidth() - value + myRadius;
          myRightOvalWidth = value;
          myLeftOvalWidth = myDiameter;
        }
        invalidateSelf();
      }
    });
    return animator1;
  }

  public void setSelectedCircle(boolean left) {
    mySelectedCircleCenterX = left ? myRadius : getWidth() - myRadius;
    myLeftOvalWidth = myDiameter;
    myRightOvalWidth = myDiameter;
    invalidateSelf();
  }

  @Override
  public void draw(Canvas canvas) {
    Rect bounds = getBounds();
    myTempRect.set(bounds.left, bounds.top, bounds.left + myLeftOvalWidth, bounds.bottom);
    canvas.drawRoundRect(myTempRect, myRadius, myRadius, myPaint);
    Path path = new Path();
    myTempRect.set(bounds.left + myLeftOvalWidth - myDiameter, bounds.top,
        bounds.left + myLeftOvalWidth, bounds.bottom);
    path.arcTo(myTempRect, -90f, 180f, true);
    path.lineTo(bounds.right, bounds.bottom);
    path.lineTo(bounds.right, bounds.top);
    path.lineTo(bounds.left + myLeftOvalWidth - myRadius, bounds.top);
    int saveCount = canvas.save();
    canvas.clipPath(path);
    myTempRect.set(bounds.right - myRightOvalWidth, bounds.top, bounds.right, bounds.bottom);
    canvas.drawRoundRect(myTempRect, myRadius, myRadius, myPaint);
    canvas.restoreToCount(saveCount);
    canvas.drawCircle(bounds.left + mySelectedCircleCenterX, bounds.top + myRadius, myRadius,
        mySelectedCirclePaint);
  }

  @Override
  public void setAlpha(int alpha) {
  }

  @Override
  public void setColorFilter(ColorFilter colorFilter) {
  }

  @Override
  public int getIntrinsicWidth() {
    return (int) getWidth();
  }

  private float getWidth() {
    return myRadius * 4 + myDistanceBetweenCircles;
  }

  @Override
  public int getIntrinsicHeight() {
    return (int) myDiameter;
  }

  @Override
  public int getOpacity() {
    return PixelFormat.TRANSLUCENT;
  }
}
