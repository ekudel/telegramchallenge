package com.kubro.telegramchallenge.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.TextView;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * @author Eugene.Kudelevsky
 */
public class VideoTimerView extends TextView {
  private float myRotationInDegrees = 0;
  private final Paint myPaint = new Paint();

  private final Rect myTempRect = new Rect();
  private long myTime = 0;
  private long myHours = 0;

  public VideoTimerView(Context context, AttributeSet attrs) {
    super(context, attrs);
    myPaint.setColor(getTextColors().getDefaultColor());
    myPaint.setTextSize(getTextSize());
  }

  public void setRotationInDegrees(float rotationInDegrees) {
    if (rotationInDegrees == myRotationInDegrees) {
      return;
    }
    myRotationInDegrees = rotationInDegrees;
    requestLayout();
  }

  public void setTime(long time) {
    if (time == myTime) {
      return;
    }
    myTime = time;
    long oldHours = myHours;
    myHours = TimeUnit.MILLISECONDS.toHours(myTime);

    if (oldHours == 0 && myHours != 0 || oldHours != 0 && myHours == 0) {
      requestLayout();
    } else {
      invalidate();
    }
  }

  @Override
  protected void onDraw(Canvas canvas) {
    canvas.rotate(myRotationInDegrees, (float) canvas.getWidth() / 2, (float) canvas.getHeight() / 2);

    float maxDimen = Math.max(canvas.getWidth(), canvas.getHeight());
    float minDimen = Math.min(canvas.getWidth(), canvas.getHeight());

    if (myRotationInDegrees == 90 || myRotationInDegrees == 270) {
      float d = (maxDimen - minDimen) / 2f;
      canvas.translate(-d, d);
    }
    String text = buildText(myTime);
    myPaint.getTextBounds(text, 0, text.length(), myTempRect);
    canvas.drawText(text, getPaddingLeft(), minDimen / 2 - myTempRect.exactCenterY(), myPaint);
  }

  @NonNull
  private static String buildText(long time) {
    final long hr = TimeUnit.MILLISECONDS.toHours(time);
    final long min = TimeUnit.MILLISECONDS.toMinutes(time - TimeUnit.HOURS.toMillis(hr));
    final long sec = TimeUnit.MILLISECONDS.toSeconds(time - TimeUnit.HOURS.toMillis(hr) -
        TimeUnit.MINUTES.toMillis(min));
    return hr > 0
        ? String.format(Locale.getDefault(), "%d:%02d:%02d", hr, min, sec)
        : String.format(Locale.getDefault(), "%02d:%02d", min, sec);
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    String measuringStr = myHours > 0 ? "00:00:00" : "00:00";
    myPaint.getTextBounds(measuringStr, 0, measuringStr.length(), myTempRect);
    boolean horizontal = myRotationInDegrees % 180 == 0;
    int w = myTempRect.width() + getPaddingLeft() + getPaddingRight();
    int h = myTempRect.height() + getPaddingTop() + getPaddingBottom();
    int width = Math.max(0, horizontal ? w : h);
    int height = Math.max(0, horizontal ? h : w);
    super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
        MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
  }
}
