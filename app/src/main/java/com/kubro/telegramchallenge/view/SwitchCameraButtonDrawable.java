package com.kubro.telegramchallenge.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.graphics.drawable.DrawableWrapper;
import android.view.animation.DecelerateInterpolator;

import com.kubro.telegramchallenge.R;
import com.kubro.telegramchallenge.Util;

/**
 * @author Eugene.Kudelevsky
 */
public class SwitchCameraButtonDrawable extends DrawableWrapper {
  private final float myCircleRadius;
  private float myInternalCircleRadius;
  private float myRotation = 0;
  private final AnimatorSet myToFrontAnimator;
  private final AnimatorSet myToBackAnimator;
  private final Paint myPaint = new Paint();

  public SwitchCameraButtonDrawable(@NonNull Context context) {
    super(context.getResources().getDrawable(R.drawable.btn_switch));
    Resources resources = context.getResources();
    myCircleRadius = resources.getDimension(R.dimen.switch_button_circle_radius);
    float defaultInternalCircleRadius = resources.getDimension(R.dimen.switch_button_internal_circle_radius);
    myInternalCircleRadius = defaultInternalCircleRadius;
    ValueAnimator rotateAnimator = ValueAnimator.ofFloat(0f, -180f);

    rotateAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      @Override
      public void onAnimationUpdate(ValueAnimator animation) {
        myRotation = (float) animation.getAnimatedValue();
        invalidateSelf();
      }
    });
    ValueAnimator.AnimatorUpdateListener circleAnimatorListener = new ValueAnimator.AnimatorUpdateListener() {
      @Override
      public void onAnimationUpdate(ValueAnimator animation) {
        myInternalCircleRadius = (float) animation.getAnimatedValue();
        invalidateSelf();
      }
    };
    ValueAnimator circleAnimator = ValueAnimator.ofFloat(0f, defaultInternalCircleRadius);
    circleAnimator.addUpdateListener(circleAnimatorListener);

    ValueAnimator circleAnimatorToggled = ValueAnimator.ofFloat(defaultInternalCircleRadius, 0f);
    circleAnimatorToggled.addUpdateListener(circleAnimatorListener);

    myToFrontAnimator = new AnimatorSet();
    myToFrontAnimator.playTogether(rotateAnimator, circleAnimator);
    myToFrontAnimator.setInterpolator(new DecelerateInterpolator());
    myToBackAnimator = new AnimatorSet();
    myToBackAnimator.playTogether(rotateAnimator, circleAnimatorToggled);
    myToBackAnimator.setInterpolator(new DecelerateInterpolator());

    myPaint.setColor(Color.WHITE);
    myPaint.setStyle(Paint.Style.STROKE);
    myPaint.setAntiAlias(true);
  }

  @NonNull
  public Animator getAnimator(boolean toBack) {
    return toBack ? myToBackAnimator : myToFrontAnimator;
  }

  @Override
  public void draw(Canvas canvas) {
    final int saveCount = canvas.save();
    Drawable drawable = getWrappedDrawable();
    final Rect bounds = drawable.getBounds();
    float cx = (float) (bounds.left + bounds.right) / 2;
    float cy = (float) (bounds.top + bounds.bottom) / 2;

    if (Math.abs(myRotation) > Util.EPS) {
      canvas.rotate(myRotation, cx, cy);
    }
    drawable.draw(canvas);
    float strokeWidth = myCircleRadius - myInternalCircleRadius;
    myPaint.setStrokeWidth(strokeWidth);
    canvas.drawCircle(cx, cy, myCircleRadius - (strokeWidth / 2), myPaint);
    canvas.restoreToCount(saveCount);
  }
}
