package com.kubro.telegramchallenge.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

import com.kubro.telegramchallenge.MainFragment;
import com.kubro.telegramchallenge.Util;

/**
 * @author Eugene.Kudelevsky
 */
public class SwitchCameraAnimationProvider {
  public static final int FADE_DURATION = 250;

  private final ValueAnimator myFadeInAnimator;
  private final ValueAnimator myPreviewAnimator;
  private final MainFragment myFragment;

  private AnimatorSet myFadeOutAnimator;
  private boolean myStartFadeInAutomatically = false;

  @Nullable private Runnable myAnimationEndListener;

  public SwitchCameraAnimationProvider(@NonNull MainFragment fragment) {
    myFragment = fragment;
    myPreviewAnimator = ValueAnimator.ofFloat(0, 1);

    myPreviewAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      @Override
      public void onAnimationUpdate(ValueAnimator animation) {
        float value = (float) animation.getAnimatedValue();
        myFragment.getCameraPreviewBlackout().setAlpha(value);
        myFragment.getFlashButtonsPanel().setAlpha(1 - value);
      }
    });
    myPreviewAnimator.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationStart(Animator animation) {
        myFragment.getCameraPreviewBlackout().setVisibility(View.VISIBLE);
      }

      @Override
      public void onAnimationEnd(Animator animation) {
        if (myStartFadeInAutomatically) {
          myFadeInAnimator.start();
        }
      }
    });
    myPreviewAnimator.setInterpolator(new AccelerateInterpolator());
    myFadeInAnimator = ValueAnimator.ofFloat(1, 0);

    myFadeInAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      @Override
      public void onAnimationUpdate(ValueAnimator animation) {
        float value = (float) animation.getAnimatedValue();
        myFragment.getCameraPreviewBlackout().setAlpha(value);
        myFragment.getFlashButtonsPanel().setAlpha(1 - value);
      }
    });
    myFadeInAnimator.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationEnd(Animator animation) {
        myFragment.getCameraPreviewBlackout().setVisibility(View.GONE);

        if (myAnimationEndListener != null) {
          myAnimationEndListener.run();
        }
      }
    });
    myFadeInAnimator.setInterpolator(new AccelerateInterpolator());
    Util.setAnimatorDuration(myFadeInAnimator, FADE_DURATION);
  }

  public void setAnimationEndListener(@Nullable Runnable animationEndListener) {
    myAnimationEndListener = animationEndListener;
  }

  public void startFadeOut(boolean toBackCamera) {
    myStartFadeInAutomatically = false;

    if (myFadeOutAnimator != null) {
      myFadeOutAnimator.cancel();
    }
    myFadeInAnimator.cancel();
    myFragment.getCameraPreviewBlackout().setAlpha(0);
    myFragment.getFlashButtonsPanel().setAlpha(1);
    myFragment.getCameraPreviewBlackout().setVisibility(View.GONE);
    myFadeOutAnimator = new AnimatorSet();
    Util.setAnimatorDuration(myFadeOutAnimator, FADE_DURATION);
    Animator buttonAnimator = myFragment.getSwitchCameraButtonDrawable().getAnimator(toBackCamera);
    myFadeOutAnimator.playTogether(buttonAnimator, myPreviewAnimator);
    myFadeOutAnimator.start();
  }

  public void scheduleFadeIn() {
    if (myFadeOutAnimator != null && myFadeOutAnimator.isRunning()) {
      myStartFadeInAutomatically = true;
    } else {
      myFadeInAnimator.start();
    }
  }
}
