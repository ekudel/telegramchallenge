package com.kubro.telegramchallenge.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.kubro.telegramchallenge.R;
import com.kubro.telegramchallenge.util.Size;

/**
 * @author Eugene.Kudelevsky
 */
public class BottomPanelLayout extends FrameLayout {
  private int myMinButtonPanelHeight;

  public BottomPanelLayout(Context context) {
    super(context);
  }

  public BottomPanelLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
    myMinButtonPanelHeight = context.getResources().
        getDimensionPixelSize(R.dimen.min_photo_bottom_panel_height);
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    Size previewSize = MainPanelLayout.computeSize(
        widthMeasureSpec, heightMeasureSpec, myMinButtonPanelHeight);
    int height = MeasureSpec.getSize(heightMeasureSpec) - previewSize.getHeight();
    super.onMeasure(MeasureSpec.makeMeasureSpec(previewSize.getWidth(), MeasureSpec.EXACTLY),
        MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
  }
}