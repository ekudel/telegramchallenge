package com.kubro.telegramchallenge;


import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.kubro.telegramchallenge.util.Consumer;
import com.kubro.telegramchallenge.util.Disposable;
import com.kubro.telegramchallenge.view.CaptureButtonDrawable;
import com.kubro.telegramchallenge.view.CaptureModeIndicatorDrawable;
import com.kubro.telegramchallenge.view.MainPanelLayout;
import com.kubro.telegramchallenge.view.SwitchCameraAnimationProvider;
import com.kubro.telegramchallenge.view.SwitchCameraButtonDrawable;
import com.kubro.telegramchallenge.view.ToggleImageButton;
import com.kubro.telegramchallenge.view.VideoTimerView;

import java.util.Collections;
import java.util.List;

public class MainFragment extends FragmentBase {
  private static final String LOG_TAG = MainFragment.class.getSimpleName();
  public static final int REQUEST_CAMERA_PERMISSION = 1;
  private static final String FRAGMENT_DIALOG = "dialog";

  private TextureView myCameraPreview;
  private MainPanelLayout myMainPanelLayout;
  private ImageButton myCaptureButton;
  private View myCameraPreviewBlackout;
  private ImageButton mySwitchToFrontCameraButton;
  private View myCaptureControlPanel;
  private ToggleImageButton myCaptureModeIndicator;
  private View myBottomPanel;
  private View myBottomPanelBlackout;
  private View myFlashButtonsPanel;
  private ImageButton myFlashAutoButton;
  private ImageButton myFlashOnButton;
  private ImageButton myFlashOffButton;
  private VideoTimerView myVideoRecordingTimer;

  private SwitchCameraButtonDrawable mySwitchCameraButtonDrawable;
  private SwitchCameraAnimationProvider mySwitchCameraAnimationProvider;
  private boolean mySurfaceCreated = false;
  private CaptureModeIndicatorDrawable myCaptureModeIndicatorDrawable;
  private CaptureButtonDrawable myCaptureButtonDrawable;
  private List<CameraType> mySupportedCameraTypes;
  private FlashMode myCurrentFlashMode;
  private CameraType myCurrentCameraType;
  private List<FlashMode> mySupportedFlashModes = Collections.emptyList();
  private Disposable myCameraSwitchingDisposable;
  private CameraHelperBase myCameraHelper;
  private Disposable myVideoRecordingTimerUpdatingDisposable;
  private boolean myDestroyed;

  private boolean myRestartPreviewScheduled = false;
  private Runnable myRunAfterPreviewRestart;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_main, container, false);
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    View view = getView();
    assert view != null;
    myCameraPreview = (TextureView) view.findViewById(R.id.cameraPreview);
    myMainPanelLayout = (MainPanelLayout) view.findViewById(R.id.mainPanelLayout);
    myCameraPreviewBlackout = view.findViewById(R.id.cameraPreviewBlackout);
    myCaptureButton = (ImageButton) view.findViewById(R.id.capturePhotoButton);
    mySwitchToFrontCameraButton = (ToggleImageButton) view.findViewById(R.id.switchToFrontCameraButton);
    myCaptureControlPanel = view.findViewById(R.id.captureControlPanel);
    myCaptureModeIndicator = (ToggleImageButton) view.findViewById(R.id.captureModeIndicator);
    myBottomPanel = view.findViewById(R.id.bottomPanel);
    myBottomPanelBlackout = view.findViewById(R.id.bottomPanelBlackout);
    myFlashButtonsPanel = view.findViewById(R.id.flashButtonsPanel);
    myFlashAutoButton = (ImageButton) view.findViewById(R.id.flashAutoButton);
    myFlashOnButton = (ImageButton) view.findViewById(R.id.flashOnButton);
    myFlashOffButton = (ImageButton) view.findViewById(R.id.flashOffButton);
    myVideoRecordingTimer = (VideoTimerView) view.findViewById(R.id.videoRecordingTimer);

    myDestroyed = false;
    mySwitchCameraButtonDrawable = new SwitchCameraButtonDrawable(getActivity());
    mySwitchToFrontCameraButton.setImageDrawable(mySwitchCameraButtonDrawable);
    mySwitchCameraAnimationProvider = new SwitchCameraAnimationProvider(this);
    mySurfaceCreated = false;
    myCurrentFlashMode = null;
    mySupportedFlashModes = Collections.emptyList();
    mySupportedCameraTypes = CameraHelper.getSupportedCameraTypes();
    myCurrentCameraType = mySupportedCameraTypes.isEmpty() ? null : mySupportedCameraTypes.get(0);
    myRestartPreviewScheduled = false;
    myRunAfterPreviewRestart = null;

    myCameraPreview.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
      @Override
      public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        mySurfaceCreated = true;
        resetCamera(false);
      }

      @Override
      public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        if (!myRestartPreviewScheduled) {
          return;
        }
        final Runnable runAfter = myRunAfterPreviewRestart;
        myRestartPreviewScheduled = false;
        myRunAfterPreviewRestart = null;

        myCameraHelper.changePreviewSize(width, height, new Runnable() {
          @Override
          public void run() {
            if (runAfter != null) {
              runAfter.run();
            }
          }
        });
      }

      @Override
      public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return true;
      }

      @Override
      public void onSurfaceTextureUpdated(SurfaceTexture surface) {
      }
    });
    myCaptureModeIndicatorDrawable = new CaptureModeIndicatorDrawable(getActivity());
    myCaptureModeIndicator.setImageDrawable(myCaptureModeIndicatorDrawable);

    myCaptureButtonDrawable = new CaptureButtonDrawable(getActivity());
    myCaptureButton.setImageDrawable(myCaptureButtonDrawable);
    myCaptureButton.setOnTouchListener(new CaptureButtonTouchListener());

    mySwitchCameraAnimationProvider.setAnimationEndListener(new Runnable() {
      @Override
      public void run() {
        StateAwareActivity activity = getActivityInterface();

        if (activity != null && activity.getState() instanceof ActivityState.SwitchingCamera) {
          activity.updateState(((ActivityState.SwitchingCamera) activity.getState()).getNextState());
        }
      }
    });
    mySwitchToFrontCameraButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        switchCameraToFrontOrRare();
      }
    });
    StateAwareActivity activity = getActivityInterface();
    assert activity != null;
    new MySwipeDetector().install(view);

    initFlashButtons();
    myVideoRecordingTimer.setTime(0);

    mySwitchToFrontCameraButton.setVisibility(
        mySupportedCameraTypes.contains(CameraType.FRONT) &&
            mySupportedCameraTypes.contains(CameraType.BACK)
            ? View.VISIBLE : View.GONE);
    myCameraHelper = new Camera2Helper(getActivity(), new MyCameraHelperCallback());
  }

  @Override
  public void onPause() {
    super.onPause();

    if (isVisible()) {
      if (myCameraSwitchingDisposable != null) {
        myCameraSwitchingDisposable.dispose();
        myCameraSwitchingDisposable = null;
      }
      myCameraHelper.closeCamera();
    }
  }

  @Override
  public void onResume() {
    super.onResume();

    if (isVisible() && mySurfaceCreated) {
      resetCamera(false);
    }
  }

  @Override
  public void onDestroy() {
    myDestroyed = true;
    myCameraHelper.release();
    super.onDestroy();
  }

  @Override
  public void onHiddenChanged(boolean hidden) {
    super.onHiddenChanged(hidden);

    if (hidden) {
      if (myCameraSwitchingDisposable != null) {
        myCameraSwitchingDisposable.dispose();
        myCameraSwitchingDisposable = null;
      }
      myCameraHelper.closeCamera();
    } else if (mySurfaceCreated) {
      resetCamera(false);
    }
  }

  public void resetCamera(boolean force) {
    resetCamera(force, null);
  }

  @Nullable
  private Disposable resetCamera(boolean force, @Nullable final Runnable runAfter) {
    ActivityInterface activityInterface = getActivityInterface();

    if (myCurrentCameraType == null || activityInterface == null) {
      if (runAfter != null) {
        runAfter.run();
      }
      return null;
    }
    boolean videoMode = activityInterface.getState() == ActivityState.CAPTURING_VIDEO;
    int displayRotation = activityInterface.getSensorOrientationInDegrees();
    myCameraHelper.setSurfaceTexture(myCameraPreview.getSurfaceTexture());

    return myCameraHelper.resetCamera(myCameraPreview.getWidth(), myCameraPreview.getHeight(),
        myCurrentCameraType, displayRotation, myCurrentFlashMode, videoMode, force, new Runnable() {
          @Override
          public void run() {
            myCameraHelper.getSupportedFlashModes(new Consumer<List<FlashMode>>() {
              @Override
              public void consume(List<FlashMode> flashModes) {
                if (myDestroyed) {
                  return;
                }
                mySupportedFlashModes = flashModes;

                if (mySupportedFlashModes.size() == 1 && mySupportedFlashModes.get(0) == FlashMode.OFF) {
                  mySupportedFlashModes = Collections.emptyList();
                }
                if (myCurrentFlashMode == null || !flashModes.contains(myCurrentFlashMode)) {
                  myCurrentFlashMode = mySupportedFlashModes.isEmpty() ? null : mySupportedFlashModes.get(0);
                  updateFlashButtonsVisibility();
                }
              }
            });
            if (runAfter != null) {
              runAfter.run();
            }
          }
        });
  }

  @Nullable
  public ActivityInterface getActivityInterface() {
    return (ActivityInterface) getActivity();
  }

  private void startVideoRecordingTimerUpdating() {
    if (myVideoRecordingTimerUpdatingDisposable != null) {
      myVideoRecordingTimerUpdatingDisposable.dispose();
    }
    final boolean[] canceledFlagRef = {false};
    doStartVideoRecordingUpdatingTimer(canceledFlagRef);
    myVideoRecordingTimerUpdatingDisposable = new Disposable() {
      @Override
      public void dispose() {
        canceledFlagRef[0] = true;
      }
    };
  }

  private void doStartVideoRecordingUpdatingTimer(final boolean[] cancelFlagRef) {
    myVideoRecordingTimer.postDelayed(new Runnable() {
      @Override
      public void run() {
        if (myDestroyed || cancelFlagRef[0]) {
          return;
        }
        myVideoRecordingTimer.setTime(System.currentTimeMillis() -
            myCameraHelper.getRecordingStartTime());
        doStartVideoRecordingUpdatingTimer(cancelFlagRef);
      }
    }, 1000);
  }

  private void stopVideoRecordingTimerUpdating() {
    if (myVideoRecordingTimerUpdatingDisposable != null) {
      myVideoRecordingTimerUpdatingDisposable.dispose();
      myVideoRecordingTimerUpdatingDisposable = null;
    }
    myVideoRecordingTimer.setTime(0);
  }

  private void initFlashButtons() {
    final int flashButtonTopMargin = getResources().
        getDimensionPixelSize(R.dimen.flash_button_top_margin);

    View.OnClickListener listener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        FlashMode oldFlashMode = myCurrentFlashMode;

        if (oldFlashMode == null) {
          Log.e(LOG_TAG, "flash mode is null");
          return;
        }
        if (mySupportedFlashModes.isEmpty()) {
          Log.e(LOG_TAG, "empty flash modes");
          return;
        }
        int i = 0;

        while (i < mySupportedFlashModes.size() && oldFlashMode != mySupportedFlashModes.get(i)) {
          i++;
        }
        if (i >= mySupportedFlashModes.size()) {
          throw new IllegalStateException("unknown flash mode " + oldFlashMode);
        }
        myCurrentFlashMode = i + 1 < mySupportedFlashModes.size() ? mySupportedFlashModes.get(i + 1)
            : mySupportedFlashModes.get(0);
        myCameraHelper.setFlashMode(myCurrentFlashMode);
        StateAwareActivity activity = getActivityInterface();

        if (oldFlashMode != myCurrentFlashMode && activity != null) {
          activity.updateStateBeforePlayingAnimation();
          buildSwitchFlashModeAnimator(oldFlashMode, myCurrentFlashMode, flashButtonTopMargin).start();
        }
      }
    };
    for (FlashMode flashMode : FlashMode.values()) {
      View button = getFlashButton(flashMode);
      FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) button.getLayoutParams();
      layoutParams.topMargin = flashButtonTopMargin;
      button.setLayoutParams(layoutParams);
      button.setOnClickListener(listener);
    }
    updateFlashButtonsVisibility();
  }

  private void updateFlashButtonsVisibility() {
    for (FlashMode flashMode : FlashMode.values()) {
      getFlashButton(flashMode).setVisibility(
          myCurrentFlashMode == flashMode ? View.VISIBLE : View.GONE);
    }
  }

  @NonNull
  private ImageButton getFlashButton(@NonNull FlashMode mode) {
    switch (mode) {
      case AUTO:
        return myFlashAutoButton;
      case ON:
        return myFlashOnButton;
      case OFF:
        return myFlashOffButton;
      default:
        throw new IllegalArgumentException();
    }
  }

  private Animator buildSwitchFlashModeAnimator(@NonNull final FlashMode from, @NonNull FlashMode to,
                                                final int baseTopMargin) {
    ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f);
    Util.setAnimatorDuration(animator, 160);
    animator.setInterpolator(new DecelerateInterpolator(0.4f));
    final View fromButton = getFlashButton(from);
    final View toButton = getFlashButton(to);

    animator.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationStart(Animator animation) {
        fromButton.setVisibility(View.VISIBLE);
        toButton.setVisibility(View.VISIBLE);
      }

      @Override
      public void onAnimationEnd(Animator animation) {
        fromButton.setVisibility(View.GONE);
        fromButton.setAlpha(1f);
        FrameLayout.LayoutParams fromButtonLayoutParams =
            (FrameLayout.LayoutParams) fromButton.getLayoutParams();
        fromButtonLayoutParams.topMargin = baseTopMargin;
        fromButton.setLayoutParams(fromButtonLayoutParams);
        StateAwareActivity activity = getActivityInterface();

        if (activity != null) {
          activity.updateStateAfterPlayingAnimation();
        }
      }
    });
    final float shift = fromButton.getHeight() + getResources().getDimension(
        R.dimen.switch_flash_mode_animator_gap_between_buttons);

    animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      @Override
      public void onAnimationUpdate(ValueAnimator animation) {
        float value = (float) animation.getAnimatedValue();
        FrameLayout.LayoutParams fromButtonLayoutParams =
            (FrameLayout.LayoutParams) fromButton.getLayoutParams();
        fromButtonLayoutParams.topMargin = baseTopMargin + (int) (value * shift);
        fromButton.setLayoutParams(fromButtonLayoutParams);
        FrameLayout.LayoutParams toButtonLayoutParams =
            (FrameLayout.LayoutParams) toButton.getLayoutParams();
        toButtonLayoutParams.topMargin = baseTopMargin + (int) ((value - 1f) * shift);
        toButton.setLayoutParams(toButtonLayoutParams);
        fromButton.setAlpha(1f - value);
      }
    });
    return animator;
  }

  private void switchCameraToFrontOrRare() {
    StateAwareActivity activity = getActivityInterface();

    if (activity == null || activity.getState() != ActivityState.CAPTURING_PHOTO &&
        activity.getState() != ActivityState.CAPTURING_VIDEO) {
      return;
    }
    CameraType newCameraType = myCurrentCameraType == CameraType.FRONT
        ? CameraType.BACK : CameraType.FRONT;

    if (!mySupportedCameraTypes.contains(newCameraType)) {
      return;
    }
    myCurrentCameraType = newCameraType;
    myCurrentFlashMode = null;
    updateFlashButtonsVisibility();
    mySwitchCameraAnimationProvider.startFadeOut(myCurrentCameraType == CameraType.FRONT);

    if (myCameraSwitchingDisposable != null) {
      myCameraSwitchingDisposable.dispose();
      myCameraSwitchingDisposable = null;
    }
    activity.updateState(new ActivityState.SwitchingCamera(activity.getState()));
    final boolean[] canceled = {false};

    final Disposable disposable = resetCamera(false, new Runnable() {
      @Override
      public void run() {
        if (!canceled[0]) {
          mySwitchCameraAnimationProvider.scheduleFadeIn();
        }
      }
    });
    if (disposable != null) {
      myCameraSwitchingDisposable = new Disposable() {
        @Override
        public void dispose() {
          canceled[0] = true;
          disposable.dispose();
        }
      };
    }
  }

  public View getCameraPreviewBlackout() {
    return myCameraPreviewBlackout;
  }

  public View getFlashButtonsPanel() {
    return myFlashButtonsPanel;
  }

  public SwitchCameraButtonDrawable getSwitchCameraButtonDrawable() {
    return mySwitchCameraButtonDrawable;
  }

  public View getCaptureControlPanel() {
    return myCaptureControlPanel;
  }

  public ToggleImageButton getCaptureModeIndicator() {
    return myCaptureModeIndicator;
  }

  private void changeCaptureMode(boolean toVideo, @Nullable final Runnable runAfter) {
    myCameraHelper.setupFocusMode(toVideo);
    myMainPanelLayout.setMatchParentHeight(toVideo);
    myRestartPreviewScheduled = true;
    myRunAfterPreviewRestart = runAfter;
  }

  @Override
  public boolean canHandleOrientationChange() {
    ActivityInterface activityInterface = getActivityInterface();

    if (activityInterface != null) {
      ActivityState state = activityInterface.getState();
      return state == ActivityState.CAPTURING_PHOTO || state == ActivityState.CAPTURING_VIDEO ||
          state instanceof ActivityState.RecordingVideo;
    }
    return false;
  }

  @Override
  public Animator buildOrientationChangeAnimator(final int startDegrees, final int deltaDegrees) {
    final int endDegrees = (360 + startDegrees + deltaDegrees) % 360;
    ValueAnimator animator = ValueAnimator.ofFloat(1, 0, 1);

    animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      private boolean myFlag = false;

      @Override
      public void onAnimationUpdate(ValueAnimator animation) {
        float value = (float) animation.getAnimatedValue();
        myFlashButtonsPanel.setAlpha(value);
        myVideoRecordingTimer.setAlpha(value);

        if (!myFlag && animation.getAnimatedFraction() > 0.5 - Util.EPS) {
          myFlag = true;
          @SuppressWarnings("ConstantConditions")
          Bitmap bitmap = ((BitmapDrawable) getResources().getDrawable(
              R.drawable.flash_auto)).getBitmap();
          final float endRotation = (360 - startDegrees - deltaDegrees) % 360;
          Matrix matrix = new Matrix();
          matrix.postRotate(endRotation, (float) bitmap.getWidth() / 2,
              (float) bitmap.getHeight() / 2);

          for (FlashMode flashMode : FlashMode.values()) {
            getFlashButton(flashMode).setImageMatrix(matrix);
          }
          myVideoRecordingTimer.setRotationInDegrees((360 - endDegrees) % 360);
        }
      }
    });
    animator.setInterpolator(new LinearInterpolator());
    Util.setAnimatorDuration(animator, 300);
    return animator;
  }

  @Override
  public void setUiOrientation(int degrees) {
    final float rotation = (360 - degrees) % 360;
    @SuppressWarnings("ConstantConditions")
    Bitmap bitmap = ((BitmapDrawable) getResources().getDrawable(
        R.drawable.flash_auto)).getBitmap();
    Matrix matrix = new Matrix();
    matrix.postRotate(rotation, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);

    for (FlashMode flashMode : FlashMode.values()) {
      getFlashButton(flashMode).setImageMatrix(matrix);
    }
    myVideoRecordingTimer.setRotationInDegrees(rotation);
  }

  @Override
  public void onSensorOrientationChanged(int degrees) {
    myCameraHelper.setDisplayRotation(degrees);
  }

  private class MySwipeDetector extends SwipeDetector {
    public MySwipeDetector() {
      super(getActivity());
    }

    @Override
    protected void onSwipeRight() {
      StateAwareActivity activity = getActivityInterface();

      if (activity != null && activity.getState() == ActivityState.CAPTURING_VIDEO) {
        switchCaptureMode(false);
      }
    }

    @Override
    protected void onSwipeLeft() {
      StateAwareActivity activity = getActivityInterface();

      if (activity != null && activity.getState() == ActivityState.CAPTURING_PHOTO) {
        switchCaptureMode(true);
      }
    }

    private void switchCaptureMode(final boolean toVideo) {
      StateAwareActivity activity = getActivityInterface();

      if (activity == null) {
        return;
      }
      activity.updateStateBeforeTransition(toVideo ? ActivityState.CAPTURING_VIDEO
          : ActivityState.CAPTURING_PHOTO);

      final ValueAnimator bottomPanelAnimator = toVideo
          ? myCaptureButtonDrawable.getCapturingPhotoState().createToCapturingVideoAnimator()
          : myCaptureButtonDrawable.getCapturingVideoState().createToCapturingPhotoAnimator();

      bottomPanelAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
          float value = myCaptureButtonDrawable.getShape2Alpha() / 255f;
          myBottomPanelBlackout.setAlpha(
              value * (1f - Util.VIDEO_BOTTOM_PANEL_ALPHA) + Util.VIDEO_BOTTOM_PANEL_ALPHA);
          myFlashButtonsPanel.setAlpha(value);
        }
      });
      bottomPanelAnimator.addListener(new AnimatorListenerAdapter() {
        @Override
        public void onAnimationStart(Animator animation) {
          if (!toVideo) {
            myFlashButtonsPanel.setVisibility(View.VISIBLE);
          }
        }

        @Override
        public void onAnimationEnd(Animator animation) {
          if (toVideo) {
            myFlashButtonsPanel.setVisibility(View.GONE);
          }
        }
      });
      final Runnable secondPart = new Runnable() {
        private boolean myFlag = false;

        @Override
        public void run() {
          if (myFlag || myDestroyed) return;
          myFlag = true;
          ObjectAnimator previewAnimator2 = ObjectAnimator.ofFloat(myCameraPreviewBlackout, "alpha", 1f, 0f);
          previewAnimator2.setInterpolator(new LinearInterpolator());
          Util.setAnimatorDuration(previewAnimator2, 150);

          previewAnimator2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
              StateAwareActivity activity = getActivityInterface();

              if (activity != null) {
                myCameraPreviewBlackout.setVisibility(View.GONE);
                activity.updateStateAfterPlayingAnimation();
              }
            }
          });
          AnimatorSet set = new AnimatorSet();

          if (toVideo) {
            set.playTogether(previewAnimator2, myCaptureModeIndicatorDrawable.getAnimator2());
          } else {
            set.playTogether(previewAnimator2, bottomPanelAnimator, myCaptureModeIndicatorDrawable.getAnimator2());
          }
          set.start();
        }
      };
      Animator previewAnimator1 = ObjectAnimator.ofFloat(myCameraPreviewBlackout, "alpha", 0f, 1f);
      previewAnimator1.setInterpolator(new LinearInterpolator());
      Util.setAnimatorDuration(previewAnimator1, 150);

      previewAnimator1.addListener(new AnimatorListenerAdapter() {
        @Override
        public void onAnimationStart(Animator animation) {
          myCameraPreviewBlackout.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAnimationEnd(Animator animation) {
          changeCaptureMode(toVideo, secondPart);
          myCameraPreview.postDelayed(secondPart, 2000);
        }
      });
      AnimatorSet set = new AnimatorSet();

      if (toVideo) {
        set.playTogether(bottomPanelAnimator, previewAnimator1, myCaptureModeIndicatorDrawable.getAnimator1(true));
      } else {
        set.playTogether(previewAnimator1, myCaptureModeIndicatorDrawable.getAnimator1(false));
      }
      set.start();
    }
  }

  private class CaptureButtonTouchListener implements View.OnTouchListener {
    private final GestureDetector myGestureDetector;
    private Animator myStartVideoRecordingAnimator;
    private Animator myStopVideoRecordingAnimator;

    public CaptureButtonTouchListener() {
      myGestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onDown(MotionEvent e) {
          return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {
          handleLongTap();
        }
      });
    }

    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
      if (myGestureDetector.onTouchEvent(event)) {
        return true;
      }
      if (event.getAction() == MotionEvent.ACTION_UP) {
        return handleUp();
      }
      return false;
    }

    private boolean handleLongTap() {
      ActivityInterface activityInterface = getActivityInterface();

      if (activityInterface == null) {
        return false;
      }
      if (activityInterface.getState() == ActivityState.CAPTURING_PHOTO) {
        activityInterface.initOutputVideoFile(new Consumer<String>() {
          @Override
          public void consume(final String outputVideoFilePath) {
            final ActivityInterface activity = getActivityInterface();

            if (outputVideoFilePath == null || activity == null || myDestroyed) {
              return;
            }
            activity.updateStateBeforeTransition(new ActivityState.RecordingVideo(true));
            changeCaptureMode(true, null);

            myMainPanelLayout.post(new Runnable() {
              @Override
              public void run() {
                ActivityInterface activityInterface = getActivityInterface();

                if (!myDestroyed && activityInterface != null) {
                  int displayRotation = activityInterface.getSensorOrientationInDegrees();

                  myStartVideoRecordingAnimator = buildStartVideoRecordingByLongTapAnimator();
                  myStartVideoRecordingAnimator.start();
                  myCameraHelper.startVideoRecording(new Surface(myCameraPreview.getSurfaceTexture()),
                      displayRotation, outputVideoFilePath, new Consumer<Boolean>() {
                        @Override
                        public void consume(Boolean success) {
                          if (success && !myDestroyed) {
                            startVideoRecordingTimerUpdating();
                          }
                        }
                      });
                }
              }
            });
          }
        });
        return true;
      }
      return false;
    }

    private Animator buildStartVideoRecordingByLongTapAnimator() {
      ValueAnimator buttonAnimator = myCaptureButtonDrawable.getCapturingPhotoState()
          .createToRecordingVideoAnimator();
      ValueAnimator additionalAnimator = ValueAnimator.ofFloat(1f, 0f);
      final int bottomPanelHeight = myBottomPanel.getHeight();

      additionalAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
          float value = (float) animation.getAnimatedValue();
          myCaptureModeIndicator.setAlpha(value);
          myVideoRecordingTimer.setAlpha(1f - value);
          mySwitchToFrontCameraButton.setAlpha(value);
          myBottomPanelBlackout.setAlpha(value);
          myFlashButtonsPanel.setAlpha(value);
          FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)
              myBottomPanelBlackout.getLayoutParams();
          layoutParams.topMargin = bottomPanelHeight - (int) (bottomPanelHeight * value);
          myBottomPanelBlackout.setLayoutParams(layoutParams);
        }
      });
      additionalAnimator.addListener(new AnimatorListenerAdapter() {
        @Override
        public void onAnimationStart(Animator animation) {
          myVideoRecordingTimer.setVisibility(View.VISIBLE);
        }
      });
      Util.setAnimatorDuration(additionalAnimator, 170);
      additionalAnimator.setInterpolator(new DecelerateInterpolator());
      AnimatorSet result = new AnimatorSet();
      result.playTogether(buttonAnimator, additionalAnimator);

      result.addListener(new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
          StateAwareActivity activity = getActivityInterface();

          if (activity != null) {
            activity.updateStateAfterPlayingAnimation();
          }
        }
      });
      return result;
    }

    private boolean handleUp() {
      ActivityInterface activity = getActivityInterface();

      if (activity == null) {
        return false;
      }
      if (activity.getState() == ActivityState.CAPTURING_VIDEO) {
        activity.initOutputVideoFile(new Consumer<String>() {
          @Override
          public void consume(String outputVideoFilePath) {
            startVideoRecording(outputVideoFilePath);
          }
        });
        return true;
      } else if (activity.getState() instanceof ActivityState.RecordingVideo) {
        final boolean byLongTap = ((ActivityState.RecordingVideo) activity.getState()).isByLongTap();
        activity.updateStateBeforeTransition(ActivityState.BROWSING_VIDEO);

        if (myStartVideoRecordingAnimator != null) {
          myStartVideoRecordingAnimator.end();
        }
        if (byLongTap) {
          myCaptureModeIndicator.setChecked(true);
          myCaptureModeIndicatorDrawable.setSelectedCircle(false);
        }
        myStopVideoRecordingAnimator = buildStartOrStopVideoRecordingAnimator(false);
        myStopVideoRecordingAnimator.start();
        stopVideoRecordingTimerUpdating();

        myCameraHelper.stopVideoRecording(new Consumer<Boolean>() {
          @Override
          public void consume(final Boolean success) {
            StateAwareActivity activity = getActivityInterface();

            if (myDestroyed || activity == null) {
              return;
            }
            onVideoRecordingStopped(success);
          }
        });
        return true;
      } else if (activity.getState() == ActivityState.CAPTURING_PHOTO) {
        activity.updateState(ActivityState.TAKING_PICTURE);
        myCaptureButtonDrawable.getCapturingPhotoState().createTakePictureAnimator().start();
        myCameraHelper.takePictureWhenFocused();
        return true;
      }
      return false;
    }

    private void startVideoRecording(@Nullable String outputVideoFilePath) {
      ActivityInterface activityInterface = getActivityInterface();

      if (outputVideoFilePath == null || activityInterface == null || myDestroyed) {
        return;
      }
      activityInterface.updateStateBeforeTransition(new ActivityState.RecordingVideo(false));
      myStartVideoRecordingAnimator = buildStartOrStopVideoRecordingAnimator(true);

      myStartVideoRecordingAnimator.addListener(new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
          StateAwareActivity activity = getActivityInterface();

          if (activity != null) {
            activity.updateStateAfterPlayingAnimation();
          }
        }
      });
      myStartVideoRecordingAnimator.start();
      int displayRotation = activityInterface.getSensorOrientationInDegrees();

      myCameraHelper.startVideoRecording(new Surface(myCameraPreview.getSurfaceTexture()),
          displayRotation, outputVideoFilePath, new Consumer<Boolean>() {
            @Override
            public void consume(Boolean success) {
              if (success && !myDestroyed) {
                startVideoRecordingTimerUpdating();
              }
            }
          });
    }

    private void onVideoRecordingStopped(final boolean success) {
      final ActivityInterface activity = getActivityInterface();

      if (activity == null) {
        return;
      }
      if (myStopVideoRecordingAnimator != null && myStopVideoRecordingAnimator.isStarted()) {
        myStopVideoRecordingAnimator.addListener(new AnimatorListenerAdapter() {
          @Override
          public void onAnimationEnd(Animator animation) {
            ActivityInterface activity = getActivityInterface();

            if (activity != null) {
              activity.onVideoRecordingFinished(success);
            }
          }
        });
      } else {
        activity.onVideoRecordingFinished(success);
      }
    }

    @NonNull
    private AnimatorSet buildStartOrStopVideoRecordingAnimator(final boolean forStart) {
      Animator buttonAnimator = forStart
          ? myCaptureButtonDrawable.getCapturingVideoState().createToRecordingVideoAnimator()
          : myCaptureButtonDrawable.getRecordingVideoState().createToCapturingVideoAnimator();

      ValueAnimator additionalAnimator = forStart ? ValueAnimator.ofFloat(1f, 0f)
          : ValueAnimator.ofFloat(0f, 1f);
      final int bottomPanelHeight = myBottomPanel.getHeight();

      additionalAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
          float value = (float) animation.getAnimatedValue();
          myCaptureModeIndicator.setAlpha(value);
          myVideoRecordingTimer.setAlpha(1f - value);
          mySwitchToFrontCameraButton.setAlpha(value);
          FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)
              myBottomPanelBlackout.getLayoutParams();
          layoutParams.topMargin = bottomPanelHeight - (int) (bottomPanelHeight * value);
          myBottomPanelBlackout.setLayoutParams(layoutParams);

          if (!forStart) {
            myBottomPanelBlackout.setAlpha(value * Util.VIDEO_BOTTOM_PANEL_ALPHA);
          }
        }
      });
      additionalAnimator.addListener(new AnimatorListenerAdapter() {
        @Override
        public void onAnimationStart(Animator animation) {
          if (forStart) {
            myVideoRecordingTimer.setVisibility(View.VISIBLE);
          }
        }

        @Override
        public void onAnimationEnd(Animator animation) {
          if (!forStart) {
            myVideoRecordingTimer.setVisibility(View.GONE);
          }
        }
      });
      Util.setAnimatorDuration(additionalAnimator, 170);
      additionalAnimator.setInterpolator(new DecelerateInterpolator());
      AnimatorSet result = new AnimatorSet();
      result.playTogether(buttonAnimator, additionalAnimator);
      return result;
    }
  }

  private class MyCameraHelperCallback implements CameraHelperCallback {
    @Override
    public void onPictureTaken(@NonNull byte[] data) {
      ActivityInterface activity = getActivityInterface();

      if (activity != null) {
        activity.onPictureTaken(data);
      }
    }

    @Override
    public boolean requestCameraPermissionIfNeeded() {
      if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
          != PackageManager.PERMISSION_GRANTED) {
        if (FragmentCompat.shouldShowRequestPermissionRationale(MainFragment.this, Manifest.permission.CAMERA)) {
          new AskCameraPermissionDialogFragment().show(getFragmentManager(), FRAGMENT_DIALOG);
        } else {
          FragmentCompat.requestPermissions(MainFragment.this, new String[]{Manifest.permission.CAMERA},
              REQUEST_CAMERA_PERMISSION);
        }
        return false;
      }
      return true;
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    if (requestCode == REQUEST_CAMERA_PERMISSION) {
      if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
        ErrorDialogFragment.newInstance(getString(R.string.request_permission))
            .show(getFragmentManager(), FRAGMENT_DIALOG);
      }
    } else {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  public interface ActivityInterface extends StateAwareActivity, OrientationAwareActivity {
    void onVideoRecordingFinished(boolean success);

    void onPictureTaken(@NonNull byte[] data);

    void initOutputVideoFile(@NonNull Consumer<String> callback);
  }
}
