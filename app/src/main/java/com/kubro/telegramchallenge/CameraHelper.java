package com.kubro.telegramchallenge;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;

import com.kubro.telegramchallenge.util.Consumer;
import com.kubro.telegramchallenge.util.Disposable;
import com.kubro.telegramchallenge.util.Size;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Eugene.Kudelevsky
 */
public class CameraHelper extends CameraHelperBase implements Camera.PictureCallback, Camera.AutoFocusCallback {
  private static final String LOG_TAG = CameraHelper.class.getSimpleName();

  @Nullable private Camera myCamera;
  private CameraType myCameraType;
  private final Handler myHandler = new Handler();

  private final Object mySurfaceLock = new Object();
  private SurfaceHolder mySurfaceHolder;
  private SurfaceTexture mySurfaceTexture;

  public CameraHelper(@NonNull CameraHelperCallback callback) {
    super(callback);
    for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
      Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
      Camera.getCameraInfo(i, cameraInfo);
      System.out.println("Camera: " + cameraInfo.facing);
    }
  }

  @Override
  public void closeCamera() {
    myBackgroundHandler.post(new Runnable() {
      @Override
      public void run() {
        doDisposeCamera();
      }
    });
  }

  private void doDisposeCamera() {
    assertCameraThread();
    releaseMediaRecorder();

    if (myCamera != null) {
      doStopPreview(myCamera);
      myCamera.release();
      myCamera = null;
    }
  }

  public void stopPreview() {
    processCamera(new Consumer<Camera>() {
      @Override
      public void consume(Camera camera) {
        if (camera != null) {
          doStopPreview(camera);
        }
      }
    });
  }

  private void doStopPreview(@NonNull Camera camera) {
    try {
      camera.setPreviewCallback(null);
      camera.stopPreview();
    } catch (Exception ignored) {
    }
  }

  private void processCamera(@NonNull final Consumer<Camera> consumer) {
    myBackgroundHandler.post(new Runnable() {
      @Override
      public void run() {
        consumer.consume(myCamera);
      }
    });
  }

  @Override
  public void changePreviewSize(int width, int height, @NonNull final Runnable runAfter) {
    stopPreview();
    setupPreviewSize(width, height);

    startPreview(new Consumer<Boolean>() {
      @Override
      public void consume(Boolean success) {
        runAfter.run();
      }
    });
  }

  public void setupPreviewSize(final int width, final int height) {
    processCamera(new Consumer<Camera>() {
      @Override
      public void consume(Camera camera) {
        if (camera != null) {
          Camera.Parameters params = camera.getParameters();
          doSetupPreviewSize(params, width, height);
          camera.setParameters(params);
          SurfaceTexture surfaceTexture;

          synchronized (mySurfaceLock) {
            surfaceTexture = mySurfaceTexture;
          }
          if (surfaceTexture != null) {
            Camera.Size previewSize = params.getPreviewSize();
            surfaceTexture.setDefaultBufferSize(previewSize.width, previewSize.height);
          }
        }
      }
    });
  }

  @Nullable
  private static String chooseFocusMode(@NonNull List<String> supportedFocusModes,
                                        @NonNull String... focusModes) {
    for (String focusMode : focusModes) {
      if (supportedFocusModes.contains(focusMode)) {
        return focusMode;
      }
    }
    return null;
  }

  @Override
  public void setupFocusMode(final boolean video) {
    processCamera(new Consumer<Camera>() {
      @Override
      public void consume(Camera camera) {
        if (camera != null) {
          Camera.Parameters params = camera.getParameters();
          doSetupFocusMode(params, video);
          camera.setParameters(params);
        }
      }
    });
  }

  private static void doSetupFocusMode(Camera.Parameters params, boolean forVideo) {
    List<String> supportedFocusModes = params.getSupportedFocusModes();

    if (supportedFocusModes != null && !supportedFocusModes.isEmpty()) {
      String mode;

      if (forVideo) {
        mode = chooseFocusMode(supportedFocusModes,
            Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO, Camera.Parameters.FOCUS_MODE_FIXED);
      } else {
        mode = chooseFocusMode(supportedFocusModes,
            Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE, Camera.Parameters.FOCUS_MODE_AUTO,
            Camera.Parameters.FOCUS_MODE_FIXED);
      }
      if (mode != null) {
        params.setFocusMode(mode);
      }
    }
  }

  @Override
  public void setFlashMode(@NonNull final FlashMode flashMode) {
    processCamera(new Consumer<Camera>() {
      @Override
      public void consume(Camera camera) {
        if (camera != null) {
          Camera.Parameters parameters = camera.getParameters();
          parameters.setFlashMode(getApiFlashMode(flashMode));
          camera.setParameters(parameters);
        }
      }
    });
  }

  @NonNull
  private static String getApiFlashMode(@NonNull FlashMode flashMode) {
    switch (flashMode) {
      case AUTO:
        return Camera.Parameters.FLASH_MODE_AUTO;
      case ON:
        return Camera.Parameters.FLASH_MODE_ON;
      case OFF:
        return Camera.Parameters.FLASH_MODE_OFF;
      default:
        Log.e(LOG_TAG, "unknown flash mode " + flashMode);
        return Camera.Parameters.FLASH_MODE_OFF;
    }
  }

  @Override
  public void setDisplayRotation(final int displayRotation) {
    processCamera(new Consumer<Camera>() {
      @Override
      public void consume(Camera camera) {
        if (camera != null) {
          Camera.Parameters parameters = camera.getParameters();
          parameters.setRotation(computeCameraRotation(getCameraApiId(myCameraType), displayRotation));
          camera.setParameters(parameters);
        }
      }
    });
  }

  private void startPreview(@NonNull final Consumer<Boolean> callback) {
    processCamera(new Consumer<Camera>() {
      @Override
      public void consume(Camera camera) {
        if (camera != null) {
          doStartPreview(camera, new Runnable() {
            @Override
            public void run() {
              callback.consume(true);
            }
          });
        } else {
          myHandler.post(new Runnable() {
            @Override
            public void run() {
              callback.consume(false);
            }
          });
        }
      }
    });
  }

  private void doStartPreview(@NonNull final Camera camera, @NonNull final Runnable runAfter) {
    assertCameraThread();
    final boolean[] flag = {false};

    camera.setPreviewCallback(new Camera.PreviewCallback() {
      @Override
      public void onPreviewFrame(byte[] data, Camera camera) {
        if (!flag[0]) {
          flag[0] = true;
          camera.setPreviewCallback(null);
          myHandler.post(runAfter);
        }
      }
    });
    myBackgroundHandler.postDelayed(new Runnable() {
      @Override
      public void run() {
        if (!flag[0]) {
          flag[0] = true;
          camera.setPreviewCallback(null);
          myHandler.post(runAfter);
        }
      }
    }, 2000);
    camera.startPreview();
  }

  private void assertCameraThread() {
    if (!myBackgroundThread.equals(Thread.currentThread())) {
      throw new RuntimeException("wrong thread");
    }
  }

  public void setSurfaceHolder(@Nullable SurfaceHolder holder) {
    synchronized (mySurfaceLock) {
      mySurfaceHolder = holder;
    }
  }

  public void setSurfaceTexture(@Nullable SurfaceTexture texture) {
    synchronized (mySurfaceLock) {
      mySurfaceTexture = texture;
    }
  }

  @Override
  @NonNull
  public Disposable resetCamera(final int previewSurfaceWidth, final int previewSurfaceHeight, @NonNull final CameraType cameraType,
                                final int displayRotation, @Nullable final FlashMode flashMode, final boolean forVideo, final boolean force,
                                @NonNull final Runnable runAfter) {
    final Runnable callback = new Runnable() {
      @Override
      public void run() {
        if (myCamera != null && myCameraType == cameraType && !force) {
          myHandler.post(runAfter);
        } else {
          doResetCamera(cameraType, previewSurfaceWidth, previewSurfaceHeight,
              displayRotation, flashMode, forVideo, runAfter);
        }
      }
    };
    myBackgroundHandler.post(callback);

    return new Disposable() {
      @Override
      public void dispose() {
        myBackgroundHandler.removeCallbacks(callback);
      }
    };
  }

  private void doResetCamera(@NonNull CameraType cameraType, int previewSurfaceWidth,
                             int previewSurfaceHeight, int displayRotation,
                             @Nullable FlashMode flashMode, boolean forVideo,
                             @NonNull final Runnable runAfter) {
    assertCameraThread();
    @SuppressWarnings("SimplifiableConditionalExpression")
    int cameraApiId = getCameraApiId(cameraType);

    if (cameraApiId == -1) {
      Log.e(LOG_TAG, "Camera (id=" + cameraType + ") is not available");
      myHandler.post(runAfter);
      return;
    }
    doDisposeCamera();
    myCamera = setupAndOpenCamera(cameraApiId, previewSurfaceWidth,
        previewSurfaceHeight, displayRotation, flashMode, forVideo);
    myCameraType = cameraType;

    if (myCamera == null) {
      myHandler.post(runAfter);
      return;
    }
    SurfaceHolder surfaceHolder;
    SurfaceTexture surfaceTexture;

    synchronized (mySurfaceLock) {
      surfaceTexture = mySurfaceTexture;
      surfaceHolder = mySurfaceHolder;
    }
    if (surfaceTexture != null) {
      Camera.Size previewSize = myCamera.getParameters().getPreviewSize();
      surfaceTexture.setDefaultBufferSize(previewSize.width, previewSize.height);
    }
    try {
      if (surfaceHolder != null) {
        myCamera.setPreviewDisplay(surfaceHolder);
      }
      if (surfaceTexture != null) {
        myCamera.setPreviewTexture(surfaceTexture);
      }
    } catch (IOException e) {
      Log.e(LOG_TAG, "Error setting preview display", e);
      myHandler.post(runAfter);
      return;
    }
    doStartPreview(myCamera, runAfter);
  }

  @Override
  public void onPictureTaken(final byte[] data, final Camera camera) {
    myHandler.post(new Runnable() {
      @Override
      public void run() {
        myCallback.onPictureTaken(data);
      }
    });
  }

  public void startVideoRecording(@NonNull final Surface surface, final int rotation, @NonNull final String outputFilePath,
                                  @NonNull final Consumer<Boolean> callback) {
    processCamera(new Consumer<Camera>() {
      @Override
      public void consume(Camera camera) {
        if (camera == null) {
          myHandler.post(new Runnable() {
            @Override
            public void run() {
              callback.consume(false);
            }
          });
          return;
        }
        camera.unlock();
        int cameraId = getCameraApiId(myCameraType);
        MediaRecorder mediaRecorder = new MediaRecorder();
        mediaRecorder.setCamera(camera);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setProfile(CamcorderProfile.get(cameraId, CamcorderProfile.QUALITY_HIGH));
        mediaRecorder.setOrientationHint(computeCameraRotation(cameraId, rotation));
        mediaRecorder.setOutputFile(outputFilePath);
        mediaRecorder.setPreviewDisplay(surface);
        final boolean success = prepareMediaRecorder(mediaRecorder);

        if (!success) {
          camera.lock();
        } else {
          doStartVideoRecording();
        }
        myHandler.post(new Runnable() {
          @Override
          public void run() {
            callback.consume(success);
          }
        });
      }
    });
  }

  public void stopVideoRecording(@NonNull final Consumer<Boolean> consumer) {
    processCamera(new Consumer<Camera>() {
      @Override
      public void consume(Camera camera) {
        final boolean success = doStopVideoRecording();

        myHandler.post(new Runnable() {
          @Override
          public void run() {
            consumer.consume(success);
          }
        });
      }
    });
  }

  public void takePictureWhenFocused() {
    processCamera(new Consumer<Camera>() {
      @Override
      public void consume(Camera camera) {
        if (camera != null) {
          if (Camera.Parameters.FOCUS_MODE_AUTO.equals(camera.getParameters().getFocusMode())) {
            camera.autoFocus(CameraHelper.this);
          } else {
            takePicture(camera);
          }
        }
      }
    });
  }

  @Override
  public void onAutoFocus(boolean success, Camera camera) {
    assertCameraThread();
    takePicture(camera);
  }

  private void takePicture(@NonNull Camera camera) {
    camera.takePicture(null, null, this);
  }

  @Nullable
  private static Camera setupAndOpenCamera(int cameraId, int surfaceWidth, int surfaceHeight,
                                           int displayRotation, @Nullable FlashMode flashMode,
                                           boolean forVideo) {
    Camera newCamera = Camera.open(cameraId);

    if (newCamera == null) {
      // todo: show error
      Log.e(LOG_TAG, "Camera is not available");
      return null;
    }
    newCamera.setDisplayOrientation(90);
    Camera.Parameters params = newCamera.getParameters();
    params.setRotation(computeCameraRotation(cameraId, displayRotation));
    setupPictureSize(params);
    doSetupPreviewSize(params, surfaceWidth, surfaceHeight);
    doSetupFocusMode(params, forVideo);

    if (flashMode != null) {
      params.setFlashMode(getApiFlashMode(flashMode));
    }
    params.setJpegQuality(100);
    newCamera.setParameters(params);
    return newCamera;
  }

  @NonNull
  public static List<CameraType> getSupportedCameraTypes() {
    List<CameraType> result = new ArrayList<>();

    for (CameraType cameraType : CameraType.values()) {
      if (getCameraApiId(cameraType) >= 0) {
        result.add(cameraType);
      }
    }
    return result;
  }

  public void getSupportedFlashModes(@NonNull final Consumer<List<FlashMode>> callback) {
    processCamera(new Consumer<Camera>() {
      @Override
      public void consume(Camera camera) {
        List<FlashMode> result = Collections.emptyList();

        if (camera != null) {
          List<String> supportedFlashModes = camera.getParameters().getSupportedFlashModes();

          if (supportedFlashModes != null) {
            result = new ArrayList<>();

            for (FlashMode flashMode : FlashMode.values()) {
              if (supportedFlashModes.contains(getApiFlashMode(flashMode))) {
                result.add(flashMode);
              }
            }
          }
        }
        final List<FlashMode> finalResult = result;

        myHandler.post(new Runnable() {
          @Override
          public void run() {
            callback.consume(finalResult);
          }
        });
      }
    });
  }

  private static void doSetupPreviewSize(@NonNull Camera.Parameters params,
                                         int surfaceWidth, int surfaceHeight) {
    List<Camera.Size> supportedPreviewSizes = params.getSupportedPreviewSizes();

    if (supportedPreviewSizes == null) {
      supportedPreviewSizes = Collections.emptyList();
    }
    List<Size> supportedPreviewSizes1 = new ArrayList<>(supportedPreviewSizes.size());

    for (Camera.Size size : supportedPreviewSizes) {
      supportedPreviewSizes1.add(new Size(size.width, size.height));
    }
    Size previewSize = computeBestPreviewSize(
        supportedPreviewSizes1, surfaceWidth, surfaceHeight);

    if (previewSize != null) {
      params.setPreviewSize(previewSize.getWidth(), previewSize.getHeight());
    }
  }

  private static void setupPictureSize(@NonNull Camera.Parameters params) {
    List<Camera.Size> supportedPictureSizes = params.getSupportedPictureSizes();

    if (supportedPictureSizes == null) {
      supportedPictureSizes = Collections.emptyList();
    }
    List<Size> supportedPictureSizes1 = new ArrayList<>(supportedPictureSizes.size());

    for (Camera.Size size : supportedPictureSizes) {
      supportedPictureSizes1.add(new Size(size.width, size.height));
    }
    Size pictureSize = computeBestPictureSize(supportedPictureSizes1);

    if (pictureSize != null) {
      params.setPictureSize(pictureSize.getWidth(), pictureSize.getHeight());
    }
  }

  private static int computeCameraRotation(int cameraId, int displayRotation) {
    if (displayRotation < 0) {
      return 0;
    }
    Camera.CameraInfo info = new Camera.CameraInfo();
    Camera.getCameraInfo(cameraId, info);
    displayRotation = (displayRotation + 45) / 90 * 90;

    return info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT
        ? (info.orientation - displayRotation + 360) % 360
        : (info.orientation + displayRotation) % 360;
  }

  private static int getCameraApiId(@NonNull CameraType cameraType) {
    Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
    int cameraFacing = getCameraFacing(cameraType);

    for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
      Camera.getCameraInfo(i, cameraInfo);

      if (cameraInfo.facing == cameraFacing) {
        return i;
      }
    }
    return -1;
  }

  private static int getCameraFacing(@NonNull CameraType type) {
    switch (type) {
      case BACK:
        return Camera.CameraInfo.CAMERA_FACING_BACK;
      case FRONT:
        return Camera.CameraInfo.CAMERA_FACING_FRONT;
      default:
        Log.e(LOG_TAG, "unknown camera type " + type);
        return Camera.CameraInfo.CAMERA_FACING_FRONT;
    }
  }
}
