package com.kubro.telegramchallenge;

import android.support.annotation.NonNull;

/**
 * @author Eugene.Kudelevsky
 */
public class ActivityState {
  public static final ActivityState CAPTURING_PHOTO = new SimpleActivityState("CAPTURING_PHOTO");
  public static final ActivityState CAPTURING_VIDEO = new SimpleActivityState("CAPTURING_VIDEO");
  public static final ActivityState TAKING_PICTURE = new SimpleActivityState("TAKING_PICTURE");
  public static final ActivityState BROWSING_PHOTO = new SimpleActivityState("BROWSING_PHOTO");
  public static final ActivityState BROWSING_VIDEO = new SimpleActivityState("BROWSING_VIDEO");
  public static final ActivityState EDITING_PHOTO = new SimpleActivityState("EDITING_PHOTO");

  public static class SwitchingCamera extends ActivityState {
    @NonNull private final ActivityState myNextState;

    public SwitchingCamera(@NonNull ActivityState nextState) {
      myNextState = nextState;
    }

    @NonNull
    public ActivityState getNextState() {
      return myNextState;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      SwitchingCamera that = (SwitchingCamera) o;
      return myNextState.equals(that.myNextState);

    }

    @Override
    public int hashCode() {
      return myNextState.hashCode();
    }

    @Override
    public String toString() {
      return "SWITCHING_CAMERA: " + myNextState;
    }
  }

  public static class RecordingVideo extends ActivityState {
    private final boolean myByLongTap;

    public RecordingVideo(boolean byLongTap) {
      myByLongTap = byLongTap;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      RecordingVideo that = (RecordingVideo) o;
      return myByLongTap == that.myByLongTap;

    }

    @Override
    public int hashCode() {
      return (myByLongTap ? 1 : 0);
    }

    public boolean isByLongTap() {
      return myByLongTap;
    }

    @Override
    public String toString() {
      return "RECORDING_VIDEO: " + myByLongTap;
    }
  }

  private static class SimpleActivityState extends ActivityState {
    @NonNull private final String myDebugName;

    private SimpleActivityState(@NonNull String debugName) {
      myDebugName = debugName;
    }

    @Override
    public String toString() {
      return myDebugName;
    }
  }

  public static class TransitionalState extends ActivityState {
    @NonNull private final ActivityState myFromState;
    @NonNull private final ActivityState myToState;

    public TransitionalState(@NonNull ActivityState fromState, @NonNull ActivityState toState) {
      myFromState = fromState;
      myToState = toState;
    }

    @NonNull
    public ActivityState getFromState() {
      return myFromState;
    }

    @NonNull
    public ActivityState getToState() {
      return myToState;
    }

    @Override
    public String toString() {
      return "TRANSITION: (" + myFromState + ") -> (" + myToState + ")";
    }
  }

  public static class PlayingAnimationState extends ActivityState {
    @NonNull private final ActivityState myNextState;

    public PlayingAnimationState(@NonNull ActivityState nextState) {
      myNextState = nextState;
    }

    @NonNull
    public ActivityState getNextState() {
      return myNextState;
    }

    @Override
    public String toString() {
      return "PLAYING_ANIMATION: " + myNextState;
    }
  }

  public static class ChangingOrientation extends ActivityState {
    @NonNull private final ActivityState myNextState;

    public ChangingOrientation(@NonNull ActivityState nextState) {
      myNextState = nextState;
    }

    @NonNull
    public ActivityState getNextState() {
      return myNextState;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      SwitchingCamera that = (SwitchingCamera) o;
      return myNextState.equals(that.myNextState);

    }

    @Override
    public int hashCode() {
      return myNextState.hashCode();
    }

    @Override
    public String toString() {
      return "CHANGING_ORIENTATION: " + myNextState;
    }
  }
}
