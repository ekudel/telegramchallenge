package com.kubro.telegramchallenge;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v13.app.FragmentCompat;

/**
 * @author Eugene.Kudelevsky
 */
public class AskCameraPermissionDialogFragment extends DialogFragment {
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    return new AlertDialog.Builder(getActivity())
        .setMessage(R.string.request_permission)
        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            Fragment targetFragment = getTargetFragment();

            if (targetFragment != null) {
              FragmentCompat.requestPermissions(targetFragment,
                  new String[]{Manifest.permission.CAMERA}, MainFragment.REQUEST_CAMERA_PERMISSION);
            }
          }
        })
        .setNegativeButton(android.R.string.cancel,
            new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                Activity activity = getActivity();

                if (activity != null) {
                  activity.finish();
                }
              }
            })
        .create();
  }
}
