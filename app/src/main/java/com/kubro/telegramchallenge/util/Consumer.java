package com.kubro.telegramchallenge.util;

/**
 * @author Eugene.Kudelevsky
 */
public interface Consumer<T> {
  void consume(T t);
}
