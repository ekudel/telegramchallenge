package com.kubro.telegramchallenge.util;

/**
 * @author Eugene.Kudelevsky
 */
public class Size {
  public static final Size EMPTY = new Size(0, 0);

  private final int myWidth;
  private final int myHeight;

  public Size(int width, int height) {
    myWidth = width;
    myHeight = height;
  }

  public int getWidth() {
    return myWidth;
  }

  public int getHeight() {
    return myHeight;
  }
}
