package com.kubro.telegramchallenge.util;

/**
 * @author Eugene.Kudelevsky
 */
public interface Function<V, R> {
  R fun(V value);
}
