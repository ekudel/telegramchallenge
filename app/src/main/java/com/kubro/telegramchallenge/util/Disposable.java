package com.kubro.telegramchallenge.util;

/**
 * @author Eugene.Kudelevsky
 */
public interface Disposable {
  void dispose();
}
