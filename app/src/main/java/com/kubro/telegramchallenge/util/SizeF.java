package com.kubro.telegramchallenge.util;

/**
 * @author Eugene.Kudelevsky
 */
public class SizeF {
  public static final SizeF EMPTY = new SizeF(0, 0);

  private final float myWidth;
  private final float myHeight;

  public SizeF(float width, float height) {
    myWidth = width;
    myHeight = height;
  }

  public float getWidth() {
    return myWidth;
  }

  public float getHeight() {
    return myHeight;
  }
}
