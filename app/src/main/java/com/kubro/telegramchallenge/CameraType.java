package com.kubro.telegramchallenge;

/**
 * @author Eugene.Kudelevsky
 */
public enum CameraType {
  BACK, FRONT
}
