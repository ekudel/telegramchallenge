package com.kubro.telegramchallenge;


import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;

import com.kubro.telegramchallenge.util.Disposable;
import com.kubro.telegramchallenge.view.ImageBrowserView;
import com.kubro.telegramchallenge.view.MainPanelLayout;
import com.kubro.telegramchallenge.view.SimpleSeekBar;
import com.kubro.telegramchallenge.view.ToggleImageButton;

import java.io.File;

public class BrowseFragment extends FragmentBase {
  private static final String LOG_TAG = BrowseFragment.class.getSimpleName();

  private MainPanelLayout myMainPanelLayout;
  private SurfaceView mySurfaceView;
  private View myBrowsePicturePanel;
  private ImageButton myBrowseOkButton;
  private ImageButton myBrowseCancelButton;
  private ImageButton myCropButton;
  private ImageBrowserView myBrowsePhotoView;
  private View myBrowsePhotoViewWrapper;
  private ToggleImageButton myPlayPauseVideoButton;
  private SimpleSeekBar mySeekBar;
  private View myBottomPanelBlackout;

  private boolean myDestroyed;
  private MediaPlayer myMediaPlayer;
  private HandlerThread myMediaPlayerThread;
  private Handler myMediaPlayerHandler;
  private Disposable mySeekBarUpdatingDisposable;
  private Handler myHandler;
  private boolean mySurfaceCreated;
  private String myVideoFilePathToShowWhenSurfaceCreated;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_browse, container, false);
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    View view = getView();
    assert view != null;
    myMainPanelLayout = (MainPanelLayout) view.findViewById(R.id.mainPanelLayout);
    mySurfaceView = (SurfaceView) view.findViewById(R.id.surfaceView);
    myBrowsePhotoView = (ImageBrowserView) view.findViewById(R.id.browsePhotoView);
    myBrowsePhotoViewWrapper = view.findViewById(R.id.browsePhotoViewWrapper);
    myBrowsePicturePanel = view.findViewById(R.id.browsePicturePanel);
    myBrowseOkButton = (ImageButton) view.findViewById(R.id.browseOkButton);
    myBrowseCancelButton = (ImageButton) view.findViewById(R.id.browseCancelButton);
    myCropButton = (ImageButton) view.findViewById(R.id.cropButton);
    myPlayPauseVideoButton = (ToggleImageButton) view.findViewById(R.id.playPauseVideoButton);
    mySeekBar = (SimpleSeekBar) view.findViewById(R.id.seekBar);
    myBottomPanelBlackout = view.findViewById(R.id.bottomPanelBlackout);
    myDestroyed = false;
    mySurfaceCreated = false;
    myVideoFilePathToShowWhenSurfaceCreated = null;
    myHandler = new Handler();

    myCropButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        ActivityInterface activity = getActivityInterface();

        if (activity != null && activity.getState() == ActivityState.BROWSING_PHOTO) {
          activity.onCropPictureButtonClicked();
        }
      }
    });
    StateAwareActivity activity = getActivityInterface();
    assert activity != null;

    myBrowseCancelButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finishBrowsing(false);
      }
    });
    myBrowseOkButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finishBrowsing(true);
      }
    });
    myPlayPauseVideoButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        StateAwareActivity activity = getActivityInterface();

        if (activity == null) {
          return;
        }
        if (activity.getState() == ActivityState.BROWSING_VIDEO) {
          myPlayPauseVideoButton.toggle();
          final boolean startPlayback = myPlayPauseVideoButton.isChecked();

          myMediaPlayerHandler.post(new Runnable() {
            @Override
            public void run() {
              if (myMediaPlayer != null) {
                if (startPlayback) {
                  myMediaPlayer.start();
                  startSeekBarUpdating();
                } else {
                  myMediaPlayer.pause();
                  stopSeekBarUpdating();
                }
              }
            }
          });
        }
      }
    });
    mySurfaceView.setZOrderMediaOverlay(true);
    mySurfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
      @Override
      public void surfaceCreated(SurfaceHolder holder) {
        mySurfaceCreated = true;

        if (!myDestroyed && myVideoFilePathToShowWhenSurfaceCreated != null) {
          doShowVideoPlayer(myVideoFilePathToShowWhenSurfaceCreated);
          myVideoFilePathToShowWhenSurfaceCreated = null;
        }
      }

      @Override
      public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
      }

      @Override
      public void surfaceDestroyed(SurfaceHolder holder) {
        mySurfaceCreated = false;
      }
    });

    myMediaPlayerThread = new HandlerThread("MediaPlayer");
    myMediaPlayerThread.start();
    myMediaPlayerHandler = new Handler(myMediaPlayerThread.getLooper());
  }

  private void startSeekBarUpdating() {
    if (mySeekBarUpdatingDisposable != null) {
      mySeekBarUpdatingDisposable.dispose();
    }
    final boolean[] canceledFlagRef = {false};

    myMediaPlayerHandler.post(new Runnable() {
      @Override
      public void run() {
        if (myMediaPlayer != null) {
          int duration = myMediaPlayer.getDuration();

          if (shouldShowSeekBarForDuration(duration)) {
            final long step = Math.max(50, duration / 200);

            myHandler.post(new Runnable() {
              @Override
              public void run() {
                if (!myDestroyed && !canceledFlagRef[0]) {
                  doUpdateSeekBar(canceledFlagRef, step);
                }
              }
            });
          }
        }
      }
    });

    mySeekBarUpdatingDisposable = new Disposable() {
      @Override
      public void dispose() {
        canceledFlagRef[0] = true;
      }
    };
  }

  private boolean shouldShowSeekBarForDuration(int duration) {
    return duration >= 500;
  }

  private void stopSeekBarUpdating() {
    if (mySeekBarUpdatingDisposable != null) {
      mySeekBarUpdatingDisposable.dispose();
      mySeekBarUpdatingDisposable = null;
    }
  }

  private void doUpdateSeekBar(final boolean[] canceledFlagRef, final long step) {
    if (myDestroyed) {
      return;
    }
    final Runnable runAfter = new Runnable() {
      @Override
      public void run() {
        doUpdateSeekBar(canceledFlagRef, step);
      }
    };
    mySeekBar.postDelayed(new Runnable() {
      @Override
      public void run() {
        StateAwareActivity activity = getActivityInterface();

        if (activity == null || myDestroyed || canceledFlagRef[0]) {
          return;
        }
        if (activity.getState() == ActivityState.BROWSING_VIDEO) {
          myMediaPlayerHandler.post(new Runnable() {
            @Override
            public void run() {
              if (myMediaPlayer == null || !myMediaPlayer.isPlaying()) {
                mySeekBar.post(runAfter);
                return;
              }
              int duration = myMediaPlayer.getDuration();
              int position = myMediaPlayer.getCurrentPosition();
              final float progress = duration == 0 ? 0 : Math.min(1f, (float) position / duration);

              mySeekBar.post(new Runnable() {
                @Override
                public void run() {
                  if (myDestroyed || canceledFlagRef[0]) {
                    return;
                  }
                  mySeekBar.setProgress(progress);
                  runAfter.run();
                }
              });
            }
          });
        } else {
          runAfter.run();
        }
      }
    }, step);
  }

  private void finishBrowsing(boolean ok) {
    ActivityInterface activity = getActivityInterface();

    if (activity == null || (activity.getState() != ActivityState.BROWSING_PHOTO &&
        activity.getState() != ActivityState.BROWSING_VIDEO)) {
      return;
    }
    myPlayPauseVideoButton.setChecked(false);
    activity.onBrowsingFinished(ok);

    myMediaPlayerHandler.post(new Runnable() {
      @Override
      public void run() {
        if (myMediaPlayer != null) {
          myMediaPlayer.release();
          myMediaPlayer = null;
        }
      }
    });
  }

  @Override
  public void onDestroy() {
    myDestroyed = true;
    if (mySeekBarUpdatingDisposable != null) {
      mySeekBarUpdatingDisposable.dispose();
      mySeekBarUpdatingDisposable = null;
    }
    myMediaPlayerHandler.removeCallbacks(null);

    myMediaPlayerHandler.post(new Runnable() {
      @Override
      public void run() {
        if (myMediaPlayer != null) {
          myMediaPlayer.release();
          myMediaPlayer = null;
        }
        myMediaPlayerThread.quit();
      }
    });
    super.onDestroy();
  }

  @Nullable
  private ActivityInterface getActivityInterface() {
    return (ActivityInterface) getActivity();
  }

  public View getBrowseCancelButton() {
    return myBrowseCancelButton;
  }

  public View getBrowseOkButton() {
    return myBrowseOkButton;
  }

  public ToggleImageButton getPlayPauseVideoButton() {
    return myPlayPauseVideoButton;
  }

  public SimpleSeekBar getSeekBar() {
    return mySeekBar;
  }

  public View getCropButton() {
    return myCropButton;
  }

  public View getBrowsePhotoViewWrapper() {
    return myBrowsePhotoViewWrapper;
  }

  public View getBrowsePicturePanel() {
    return myBrowsePicturePanel;
  }

  public ImageBrowserView getBrowsePhotoView() {
    return myBrowsePhotoView;
  }

  @Override
  public boolean onBackPressed() {
    StateAwareActivity activity = getActivityInterface();

    if (activity != null &&
        (activity.getState() == ActivityState.BROWSING_PHOTO ||
            activity.getState() == ActivityState.BROWSING_VIDEO)) {
      finishBrowsing(false);
      return true;
    }
    return false;
  }

  @Override
  public boolean canHandleOrientationChange() {
    ActivityInterface activityInterface = getActivityInterface();

    if (activityInterface != null) {
      ActivityState state = activityInterface.getState();
      return state == ActivityState.BROWSING_PHOTO || state == ActivityState.BROWSING_VIDEO;
    }
    return false;
  }

  @Nullable
  @Override
  public Animator buildOrientationChangeAnimator(int startDegrees, int deltaDegrees) {
    ValueAnimator animator = ValueAnimator.ofFloat(startDegrees, startDegrees + deltaDegrees);

    animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      @Override
      public void onAnimationUpdate(ValueAnimator animation) {
        float rotation = (360 - (float) animation.getAnimatedValue()) % 360;
        myBrowseOkButton.setRotation(rotation);
        myBrowseCancelButton.setRotation(rotation);
        myCropButton.setRotation(rotation);
        myPlayPauseVideoButton.setRotation(rotation);
      }
    });
    animator.setDuration(200);
    animator.setInterpolator(new LinearInterpolator());
    return animator;
  }

  @Override
  public void setUiOrientation(int degrees) {
    int rotation = (360 - degrees) % 360;
    myBrowseOkButton.setRotation(rotation);
    myBrowseCancelButton.setRotation(rotation);
    myCropButton.setRotation(rotation);
    myPlayPauseVideoButton.setRotation(rotation);
  }

  public void showVideoPlayer(@NonNull final String path) {
    if (mySurfaceCreated) {
      myVideoFilePathToShowWhenSurfaceCreated = null;
      doShowVideoPlayer(path);
    } else {
      myVideoFilePathToShowWhenSurfaceCreated = path;
    }
  }

  private void doShowVideoPlayer(@NonNull final String path) {
    myMediaPlayerHandler.post(new Runnable() {
      @Override
      public void run() {
        Activity activity = getActivity();

        if (activity == null) {
          return;
        }
        if (myMediaPlayer != null) {
          myMediaPlayer.release();
        }
        myMediaPlayer = MediaPlayer.create(activity, Uri.fromFile(new File(path)),
            mySurfaceView.getHolder());

        if (myMediaPlayer == null) {
          Log.e(LOG_TAG, "Cannot create media player");
          // todo
          return;
        }
        final boolean showSeekBar = shouldShowSeekBarForDuration(myMediaPlayer.getDuration());
        myMediaPlayer.start();
        myMediaPlayer.pause();

        myMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
          @Override
          public void onCompletion(MediaPlayer mp) {
            myHandler.post(new Runnable() {
              @Override
              public void run() {
                if (!myDestroyed) {
                  myPlayPauseVideoButton.setChecked(false);

                  if (showSeekBar) {
                    mySeekBar.setProgress(1f);
                  }
                }
              }
            });
          }
        });
      }
    });
  }

  public View getBottomPanelBlackout() {
    return myBottomPanelBlackout;
  }

  public SurfaceView getSurfaceView() {
    return mySurfaceView;
  }

  public MainPanelLayout getMainPanelLayout() {
    return myMainPanelLayout;
  }

  public interface ActivityInterface extends StateAwareActivity {
    void onCropPictureButtonClicked();

    void onBrowsingFinished(boolean ok);
  }
}
