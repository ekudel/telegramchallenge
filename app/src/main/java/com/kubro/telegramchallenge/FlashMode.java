package com.kubro.telegramchallenge;

/**
 * @author Eugene.Kudelevsky
 */
public enum FlashMode {
  AUTO, ON, OFF
}
